Over the last decade I have created code and libraries for a variety of technical problems. These include artificial intelligence, physics, statistics, animation, cryptography, etc. This library is my attempt to bring all these various technical code pieces into a single library for use across a wide variety of problems including game development - where the union of many of the above problems are encountered - or other issues that require technical code.

### NOTE ###
Test cases for AI are non-deterministic. Thus, AI test cases may fail. This is particularly true for test cases that use random greedy training.  

### com.talixa.techlib.ai ###
Code for AI applications including:  
1. Dijkstra's Algorithm  
2. Group Behavior Algorithms  
3. K-Means Clustering  
4. Training with Greedy Random Walk  
5. Training with Hill Climbing  
6. Radial Basis Function Networks  
7. General AI code  
8. Genetic Algorithms  
9. Traveling Salesman Problem  

### com.talixa.techlib.arch ###
Functions for dealing with different machine architectures

### com.talixa.techlib.audio ###
Functions and classes for dealing with wave files

### com.talixa.techlib.binary ###
Functions for dealing with binary data

### com.talixa.techlib.conversion ###
Functions to convert between measurement types  

### com.talixa.techlib.crypto ###
Various cryptography functions and tools.  
This package requires BouncyCastle at runtime.  
1. Code to simplify usage of DES, 3DES, RSA  
2. Code to simplify usage of MD5 and SHA  
3. Code for cipher streams including Galois and Fibonacci  
4. Toolkit for basic steganography  
5. Toolkit for dealing with PKI data  

### com.talixa.techlib.data ###
Functions for Big Data including statistics and probability

### com.talixa.techlib.dsp ###
Simple DSP functions  
1. Simple audio mixer  
2. Band pass filter  
3. Frequency translator  
4. Gain controller  
5. Tone generator  

### com.talixa.techlib.encoding ###
Functions for different encoding types

### com.talixa.techlib.fft ###
Standard Fast Fourier Transform code

### com.talixa.techlib.games ###
Code for standard game functions  
1. Simple implementation of a card deck

### com.talixa.techlib.geo ###
Functions for dealing with distance calculation  

### com.talixa.techlib.geometry ###
Functions for geometry calculations

### com.talixa.techlib.math ###
Various advanced math calculations  
1. Field of view calculator  
2. Vector functions  
3. Interpolation and easing functions

### com.talixa.techlib.physics ###
Calculations related to the science of physics

### com.talixa.techlib.r ###
Functions to export data vectors easily into R language

### com.talixa.techlib.shared ###
Code shared among other packages