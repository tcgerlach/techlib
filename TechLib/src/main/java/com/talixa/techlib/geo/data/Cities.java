package com.talixa.techlib.geo.data;

import com.talixa.techlib.conversion.Convert;

public class Cities {
	public static final City AltoonaPA = new City("Altoona, PA", 40.518681, -78.394736, Convert.feetToMeters(1206));
	public static final City NewYorkCity = new City("New York, NY", 40.712784, -74.005941, Convert.feetToMeters(33));
	public static final City Pittsburgh = new City("Pittsburgh, PA", 40.440625, -79.995886, Convert.feetToMeters(1365));
	public static final City Boston = new City("Boston, MA", 42.360082, -71.058880, Convert.feetToMeters(141));
	public static final City Orlando = new City("Orlando, FL", 28.538335, -81.379236, Convert.feetToMeters(82));
	public static final City Savannah = new City("Savannah, GA", 32.083541, -81.099834, Convert.feetToMeters(49));
	public static final City Atlanta = new City("Atlanta, GA", 33.748995, -84.387982, Convert.feetToMeters(1050));
	public static final City LosAngeles = new City("Los Angeles, CA", 34.052234, -118.243685, Convert.feetToMeters(233));
	public static final City Seattle = new City("Seattle, WA", 47.608013, -122.335167, Convert.feetToMeters(518));
	public static final City Houston = new City("Houston, TX", 29.702902, -95.524986, Convert.feetToMeters(80));
	public static final City Mobile = new City("Mobile, AL", 30.739180, -88.080482, Convert.feetToMeters(9.8));	
	public static final City Anchorage = new City("Anchorage, AK", 61.217381, -149.863129, Convert.feetToMeters(102));
	public static final City Denver = new City("Denver, CO", 39.742043, -104.991531, Convert.feetToMeters(5280));
	public static final City Chicago = new City("Chicago, IL", 41.881832, -87.623177, Convert.feetToMeters(594));
	public static final City Baltimore = new City("Baltimore, MD", 39.299236, -76.609383, Convert.feetToMeters(480));
	public static final City Boise = new City("Boise, ID", 43.618881, -116.215019, Convert.feetToMeters(2730));
	public static final City Portland = new City("Portland, OR", 45.548325, -122.588303, Convert.feetToMeters(50));
	
	public static final City Berlin = new City("Berlin, Germany", 52.520007, 13.404954, Convert.feetToMeters(112));
	public static final City Munich = new City("Munich, Germany", 48.135125, 11.581981, Convert.feetToMeters(1706));
	public static final City Paris = new City("Paris, France", 48.856614, 2.352222, Convert.feetToMeters(115));
	public static final City Madrid = new City("Madrid, Spain", 40.416775, -3.703790, Convert.feetToMeters(2118));
	public static final City Rome = new City("Rome, Italy", 41.902783, 12.496366, Convert.feetToMeters(456));
	public static final City London = new City("London, England", 51.507351, -0.127758, Convert.feetToMeters(115));
	
	public static final City Cairo = new City("Cairo, Egypt", 30.044420, 31.235712, Convert.feetToMeters(75));
	
	public static final City Tokyo = new City("Tokyo, Japan", 35.689487, 139.691706, Convert.feetToMeters(131));
	public static final City Beijing = new City("Beijing, China", 39.904211, 116.407395, Convert.feetToMeters(144));
	public static final City Seoul = new City("Seoul, Korea", 37.566535, 126.977969, Convert.feetToMeters(125));
	public static final City HongKong = new City("Hong Kong, China", 22.396428, 114.109497, Convert.feetToMeters(500));
	public static final City Shanghai = new City("Shanghai, China", 31.230416, 121.473701, Convert.feetToMeters(13));
	
	public static final City[] AllCities = new City[] {
		NewYorkCity, Orlando, Pittsburgh, LosAngeles, Chicago, Atlanta, AltoonaPA, Boston, Savannah, Denver, Boise, 
		Seattle, Houston, Mobile, Anchorage, Baltimore, Portland, Madrid, Munich, Berlin, Paris, Rome, London,
		Cairo, Seoul, Tokyo, Beijing, HongKong, Shanghai
	};
	
	public static final City[] NorthAmerica = new City[] {
		AltoonaPA, Anchorage, Atlanta, Baltimore, Boise, Boston, Chicago, Denver, Houston, LosAngeles,
		Mobile, NewYorkCity, Orlando, Pittsburgh, Portland, Savannah, Seattle
	};
}
