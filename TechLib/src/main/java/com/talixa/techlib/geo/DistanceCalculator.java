package com.talixa.techlib.geo;

import com.talixa.techlib.geo.data.City;

public class DistanceCalculator {
	
	private static final int R = 6371;
	
	/**
	 * Calculate distance between two points in latitude and longitude taking
	 * into account height difference. If you are not interested in height
	 * difference pass 0.0. Uses Haversine method as its base.
	 * 
	 * @param start starting city data
	 * @param end ending city data
	 * @returns Distance in Meters
	 */
	public static double distance(final City start, final City  end) {
	    Double latDistance = Math.toRadians(end.getLatitude() - start.getLatitude());
	    Double lonDistance = Math.toRadians(end.getLongitude() - start.getLongitude());
	    Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
	            + Math.cos(Math.toRadians(start.getLatitude())) * Math.cos(Math.toRadians(end.getLatitude()))
	            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
	    
	    Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    double distance = R * c * 1000; // convert to meters

	    double height = start.getElevation() - end.getElevation();

	    distance = Math.pow(distance, 2) + Math.pow(height, 2);

	    return Math.sqrt(distance);
	}
	
	/**
	 * Calculate the total distance from the first city to the last in order.
	 * 
	 * @param cities list of cities in desired travel order
	 * @return total distance traveled
	 */
	public static double routeDistance(final City[] cities) {
		double distance = 0;
		for(int i = 0; i < cities.length -1; ++i) {
			distance += distance(cities[i], cities[i+1]);
		}
		return distance;
	}
	
	/**
	 * Find the city nearest to the given lat/long
	 * 
	 * @param latitude current city latitude
	 * @param longitude current city longitude
	 * @param cityList list of available cities
	 * @return nearest city from the list of available cities
	 */
	public static City getNearestCity(final double latitude, final double longitude, final City[] cityList) {
		City myCity = new City("City", latitude, longitude, 0);
		City nearest = null;
		double shortestDistance = Float.MAX_VALUE;
		for(int i = 0; i < cityList.length; ++i) {
			double distance = distance(myCity, cityList[i]);
			if (distance < shortestDistance) {
				nearest = cityList[i];
				shortestDistance = distance;
			}
		}
		return nearest;
	}
}
