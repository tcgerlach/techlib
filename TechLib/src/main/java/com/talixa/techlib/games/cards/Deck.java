package com.talixa.techlib.games.cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Simple implementation of a standard deck of playing cards.
 * 
 * @author tcgerlach
 */
public class Deck {

	private List<Card> deck;

	/**
	 * Create a new deck of standard playing cards.
	 */
	public Deck() {
		deck = new ArrayList<Card>();

		for(int i = 0; i < 4; ++i) {
			Card.Suit s = Card.Suit.CLUBS;
			switch (i) {
				case 0: s = Card.Suit.CLUBS; break;
				case 1: s = Card.Suit.DIAMONDS; break; 
				case 2: s = Card.Suit.HEARTS; break;
				case 3: s = Card.Suit.SPADES; break;
				default: s = Card.Suit.SPADES; break;
			}

			for(int j = 0; j < 13; ++j) {
				deck.add(new Card(s, j + 1));
			}
		}
	}

	/** 
	 * Shuffle the deck of cards.
	 */
	public void shuffle() {
		Collections.shuffle(deck);
	}

	/**
	 * Retrieve the top card of the deck.
	 * @return top card of deck
	 */
	public Card getTopCard() {
		return deck.remove(0);
	}
	
	/**
	 * Retrieve the number of cards left in the deck.
	 * @return number of remaining cards.
	 */
	public int cardsLeft() {
		return deck.size();
	}
}
