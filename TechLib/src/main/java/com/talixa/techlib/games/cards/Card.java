package com.talixa.techlib.games.cards;

/**
 * Simple implementation of a playing card.
 * 
 * @author tcgerlach
 */
public class Card {

	/**
	 * Suit for the playing card.
	 */
	public enum Suit {
		DIAMONDS, HEARTS, CLUBS, SPADES
	};

	private Suit suit;
	private int value;
	private boolean faceUp;

	/**
	 * Create a card with the desired values.
	 * @param suit determine what suit this card belongs to.
	 * @param value the integer value of the card.
	 */
	public Card(final Suit suit, final int value) {
		this.suit = suit;
		this.value = value;
		faceUp = false;
	}

	/**
	 * Determine if this is a red card.
	 * @return true if red
	 */
	public boolean isRed() {
		return suit.equals(Suit.DIAMONDS) || suit.equals(Suit.HEARTS);
	}

	/**
	 * Determine if this is a black card.
	 * @return true if black
	 */
	public boolean isBlack() {
		return !isRed();
	}

	/**
	 * Return the suit of this card.
	 * @return card suit
	 */
	public Suit getSuit() {
		return this.suit;
	}

	/**
	 * Return the integer value of this card
	 * @return value of card as integer
	 */
	public int getIntValue() {
		return value;
	}

	/**
	 * Determine if this card is face up.
	 * @return true if face up.
	 */
	public boolean isFaceUp() {
		return faceUp;
	}

	/**
	 * Flip this card over.
	 */
	public void flip() {
		faceUp = !faceUp;
	}

	/**
	 * Return the value of the card as a string. 
	 * 2-10, Jack, Queen, King, or ace
	 * @return String value of card
	 */
	public String getValue() {
		String valueName = "";
		if (value == 1) {
			valueName = "Ace";
		} else if (value < 11) {
			valueName = String.valueOf(value); 
		} else if (value == 11) {
			valueName = "Jack";
		} else if (value == 12) {
			valueName = "Queen";
		} else if (value == 13) {
			valueName = "King";
		}

		return valueName;
	}

	/**
	 * Shortened form of card name.
	 * 2-10, A, J, Q, K
	 * @return abbreviated name of card
	 */
	public String getAbbreviatedValue() {
		String valueName = "";
		if (value == 1) {
			valueName = "A";
		} else if (value < 11) {
			valueName = String.valueOf(value);
		} else if (value == 11) {
			valueName = "J";
		} else if (value == 12) {
			valueName = "Q";
		} else if (value == 13) {
			valueName = "K";
		}

		return valueName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((suit == null) ? 0 : suit.hashCode());
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (suit != other.suit)
			return false;
		if (value != other.value)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getValue() + " " + suit.toString();
	}
}
