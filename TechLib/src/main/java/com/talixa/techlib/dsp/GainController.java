package com.talixa.techlib.dsp;

/**
 * Simple controller to modify audio gain.
 * 
 * @author tcgerlach
 */
public class GainController {
	
	/*
	 * To change gain, multiply sample by gain
	 */
	
	/**
	 * Change the volume of the input PCM data
	 * @param data PCM input
	 * @param gain volume change to apply 2 will be twice as loud, .5 will be half as loud
	 * @return new PCM audio data
	 */
	public static short[] setGain(short[] data, double gain) {
		short[] newData = new short[data.length];
		for(int i = 0; i < data.length; ++i) {
			newData[i] = (short)(data[i] * gain);
		}
		return newData;
	}			
}
