package com.talixa.techlib.dsp;

/**
 * Tools for interfacing with the FFT data.
 * 
 * @author tcgerlach
 */
public class SharedDSPFunctions {
	
	/**
	 * Default sample rate for PCM audio data
	 */
	public static final double SAMPLE_RATE = 8000;

	/**
	 * Convert a short array of PCM samples to a byte array
	 * @param shortArray input PCM samples
	 * @return byte array for given PCM short data
	 */
	public static byte[] shortArrayToByteArray(short[] shortArray) {		
		byte[] byteData = new byte[shortArray.length*2];
		for(int i = 0; i < shortArray.length; ++i) {
			byteData[i*2+1] = (byte)(shortArray[i] >>> 8 & 0xff);
			byteData[i*2] = (byte)(shortArray[i] & 0xff);
		}
		return byteData;
	}
	
	/**
	 * Calculate the frequency associated with a particular sample number from the FFT output.
	 * 
	 * @param fftLength length of the FFT
	 * @param sampleNumber sample number in the FFT
	 * @return associated frequency
	 */
	public static int getFreqBySampleNumber(int fftLength, int sampleNumber) {	
		return (int)((SAMPLE_RATE/fftLength) * sampleNumber);		
	}
	
	/**
	 * Calculate the sample number for a given input frequency.
	 * 
	 * @param fftLength length of the FFT
	 * @param freq frequency of desired sample
	 * @return frequency
	 */
	public static int getSampleNumberByFrequency(int fftLength, double freq) {
		return (int)Math.pow((SAMPLE_RATE/(fftLength*freq)),-1);
	}
}
