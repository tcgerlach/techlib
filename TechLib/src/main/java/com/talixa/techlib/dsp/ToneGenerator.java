package com.talixa.techlib.dsp;

/**
 * Simple utility to create an audio sample of a given frequency.
 * Output wave form is sin wave.
 * 
 * @author tcgerlach
 */
public class ToneGenerator {
	
	/*
	 * To generate tone
	 * 1) Calculate angle of wave
	 * 2) Convert angle to amplitude 
	 */

	/**
	 * Generate a pure tone
	 * @param frequency what frequency the tone should be
	 * @param volume relative volume of tone from 0 to 1
	 * @param seconds how long the audio sample should be
	 * @param phase start phase for the output tone
	 * @return PCM audio data for desired tone
	 */
	public static short[] generateWave(double frequency, double volume, int seconds, int phase) {		
		if (volume > 1) {
			volume = 1;
		}
		
		double samplesPerWave = (SharedDSPFunctions.SAMPLE_RATE / frequency);
				
		short[] data = new short[seconds * (int)SharedDSPFunctions.SAMPLE_RATE];
		for(int i = 0; i < data.length; ++i) {
			// calculate angle of wave
			double angle = 0;
			if (i != 0) {
				angle = 360f/samplesPerWave*i;
			}
			// calculate sine of wave
			double radians = Math.toRadians(angle+phase);
			double sine = Math.sin(radians);
			// create sample @ desired volume
			data[i] = (short)(sine*(Short.MAX_VALUE*volume));
		}
		return data;
	}
	
	/**
	 * Generate a pure tone
	 * @param frequency what frequency the tone should be
	 * @param volume relative volume of tone from 0 to 1
	 * @param seconds how long the audio sample should be
	 * @return PCM audio data for desired tone
	 */
	public static short[] generateWave(double frequency, double volume, int length) {
		return generateWave(frequency,volume,length,0);
	}
}
