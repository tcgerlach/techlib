package com.talixa.techlib.dsp;

import com.talixa.techlib.fft.FFT;
import com.talixa.techlib.shared.Complex;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A rudimentary tool for filtering audio data. More 
 * advanced - and indeed better - algorithms are available 
 * to do this. This code was an attempt to perform 
 * filtering using a 'clean room' approach.
 * 
 * @author tcgerlach
 */
public class BandPassFilter {
	
	/*
	 * To make a band pass filter:
	 * 1) Run FFT on input data
	 * 2) Zero all data above & below the pass range
	 * 3) DeFFT the data
	 * 4) Output
	 */

	private static final int FFT_LEN = 8192*2;
	private static final int SHIFT_VALUE = FFT_LEN;	
			
	/**
	 * Filter PCM audio data
	 * @param data input data buffer in PCM format
	 * @param filterBelowFreq minimum frequency for bandpass
	 * @param filterAboveFreq maximum frequency for bandpass
	 * @return data with frequencies between min and max
	 */
	@SuppressFBWarnings(value="DLS_DEAD_LOCAL_STORE", justification="frequencyData is passed to fft - FindBugs is wrong")
	public static short[] filterData(short[] data, int filterBelowFreq, int filterAboveFreq) {
		int numberSamples = data.length;
		short[] filteredData = new short[numberSamples];
		Complex[] amplitudeData = new Complex[FFT_LEN];
		Complex[] frequencyData = new Complex[FFT_LEN];	
		
		for(int baseAddress = 0; (baseAddress)+FFT_LEN < numberSamples; baseAddress+=SHIFT_VALUE) {			
			// fill the data array
			for (int offset = 0; offset < FFT_LEN; offset++) {
				int desiredSampleIndex = (baseAddress)+offset;					
				short signedSample = data[desiredSampleIndex];									
				double sample = ((double) signedSample) / (Short.MAX_VALUE);
				amplitudeData[offset] = new Complex(sample,0);
			}															

			// run fft
			frequencyData = FFT.fft(amplitudeData);
			
			// remove below low freq
			int removeBelow = SharedDSPFunctions.getSampleNumberByFrequency(FFT_LEN, filterBelowFreq);
			for(int i = 0; i < removeBelow; ++i) {
				frequencyData[i] = new Complex(0,0);
				frequencyData[FFT_LEN-i-1] = new Complex(0,0);
			}
			
			// remove above high freq			
			int removeAbove = SharedDSPFunctions.getSampleNumberByFrequency(FFT_LEN, filterAboveFreq);
			for(int i = removeAbove; i < frequencyData.length/2; ++i) {
				frequencyData[i] = new Complex(0,0);
				frequencyData[FFT_LEN-i-1] = new Complex(0,0);
			}									
			
			// undo fft
			Complex[] filterFft = FFT.ifft(frequencyData);
			
			// create new data array
			for(int i = 0; i < FFT_LEN; ++i) {					
				filteredData[baseAddress+i] = (short)(filterFft[i].getReal() * Short.MAX_VALUE);				
			}						
		}
				
		return filteredData;	
	}	
}
