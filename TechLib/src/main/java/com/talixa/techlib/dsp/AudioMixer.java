package com.talixa.techlib.dsp;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Tool for mixing PCM audio data.
 * 
 * @author tcgerlach
 */
public class AudioMixer {
	
	/*
	 * To mix PCM samples, simply sum both values and divide by 2
	 * Or, if both files are in different ranges, sum the number
	 */
	
	/**
	 * SUM will add both PCM inputs
	 * AVERAGE will take the average value of the PCM inputs
	 */
	public enum MixerMode {SUM, AVERAGE};
	
	/**
	 * Mix two PCM audio data chunks
	 * @param background PCM input data for background audio
	 * @param foreground PCM input data for foreground audio
	 * @param mode SUM or AVERAGE
	 * @return PCM audio data of files mixed
	 */
	@SuppressFBWarnings(value="IM_AVERAGE_COMPUTATION_COULD_OVERFLOW", justification="Will only happen if user sums, known risk")
	public static short[] mix(short[] background, short[] foreground, MixerMode mode) {	
		int maxLength = background.length > foreground.length ? background.length : foreground.length;
		short[] mixedData = new short[maxLength];
		
		for(int i = 0; i < maxLength; ++i) {
			
			short bg = i < background.length ? background[i] : 0;
			short fg = i < foreground.length ? foreground[i] : 0;
			
			// add pcm samples and divide by 2
			int sum = bg + fg;
			if (mode.equals(MixerMode.AVERAGE)) {
				mixedData[i] = (short)(sum/2);
			} else {
				mixedData[i] = (short)sum;
			}			
		}
		return mixedData;
	}		
}
