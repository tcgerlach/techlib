package com.talixa.techlib.r;

/**
 * Utilities to export data sets from Java into R.
 * 
 * @author tcgerlach
 */
public class RHelper {

	/**
	 * Create a string that can be entered into an R program with the given data.
	 * @param rVar name of R variable
	 * @param data list of data objects to output
	 * @return string to run in R to import data
	 */
	public static String convertDataSetToR(final String rVar, final double[] data) {
		StringBuilder x = new StringBuilder(rVar + " <- c(");

		for(int i = 0; i < data.length; ++i) {
			x.append(data[i]);
			if (i != (data.length-1)) {
				x.append(",");
			}
		}

		x.append(")");
		return x.toString();
	}
	
	/**
	 * Create a string that can be entered into an R program with the given data.
	 * @param rVar name of R variable
	 * @param data list of data objects to output
	 * @return string to run in R to import data
	 */
	public static String convertDataSetToR(final String rVar, final int[] data) {
		StringBuilder x = new StringBuilder(rVar + " <- c(");

		for(int i = 0; i < data.length; ++i) {
			x.append(data[i]);
			if (i != (data.length-1)) {
				x.append(",");
			}
		}

		x.append(")");
		return x.toString();
	}
}
