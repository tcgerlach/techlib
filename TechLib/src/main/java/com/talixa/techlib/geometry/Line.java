package com.talixa.techlib.geometry;

import com.talixa.techlib.shared.Vector2D;

public class Line {

	/**
	 * Create an array of points describing the segment from start to end containing 
	 * individual points equidistant from each other. 
	 * 
	 * NOTE: The size must be a power of 2 + 1 (3,5,9,17,33,etc)
	 * 
	 * @param start begin point for line
	 * @param end final point for line
	 * @param size number of points total
	 * @return array of points describing line
	 */
	public static Vector2D[] makeLineSegment(Vector2D start, Vector2D end, int size) {
		Vector2D[] line = new Vector2D[size];
		line[0] = start; 
		line[size-1] = end;
		splitLine(line, 0, size-1);
		return line;
	}
	
	// split a line into multiple parts
	// recursive function - each recursion splits from start to end
	private static void splitLine(Vector2D[] points, int start, int end){
		int midPoint = (start + end) / 2;
		points[midPoint] = findMidPoint(points[start], points[end]);
		if (midPoint % 2 == 0) {
			splitLine(points, start, midPoint);
			splitLine(points, midPoint, end);
		}
	}
	
	/**
	 * Calculate midpoint between two points
	 * @param p1 point 1
	 * @param p2 point 2
	 * @return midpoint
	 */
	public static Vector2D findMidPoint(Vector2D p1, Vector2D p2) {
		return new Vector2D((p1.getX() + p2.getX())/2, (p1.getY() + p2.getY())/2);
	}
			
	/**
	 * Return the intersection point between two line segments
	 * 
	 * @param l1s line 1 start point
	 * @param l1e line 1 end point
	 * @param l2s line 2 start point
	 * @param l2e line 2 end point
	 * @return intersection point
	 */
	public static Vector2D intersection(Vector2D l1s, Vector2D l1e, Vector2D l2s, Vector2D l2e) {
		return intersection(l1s.getX(), l1s.getY(), l1e.getX(), l1e.getY(), l2s.getX(), l2s.getY(), l2e.getX(), l2e.getY());
	}
	
	// determine intersection of two lines
	// points x1 and x2 define line1, points x3 and x4 define line2
	private static Vector2D intersection(float x1,float y1,float x2,float y2, float x3, float y3, float x4,float y4) {		 
		float d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
		if (Math.abs(d) < .000001) return null;
		float xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
		float yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;
 
	    Vector2D p = new Vector2D(xi,yi);
		if (xi < Math.min(x1,x2) || xi > Math.max(x1,x2)) return null;
		if (xi < Math.min(x3,x4) || xi > Math.max(x3,x4)) return null;
		return p;
	}
	
	/**
	 * Calculate the distance between two points on a line
	 * 
	 * @param p1 first point
	 * @param p2 second point
	 * @return scaler value representing the distance between the two points
	 */
	public static Double getDistance(Vector2D p1, Vector2D p2) {
		double x2 = Math.pow((p2.getX() - p1.getX()), 2);
		double y2 = Math.pow((p2.getY() - p1.getY()), 2);
		return Math.sqrt(x2 + y2);
	}
}
