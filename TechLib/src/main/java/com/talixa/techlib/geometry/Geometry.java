package com.talixa.techlib.geometry;

import com.talixa.techlib.shared.Vector2D;

public class Geometry {
	
	public static final float GOLDEN_RATIO = (float)((1d + Math.sqrt(5d))/2d);
	public static final float CIRCLE_RADIANS = (float)(2f  * Math.PI);
	
	/**
	 * If an object is at centerX and moves radius distance in radians direction, what is the new x?
	 * @param radians angle in radians
	 * @param radius distance
	 * @param centerX current center
	 * @return new X
	 */
	public static final float getXPoint(float radians, float radius, float centerX) {
		// x = centerX + radius * cos(radians)
		return (float)(centerX + radius * Math.cos(radians));
	}
	
	/**
	 * If an object is at centerY and moves radius distance in radians direction, what is the new y?
	 * @param radians angle in radians
	 * @param radius distance
	 * @param centerY current center
	 * @return new y
	 */
	public static final float getYPoint(float radians, float radius, float centerY) {
		// y = centerY + radius * sin(radians)
		return (float)(centerY + radius * Math.sin(radians));
	}
	
	/**
	 * On a regular polygon with the given number of points, what is the angle to the given point number?
	 * @param pointsPerPolygon how many points are in the polygon
	 * @param pointNumber which point number to calculate angle for
	 * @return angle to the given point number in radians
	 */
	public static final float getAngleOfPointInPolygon(int pointsPerPolygon, float pointNumber) {
		// Divide the circle by the number of points in the polygon and multiply by the point number
		return CIRCLE_RADIANS/pointsPerPolygon*pointNumber;
	}
	
	/**
	 * On a regular polygon, with the provided number of points and given radius, what is the x position of the new point?
	 * @param pointsPerPolygon number of points in the polygon
	 * @param pointNumber what point number in the polygon to find the x value for
	 * @param radius radius of the circle encompassing the polygon
	 * @param centerX center position of the object on the x axis
	 * @return x value for new polygon point
	 */
	public static final float getXPoint(int pointsPerPolygon, float pointNumber, float radius, float centerX) {
		return getXPoint(getAngleOfPointInPolygon(pointsPerPolygon, pointNumber), radius, centerX);
	}
	
	/**
	 * On a regular polygon, with the provided number of points and given radius, what is the y position of the new point?
	 * @param pointsPerPolygon number of points in the polygon
	 * @param pointNumber what point number in the polygon to find the Y value for
	 * @param radius radius of the circle encompassing the polygon
	 * @param centerY center position of the object on the Y axis
	 * @return Y value for new polygon point
	 */
	public static final float getYPoint(int pointsPerPolygon, float pointNumber, float radius, float centerY) {
		return getYPoint(getAngleOfPointInPolygon(pointsPerPolygon, pointNumber), radius, centerY);
	}
	
	/**
	 * Calculate the midpoint between two values.
	 * @param p1 value for first number
	 * @param p2 value for second number
	 * @return midpoint
	 */
	public static final float findMidPoint(float p1, float p2) {
		return (p1+p2)/2f;
	}
	
	/**
	 * Calculate the point for a polygon.
	 * @param pointsPerPolygon number of points in polygon 
	 * @param pointNumber point number to calculate
	 * @param radius size of bounding circle
	 * @param center point at the center of the polygon
	 * @return coordinates for new point
	 */
	public static final Vector2D getPolygonPoint(int pointsPerPolygon, float pointNumber, float radius, Vector2D center) {
		float x = getXPoint(pointsPerPolygon, pointNumber, radius, center.getX());
		float y = getYPoint(pointsPerPolygon, pointNumber, radius, center.getY());
		return new Vector2D(x, y);
	}
}
