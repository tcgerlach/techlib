package com.talixa.techlib.encoding;

import java.util.Arrays;

/**
 * Decoder for Baudot character set common in RTTY
 * 
 * @author tcgerlach
 */
public class BaudotDecoder {

	private static final char[] LETTERS = {'*','E','\10','A',' ','S'  ,'I','U','\13','D','R','J'  ,'N','F','C','K','T','Z','L','W','H','Y','P','Q','O','B','G','*','M','X','V','*'};
	private static final char[] US_NUM  = {'*','3','\10','-',' ','\07','8','7','\13','$','4','\'' ,',','!',':','(','5','"',')','2','#','6','0','1','9','?','&','*','.','/',';','*'};
	private static final char[] ITU_NUM = {'*','3','\10','-',' ','\'' ,'8','7','\13','*','4','\07',',','!',':','(','5','+',')','2','L','6','0','1','9','?','&','*','.','/','=','*'};
	public enum Mode {US, CCITT2, REV_US, REV_CCITT2};
	
	private byte[] data;
	private Mode mode;
	private long bitPosition = 0;
	
	/**
	 * Create a new Baudot decoder.
	 * @param data bytes of data to be decoded
	 * @param mode decoding mode
	 */
	public BaudotDecoder(byte[] data, Mode mode) {
		// make copy of data so it is not externally mutable
		this.data = Arrays.copyOf(data, data.length);
		this.mode = mode;		
	}

	/**
	 * Decode the Baudot data.
	 * @return string containing decoded data
	 */
	public String decode() {		
		boolean figures = false;
		
		StringBuilder baudot = new StringBuilder();
		// read data in 5 byte chunks (8 baudot chars)
		int value = getNextBaudotChar();
		while (value != -1) {
			if (value == 27) {
				figures = true;
			} else if (value == 31) {
				figures = false;
			} else {
				if (!figures) {
					baudot.append(LETTERS[value]);
				} else if (mode.equals(Mode.US) || mode.equals(Mode.REV_US)) {
					baudot.append(US_NUM[value]);
				} else {
					baudot.append(ITU_NUM[value]);
				}
			}
			value = getNextBaudotChar();
		}
		
		return baudot.toString();					
	}		
			
	private int getNextBaudotChar() {		
		int bytePosition = (int)(bitPosition / 8);
		int startBit = (int)(bitPosition % 8);
		boolean hasNextByte = bytePosition + 1 < data.length;
		int value = -1;
		
		if (startBit > 3 && !hasNextByte) {
			return -1;
		} 
		
		if (bytePosition >= data.length) {
			return -1;
		}
						
		switch(startBit) {
			case 0: value = data[bytePosition] >> 3 & 0x1f; break;
			case 1: value = data[bytePosition] >> 2 & 0x1f; break;
			case 2: value = data[bytePosition] >> 1 & 0x1f; break;
			case 3: value = data[bytePosition] >> 0 & 0x1f; break;
			case 4: value = data[bytePosition] << 1 & 0x1e | data[bytePosition+1] >> 7 & 0x01; break;
			case 5: value = data[bytePosition] << 2 & 0x1c | data[bytePosition+1] >> 6 & 0x03; break;
			case 6: value = data[bytePosition] << 3 & 0x18 | data[bytePosition+1] >> 5 & 0x07; break;
			case 7: value = data[bytePosition] << 4 & 0x10 | data[bytePosition+1] >> 4 & 0x0f; break;
			default: break;
		}
	
		bitPosition += 5;
		
		if (mode.equals(Mode.REV_CCITT2) || mode.equals(Mode.REV_US)) {				
			int newVal = (value & 0x01) << 4 |
					     (value & 0x02) << 2 |
					     (value & 0x04) << 0 |
					     (value & 0x08) >> 2 |
					     (value & 0x10) >> 4;
			value = newVal;
		}
				
		return value;
	}
}