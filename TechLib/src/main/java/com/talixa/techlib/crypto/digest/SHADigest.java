package com.talixa.techlib.crypto.digest;

/**
 * Digest implementation of SHA1 
 * 
 * @author tcgerlach
 */
public class SHADigest extends CryptoDigest {
	public SHADigest() {
		super("SHA");
	}	
}
