package com.talixa.techlib.crypto;

import com.talixa.techlib.crypto.cipher.CryptoCipher;
import com.talixa.techlib.crypto.cipher.NullCipher;
import com.talixa.techlib.crypto.cipher.RSA;
import com.talixa.techlib.crypto.cipher.StandardDES;
import com.talixa.techlib.crypto.cipher.TripleDES;
import com.talixa.techlib.crypto.cipher.parameters.AsymmetricCryptoParameters;
import com.talixa.techlib.crypto.cipher.parameters.CryptoParameters;
import com.talixa.techlib.crypto.cipher.parameters.SymmetricCryptoParameters;
import com.talixa.techlib.crypto.digest.CryptoDigest;
import com.talixa.techlib.crypto.digest.MD5Digest;
import com.talixa.techlib.crypto.digest.SHA256Digest;
import com.talixa.techlib.crypto.digest.SHA384Digest;
import com.talixa.techlib.crypto.digest.SHA512Digest;
import com.talixa.techlib.crypto.digest.SHADigest;
import com.talixa.techlib.crypto.stream.CryptoStream;
import com.talixa.techlib.crypto.stream.FibonacciGenerator;
import com.talixa.techlib.crypto.stream.GaloisGenerator;
import com.talixa.techlib.crypto.stream.NullGenerator;

/**
 * Interface go get various cryptography tools.
 * 
 * @author tcgerlach
 */
public class CryptoToolkit {
	
	public enum Cipher {NULL, DES, TRIPLE_DES, RSA};
	public enum Digest {MD5, SHA, SHA256, SHA384, SHA512};
	public enum Stream {FIBONACCI, GALOIS, NULL};
	
	/**
	 * Retrieve a crypto cipher.
	 * 
	 * @param cipher desired cipher algorithm
	 * @param password cipher password
	 * @return a CryptoCipher of the type specified by cipher
	 */
	public static CryptoCipher getCipher(Cipher cipher, CryptoParameters params) {
		CryptoCipher cryptoCipher;
				
		switch (cipher) {
			case TRIPLE_DES: 	cryptoCipher = new TripleDES((SymmetricCryptoParameters)params); break;
			case RSA:			cryptoCipher = new RSA((AsymmetricCryptoParameters)params); break;
			case NULL: 			cryptoCipher = new NullCipher(params); break;
			case DES:			// DES will be default cipher
			default:	 		cryptoCipher = new StandardDES((SymmetricCryptoParameters)params); break;
		}
		
		return cryptoCipher;
	}	
	
	/**
	 * Retrieve a crypto digest.
	 * 
	 * @param digest desired digest algorithm
	 * @return a CryptoDigest of the type specified by digest
	 */
	public static CryptoDigest getDigest(Digest digest) {
		CryptoDigest cryptoDigest;
				
		switch (digest) {
			case SHA: 		cryptoDigest = new SHADigest(); break;
			case SHA256:	cryptoDigest = new SHA256Digest(); break;
			case SHA384:	cryptoDigest = new SHA384Digest(); break;
			case SHA512:	cryptoDigest = new SHA512Digest(); break;
			case MD5: 		// MD5 will be the default
			default:		cryptoDigest = new MD5Digest(); break;
		}
		
		return cryptoDigest;
	}
	
	/**
	 * Retrieve a crypto stream.
	 * 
	 * @param stream desired stream algorithm
	 * @param seed initial fill of stream register
	 * @return a CryptoStream of the type specified by stream
	 */
	public static CryptoStream getStream(Stream stream, int seed) {
		CryptoStream cryptoStream;
		
		switch (stream) {
			case GALOIS: 	cryptoStream = new GaloisGenerator(seed); break;
			case NULL: 		cryptoStream = new NullGenerator(); break;
			case FIBONACCI: // This is default stream
			default:	 	cryptoStream = new FibonacciGenerator(seed); break;
		}
		
		return cryptoStream;		
	}
}
