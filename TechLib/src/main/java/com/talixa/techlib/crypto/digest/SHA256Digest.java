package com.talixa.techlib.crypto.digest;

/**
 * @author tcgerlach
 */
public class SHA256Digest extends CryptoDigest{
	public SHA256Digest() {
		super("SHA-256");
	}	
}
