package com.talixa.techlib.ai.general;

/**
 * Functions for calculating distance between two vectors.
 * 
 * @author tcgerlach
 */
public final class Distance {
	
	/**
	 * Calculate the Euclidean distance between two vectors.
	 * This would be the distance 'as the crow flies'
	 * @param v1 first vector
	 * @param v2 second vector
	 * @return distance between vectors
	 */
	public static float euclidean(final float[] v1, final float[] v2) {
		float sum = 0;
		for(int i = 0; i < v1.length; ++i) {
			sum += (v1[i] - v2[i]) * (v1[i] - v2[i]);
		}
		return (float)Math.sqrt(sum);
	}
	
	/**
	 * Calculate the Manhattan distance between two vectors.
	 * This would be distance taken by a taxi.
	 * @param v1 first vector
	 * @param v2 second vector
	 * @return distance between vectors
	 */
	public static float manhattan(final float[] v1, final float[] v2) {
		float sum = 0;
		for(int i = 0; i < v1.length; ++i) {
			sum += (float)Math.abs(v1[i] - v2[i]);
		}
		return sum;
	}
	
	/**
	 * Calculate the Chebyshev distance between two vectors.
	 * This would be distance taken the king on a chess board.
	 * @param v1 first vector
	 * @param v2 second vector
	 * @return distance between vectors
	 */
	public static float chebyshev(final float[] v1, final float[] v2) {
		float result = 0;
		for(int i = 0; i < v1.length; ++i) {
			float d = Math.abs(v1[i] - v2[i]);
			result = Math.max(d, result);
		}
		return result;
	}
	
	private Distance() {
		//  private constructor - static methods only
	}
}
