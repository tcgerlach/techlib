package com.talixa.techlib.ai.genetic.programming;

/**
 * Node of a tree for math calculations
 * 
 * @author tcgerlach
 */
public class TreeNode {

	private TreeNode left;
	private TreeNode right;
	private int opcode;
	
	/**
	 * Create an empty tree node
	 */
	public TreeNode() {
		super();
	}

	/**
	 * Create a tree node
	 * @param left node to left side
	 * @param right node to right side
	 * @param opcode operation code for node
	 */
	public TreeNode(TreeNode left, TreeNode right, int opcode) {
		super();
		this.left = left;
		this.right = right;
		this.opcode = opcode;
	}

	/**
	 * Retrieve the left node
	 * @return left child node
	 */
	public TreeNode getLeft() {
		return left;
	}
	
	/**
	 * Set the left node
	 * @param left new left node
	 */
	public void setLeft(TreeNode left) {
		this.left = left;
	}
	
	/**
	 * Retrieve the right node
	 * @return right child node
	 */
	public TreeNode getRight() {
		return right;
	}
	
	/**
	 * Set the right node
	 * @param right new right node
	 */
	public void setRight(TreeNode right) {
		this.right = right;
	}
	
	/** 
	 * Retrieve the operation code
	 * @return opcode of node
	 */
	public int getOpcode() {
		return opcode;
	}
	
	/**
	 * Set the operation for this node
	 * @param opcode new opcode
	 */
	public void setOpcode(int opcode) {
		this.opcode = opcode;
	}
}
