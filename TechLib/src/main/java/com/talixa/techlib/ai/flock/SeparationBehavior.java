package com.talixa.techlib.ai.flock;

import com.talixa.techlib.shared.Vector2D;

/**
 * Keep an amount of separation between boids within a group
 * @author tcgerlach
 */
public class SeparationBehavior extends Behavior {

	private float sepDistance;
	private float minPercent;
	private float maxPercent;
	
	/**
	 * Boid will try to maintain the desired distance between other boids in the group
	 * @param sepDistance desired distance
	 * @param minPercent min percent to move
	 * @param maxPercent max percent to move 
	 */
	public SeparationBehavior(float sepDistance, float minPercent, float maxPercent) {
		this.sepDistance = sepDistance;
		this.minPercent = minPercent;
		this.maxPercent = maxPercent;
	}
	
	@Override
	public void updateBoid(Boid boid) {
		findNearestBoidInGroup(boid.getGroup(), boid);	
		
		Vector2D desiredMoveAdj = Vector2D.subtractVectors(nearestBoid.getLocation(), boid.getLocation());
		
		// keep separation percent within range
		float separationPercent = distanceToNearest / sepDistance;
		if (separationPercent < minPercent) {
			separationPercent = minPercent;
		} else if (separationPercent > maxPercent) {
			separationPercent = maxPercent;
		}
		
		if (distanceToNearest < sepDistance) {
			// move away
			desiredMoveAdj.normalize();
			desiredMoveAdj.scale(-separationPercent);
			boid.getDesiredMove().addVector(desiredMoveAdj);
		} else if (distanceToNearest > sepDistance) {
			// move toward
			desiredMoveAdj.normalize();
			desiredMoveAdj.scale(separationPercent);
			boid.getDesiredMove().addVector(desiredMoveAdj);
		}
	}			
}
