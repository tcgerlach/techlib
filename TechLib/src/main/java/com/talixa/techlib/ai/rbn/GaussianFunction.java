package com.talixa.techlib.ai.rbn;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Gaussian function for use with RBF Network
 * 
 * @author tcgerlach
 */
public class GaussianFunction {

    private float[] params;		// The parameter vector.  Holds the RBF width and centers.
    private int indexWidth;		// index of widths
    private int indexCenters;	// index of centers
    private int dimensions;		// dimensions

    /**
     * Construct the RBF. RBF will require space equal to (dimensions + 1) in the params vector.
     *
     * @param dimensions The number of dimensions.
     * @param params     A vector to hold the paramaters.
     * @param index      The index into the params vector.  You can store multiple RBF's in a vector.
     */
    @SuppressFBWarnings(value="EI_EXPOSE_REP2", justification="Do not want a copy - params will be changed by RBFN")
    public GaussianFunction(int dimensions, float[] params, int index) {
        this.dimensions = dimensions;
        this.params = params;
        this.indexWidth = index;
        this.indexCenters = index + 1;
    }

    /**
     * Return the center point for the given dimension
     * @param dimension desired dimension
     * @return center point
     */
    public float getCenter(int dimension) {
        return this.params[indexCenters + dimension];
    }

    /**
     * Retrieve the number of dimensions for this gaussian
     * @return number of dimensions
     */
    public int getDimensions() {
        return this.dimensions;
    }

    /**
     * Return the width of the gaussian function
     * @return gaussian width
     */
    public float getWidth() {
        return this.params[indexWidth];
    }

    /**
     * Set the width of the gaussian
     * @param width new gaussian width
     */
    public void setWidth(float width) {
        this.params[indexWidth] = width;
    }

    /**
     * Set the center point for a given dimension
     * @param dimension index of desired dimension to change
     * @param value new dimension value
     */
    public void setCenter(int dimension, float value) {
        this.params[indexCenters + dimension] = value;
    }
    
    /**
     * Set all parameters for the gaussian
     * @param params new parameters
     */
    @SuppressFBWarnings(value="EI_EXPOSE_REP2", justification="Do not want a copy - params will be changed by RBFN")
    public void setParams(float[] params) {
    	this.params = params;
    }
    
    public float evaluate(final float[] x) {
        float value = 0;
        final float width = getWidth();

        for (int i = 0; i < getDimensions(); i++) {
            float center = this.getCenter(i);
            value += Math.pow(x[i] - center, 2) / (2.0 * width * width);
        }
        return (float)Math.exp(-value);
    }
}
