package com.talixa.techlib.ai.rbn;

import java.util.Arrays;
import java.util.Random;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Implementation of a Radial Basis Function (RBF) Network
 * 
 * @author tcgerlach
 */
public class RBFNetwork {
	
	private static Random rand = new Random();	// for setting random memory values in training
	
	private float[] memory;		// storage for RBFN data
	
	private int inputCount;		// number of inputs - data points - for model
	private int rbfCount;		// number of radial basis functions (1 additional will be used for bias)
	private int outputCount;	// number of output values from model
	
	private int inputWeightCount;	// number of input weights = number inputs * number rbf
	private int outputWeightCount;	// number of output weights = number of outputs * number of rbf
	
	private int rbfParamCount;			// number of (inputs + bias) * total RBFs
	private GaussianFunction[] rbfs;	// radial basis functions (gaussian)
	
	private int indexInputWeights = 0;	// index of memory where input weights are stored
	private int indexOutputWeights;		// index of memory where output weights are stored
	
	/**
	 * Create a Radial Basis Function Network.
	 * 
	 * @param inputVectorLength number of inputs for each item
	 * @param rbfCount number of radial basis functions to use
	 * @param outputVectorLength number of outputs for each item
	 */
	public RBFNetwork(int inputVectorLength, int rbfCount, int outputVectorLength) {
		this.inputCount = inputVectorLength;
		this.outputCount = outputVectorLength;
		this.rbfCount = rbfCount;
		this.inputWeightCount = this.inputCount * this.rbfCount;
		this.outputWeightCount = this.outputCount * (this.rbfCount + 1);	// add an additional rbf for bias
		this.rbfParamCount = (this.inputCount + 1) * this.rbfCount;
		this.indexOutputWeights = inputWeightCount + rbfParamCount;
		
		// create room to store input coefficients, output coefficients, and rbf parameters
		this.memory = new float[inputWeightCount + outputWeightCount + rbfParamCount];
		
		// create rbfs
		this.rbfs = new GaussianFunction[this.rbfCount];
		
		// init each rbf
		for(int i = 0; i < rbfCount; ++i) {
			// get the index for the rbf function
			int rbfIndex = inputWeightCount + ((inputCount + 1) * i);
			rbfs[i] = new GaussianFunction(inputCount, memory, rbfIndex);
		}
	}
	
	/**
	 * Compute the outputs from the network with the given inputs.
	 * 
	 * @param inputs list of input parameters
	 * @return output values from network with provided input
	 */
	public float[] computeNetwork(float[] inputs) {
		// create space for calculations of each rbf + an additional space for bias
		float[] rbfOutputs = new float[rbfCount+1];
		
		// set bias to 1
		rbfOutputs[rbfCount] = 1;
		
		// calculate each rbf
		for(int rbfIndex = 0; rbfIndex < rbfs.length; ++rbfIndex) {
			// first, weight each input
			float[] weightedInput = new float[inputs.length];
			for(int inputIndex = 0; inputIndex < inputs.length; ++inputIndex) {
				int memoryIndex = indexInputWeights + (rbfIndex * inputCount) + inputIndex;
				float weight = memory[memoryIndex];
				weightedInput[inputIndex] = inputs[inputIndex] * weight;
			}
			
			// now calculate rbf
			rbfOutputs[rbfIndex] = rbfs[rbfIndex].evaluate(weightedInput);
		}
		
		// calculate output
		float[] outputs = new float[outputCount];
		for(int outputIndex = 0; outputIndex < outputCount; ++outputIndex) {
			float sum = 0;
			for(int rbfIndex = 0; rbfIndex < rbfOutputs.length; ++rbfIndex) {
				int index = indexOutputWeights + (outputIndex * (rbfs.length + 1)) + rbfIndex;
				sum += rbfOutputs[rbfIndex] * memory[index];
			}
			outputs[outputIndex] = sum;
		}
		
		return outputs;
	}
	
	/**
	 * Retrieve the current memory of the network.
	 * Once the network is trained, this can be saved and used as the
	 * initial state for future classification or regression runs.
	 * @return
	 */
    @SuppressFBWarnings(value="EI_EXPOSE_REP", justification="Intentionaly exposing so other tools may directly access and modify")
	public float[] getMemory() {
		return memory;
	}
	
	/**
	 * Set the RBFN memory.
	 * @param memory input memory values
	 */
	public void setMemory(float[] memory) {
		this.memory = Arrays.copyOf(memory, memory.length);
		for(int i = 0; i < rbfs.length; ++i) {
			rbfs[i].setParams(memory);
		}
	}
	
	/**
	 * Set all memory registers to a random value between -20 and +20
	 */
	public void randomizeMemory() {
		for(int i = 0; i < memory.length; ++i) {
			memory[i] = rand.nextFloat() * 20 - 10;
		}
	}
}
