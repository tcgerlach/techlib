package com.talixa.techlib.ai.flock;

import com.talixa.techlib.shared.Vector2D;

/**
 * Behavior to keep boids within a given range
 * @author tcgerlach
 */
public class StayWithinCircleBehavior extends Behavior {

	private Vector2D center;
	private float radius;
	
	/**
	 * Create a behavior for a boid to stay within a given area.
	 * @param center target area center point
	 * @param radius range from center point boid can travel
	 */
	public StayWithinCircleBehavior(Vector2D center, float radius) {
		this.center = center;
		this.radius = radius;
	}
	
	@Override
	public void updateBoid(Boid b) {
		float distance = center.getDistance(b.getLocation());
		Vector2D toCenter = Vector2D.subtractVectors(center, b.getLocation());
		if (distance > radius) { 
			Vector2D desiredMoveAdj = Vector2D.scaleVector(toCenter, 1/distance);
			desiredMoveAdj.normalize();			
			desiredMoveAdj.scale(b.getMaxSpeed());	// return as quickly as possible
			b.getDesiredMove().addVector(desiredMoveAdj);
		}
	}
}
