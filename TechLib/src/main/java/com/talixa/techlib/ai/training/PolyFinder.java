package com.talixa.techlib.ai.training;

import com.talixa.techlib.ai.general.Errors;
import com.talixa.techlib.math.Polynomial;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Algorithm to find a poly using machine learning
 * 
 * For example, will calculate A, B, and C for the function:
 * f(x) = Ax^2 + Bx + C
 * 
 * @author tcgerlach
 */
public abstract class PolyFinder {
	
	protected float[] input;				// training input data
	protected float[] idealOutput;			// ideal output from model	
	protected float[] actualOutput;			// actual value calculated
	
	protected float[] bestCoefficients;		// a,b,c... values for poly
	protected int degree;					// degree for polynomial
	
	/**
	 * Initialize with training data and expected output from poly.
	 * 
	 * @param trainingInput input value for x
	 * @param idealOutput output value from poly function
	 * @param degree polynomial degree
	 */
    @SuppressFBWarnings(value="EI_EXPOSE_REP2", justification="Do not want a copy - data may be large")
	public PolyFinder(float[] trainingInput, float[] idealOutput, int degree) {
		this.input = trainingInput;
		this.idealOutput = idealOutput;
		this.actualOutput = new float[idealOutput.length];
		this.bestCoefficients = new float[degree+1];
		this.degree = degree;
	}

    /**
     * Determine coefficients for poly.
     * @param numIterations maximum number of iterations of algorithm
     * @return best fit for poly coefficients
     */
    public abstract float[] getCoefficients(int maxIterations);
	
	/**
	 * Calculate the score with the given coefficients against the training data
	 * @param coefficients list of coefficients to test
	 * @return sum of squares error for data
	 */
	protected float calculateScore(float[] coefficients) {
		for(int i = 0; i < input.length; ++i) {
			actualOutput[i] = Polynomial.calculate(input[i], coefficients);
		}
		return Errors.sumOfSquares(idealOutput, actualOutput);
	}
}
