package com.talixa.techlib.ai.general;

/**
 * Error calculation functions.
 * 
 * @author tcgerlach
 */
public final class Errors {
	
	/**
	 * Calculate the sum of squares error of an algorithm.
	 * @param expected values expected from algorithm
	 * @param actual values actually produced from algorithm
	 * @return sum of squares of predictions
	 */
	public static float sumOfSquares(final float[] expected, final float[] actual) {
		float sum = 0;
		for(int i = 0; i < expected.length; ++i) {
			sum += Math.pow(expected[i] - actual[i], 2);
		}
		return sum;
	}
	
	/**
	 * Calculate the mean squared error of an algorithm.
	 * @param expected values expected from algorithm
	 * @param actual values actually produced from algorithm
	 * @return mean squared error of predictions
	 */
	public static float meanSquared(final float[] expected, final float[] actual) {
		return sumOfSquares(expected, actual)/expected.length;
	}
	
	/**
	 * Calculate the root mean squared error of an algorithm.
	 * @param expected values expected from algorithm
	 * @param actual values actually produced from algorithm
	 * @return root mean squared error of predictions
	 */
	public static float rootMeanSquared(final float[] expected, final float[] actual) {
		return (float)Math.sqrt(meanSquared(expected,actual));
	}
	
	private Errors() {
		// private constructor - static methods only
	}
}
