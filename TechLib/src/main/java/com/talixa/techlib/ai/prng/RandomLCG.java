package com.talixa.techlib.ai.prng;

import java.util.Random;

/**
 * Standard Linear Congruential Generator
 * X = (aX + c) mod m
 * m = 2e31 (2,147,483,648)
 * a = 1,103,515,245
 * c = 12345
 * 
 * https://en.wikipedia.org/wiki/Linear_congruential_generator
 * Chose values that would not cause sign problems w/ Java
 * 
 * Ran 1 billion iterations without repeating numbers.
 * 
 * @author tcgerlach
 */
public class RandomLCG {

	// Use Random for nextInt(int max) since this 
	// LCG does not provide well randomized ints
	// within a small range.
	private static Random rand = new Random();
	
	private static final long m = 2147483648l;
	private static final long a = 1103515245l;
	private static final long c = 12345l;
	
	private static long value = Long.MAX_VALUE % m;
	
	/**
	 * Seed the random number generator
	 * @param s starting seed
	 */
	public static void seed(long s) {
		value = s % m;
		rand.setSeed(s);
	}
	
	/**
	 * Return a random integer in the range (0,2147483648]
	 * @return random number
	 */
	public static int getNextInt() {
		value = ((a * value + c) % m);
		if (value < 0) {
			value *= -1;
		}
		return (int)value;
	}
	
	/**
	 * Return a random integer less than max
	 * 
	 * @param max maximum value for int
	 * @return random integer
	 */
	public static int getNextInt(int max) {
		return rand.nextInt(max);
	}
	
	/**
	 * Return a random float in the range [0,1]
	 * 
	 * @return random number
	 */
	public static float getNextFloat() {
		float x = getNextInt();
		return x / (float)m;
	}
	
	/**
	 * Return a random float in the range [0,X]
	 * 
	 * @return random number
	 */
	public static float getNextFloat(float max) {
		float x = getNextFloat();
		// now scale - since x can be considered a percentage,
		// we can simply take x percent of max thus returning
		// a number between - and max
		return x * max;
	}
	
	/**
	 * Return the max value of this generator
	 * 
	 * @return max value
	 */
	public static long getMaxValue() {
		return m;
	}
}
