package com.talixa.techlib.ai.flock;

import java.util.List;

import com.talixa.techlib.shared.Vector2D;

/**
 * Base class for all boid behaviors
 * @author tcgerlach
 */
public abstract class Behavior {

	protected float distanceToNearest;
	protected Boid nearestBoid;
	
	public abstract void updateBoid(Boid b);
	
	/**
	 * Find the distance to the nearest boid within a given group
	 * @param group list of boids to search
	 * @param me reference to boid lookinf for nearest neighbor
	 */
	protected void findNearestBoidInGroup(List<Boid> group, Boid me) {
		float minDistance = 99999;
		
		for(Boid otherBoid : group) {
			// ignore self
			if (!me.equals(otherBoid)) {
				float distance = me.getLocation().getDistance(otherBoid.getLocation());
				if (distance < minDistance) {
					minDistance = distance;
					nearestBoid = otherBoid;
				}				
			}
		}	
		distanceToNearest = minDistance;
	}
	
	/**
	 * Determine the center position of a group of boids
	 * @param group list of boids to calculate center of
	 * @return center position of group
	 */
	protected Vector2D getPositionOfCenter(List<Boid> group) {
		float totalX = 0;
		float totalY = 0;
		float groupSize = group.size();
		
		// check for divide by 0
		if (groupSize == 0) {
			return new Vector2D(0,0);
		}
		
		for(Boid other : group) {
			totalX += other.getLocation().getX();
			totalY += other.getLocation().getY();
		}
		
		return new Vector2D(totalX/groupSize, totalY/groupSize);
	}
	
	/**
	 * Determine the average velocity of the members of a boid group
	 * @param group list of boids to examine
	 * @return velocity of group
	 */
	protected Vector2D getAverageVelocity(List<Boid> group) {
		// save as center position, but using velocity 
		float totalX = 0;
		float totalY = 0;
		float groupSize = group.size();
		
		// check for divide by 0
		if (groupSize == 0) {
			return new Vector2D(0,0);
		}
		
		for(Boid other : group) {
			totalX += other.getVelocity().getX();
			totalY += other.getVelocity().getY();
		}
		
		return new Vector2D(totalX/groupSize, totalY/groupSize);
	}
}
