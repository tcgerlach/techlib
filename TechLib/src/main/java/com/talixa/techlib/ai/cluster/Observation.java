package com.talixa.techlib.ai.cluster;

import java.util.Arrays;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * One set of data observation points.
 * 
 * @author tcgerlach
 */
public class Observation {

	private float[] data;
	
	/**
	 * Create a new set of observation data.
	 * @param data list of data points for the observation
	 */
	@SuppressFBWarnings(value="EI_EXPOSE_REP2", justification="May be using large number of data points, don't want to copy")
	public Observation(float[] data) {
		this.data = data;
	}

	/**
	 * Retrieve the data points for this observation.
	 * @return data points
	 */
	@SuppressFBWarnings(value="EI_EXPOSE_REP", justification="May be using large number of data points, don't want to copy")
	public float[] getDataPoints() {
		return data;
	}

	/**
	 * Set the data points for the observation.
	 * @param data set of new data points
	 */
	@SuppressFBWarnings(value="EI_EXPOSE_REP2", justification="May be using large number of data points, don't want to copy")
	public void setDataPoints(float[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Observation [data=" + Arrays.toString(data) + "]";
	}
}
