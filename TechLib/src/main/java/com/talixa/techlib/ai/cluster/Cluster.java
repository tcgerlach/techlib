package com.talixa.techlib.ai.cluster;

import java.util.ArrayList;
import java.util.List;

/**
 * One data cluster for the k-means algorithm.
 * 
 * @author tcgerlach
 */
public class Cluster {

	private Observation centroid;
	private List<Observation> observations;
	
	/**
	 * Create a new - empty - cluster
	 */
	public Cluster() {
		observations = new ArrayList<Observation>();
	}
	
	/** 
	 * Retrieve the centroid object for the cluster.
	 * @return mean observation for cluster
	 */
	public Observation getCentroid() {
		return centroid;
	}
	
	/**
	 * Set a new centroid for the cluster.
	 * @param centroid new centroid
	 */
	public void setCentroid(Observation centroid) {
		this.centroid = centroid;
	}
	
	/**
	 * Retrieve all observations in this cluster.
	 * @return list of observations
	 */
	public List<Observation> getObservations() {
		return observations;
	}
	
	/**
	 * Set a new list of observations for this cluster.
	 * @param observations new observation
	 */
	public void setObservations(List<Observation> observations) {
		this.observations = observations;
	}

	@Override
	public String toString() {
		return "Cluster [centroid=" + centroid + ", observations=" + observations + "]";
	}
}
