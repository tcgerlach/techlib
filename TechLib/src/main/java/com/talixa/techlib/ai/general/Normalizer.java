package com.talixa.techlib.ai.general;

import com.talixa.techlib.data.Statistics;

/**
 * Functions for data normalization.
 * 
 * @author tcgerlach
 */
public final class Normalizer {
	
	// numbers will be in the range -1 to 1
	private static final float NORMALIZE_RANGE = 2f;
	private static final float NORMALIZE_LOW_VALUE = -1;

	/**
	 * Normalize a vector of ordinal data. 
	 * Ordinal data is data that represents ranking of some manner.
	 * Examples may be grade (values from preschool to 12th grade)
	 * Class of CPU in a computer from 8086 through 686
	 * Other qualitative data that breaks things down into categories
	 * from one extreme to another that can be ranked in an order.
	 * @param inputVector input ordinal values 
	 * @param possibleValues number of possible values for the ordinal
	 * @return normalized data in range from -1 to 1
	 */
	public static float[] normalizeOrdinalVector(final int[] inputVector, final int possibleValues) {
		float[] normalizedData = new float[inputVector.length];
		for(int i = 0; i < inputVector.length; ++i) {
			normalizedData[i] = (inputVector[i] * NORMALIZE_RANGE) / (possibleValues-1) + NORMALIZE_LOW_VALUE;
		}
		return normalizedData;
	}
	
	/**
	 * Denormalize a vector of normalized data
	 * @param normalizedVector normalized input values in range -1 to 1
	 * @param possibleValues number of ordinal positions in output
	 * @return denormalized ordinal data
	 */
	public static int[] denormalizeOrdinalVector(final float[] normalizedVector, final int possibleValues) {
		int[] denormalizedData = new int[normalizedVector.length];
		
		// save this constant
		float rOverP = ((float)(NORMALIZE_RANGE)) / ((float)(possibleValues-1));
		
		for(int i = 0; i < normalizedVector.length; ++i) {
			// solve normalize function for y
			denormalizedData[i] = (int)(1f / (rOverP * 1f/(normalizedVector[i] - NORMALIZE_LOW_VALUE)));
		}
		return denormalizedData;
	}
	
	/**
	 * Normalize data in the range of minVal to maxVal into a range -1 to 1.
	 * @param inputVector data to normalize
	 * @param minVal minimum possible value of data
	 * @param maxVal maximum possible value of data
	 * @return normalized data set
	 */
	public static float[] normalizeData(final float[] inputVector, final float minVal, final float maxVal) {
		float[] normalizedData = new float[inputVector.length];
		float dataRange = maxVal - minVal;
		for(int i = 0; i < inputVector.length; ++i) {
			float d = inputVector[i] - minVal;			// subtract min value
			float percent = d / dataRange;				// determine percent of max value
			float dnorm = NORMALIZE_RANGE * percent;	// determine a normalized value
			float norm = NORMALIZE_LOW_VALUE + dnorm;	// add to the min value
			normalizedData[i] = norm;					// add to normalized array
		}
		return normalizedData;
	}
	
	/**
	 * Denormalize an input data set.
	 * @param normalizedData normalized data to be denormalized
	 * @param minVal minimum possible value for data
	 * @param maxVal maximum possible data value
	 * @return denormalized data set
	 */
	public static float[] denormalizeData(final float[] normalizedData, final float minVal, final float maxVal) {
		float[] denormalizedData = new float[normalizedData.length];
		float dataRange = maxVal - minVal;
		for(int i = 0; i < normalizedData.length; ++i) {
			float dist = normalizedData[i] - NORMALIZE_LOW_VALUE;	// determine distance from min
			float pct = dist / NORMALIZE_RANGE;						// calculate percent of total range
			float dnorm = pct * dataRange;							// determine value
			denormalizedData[i] = dnorm + minVal;					// add min value
		}
		return denormalizedData;
	}
	
	/**
	 * Normalize numbers by using the reciprocal.
	 * NOTE: This will only work for numbers greater than 1 or less than -1.
	 * @param inputVector input data for normalization
	 * @return normalized data
	 */
	public static float[] reciprocalNormalization(final float[] inputVector) {
		float[] normalized = new float[inputVector.length];
		
		for(int i = 0; i < inputVector.length; ++i) {
			normalized[i] = 1f / inputVector[i];
		}
		
		return normalized;
	}
	
	/**
	 * Denormalizes the input data by using the reciprocal. 
	 * @param normalizedVector input data that is normalized
	 * @return denormalized data
	 */
	public static float[] reciprocalDenormalization(final float[] normalizedVector) {
		return reciprocalNormalization(normalizedVector);
	}
	
	/**
	 * Calculate the Z-Scores for an array of numbers
	 * @param input list of numbers
	 * @return z-scores corrosponding to those numbers
	 */
	public static float[] calculateZScores(final float[] input) {
		float mean = Statistics.mean(input);
		float stddev = Statistics.standardDeviation(input);
		
		float[] zScores = new float[input.length];
		for(int i = 0; i < input.length; ++i) {
			zScores[i] = (input[i] - mean) / stddev;
		}
		return zScores;
	}
	
	private Normalizer() {
		// private constructor - static methods only
	}
}
