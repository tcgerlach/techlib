package com.talixa.techlib.ai.tsp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.talixa.techlib.ai.genetic.algorithms.Mutation;
import com.talixa.techlib.ai.genetic.algorithms.Selection;
import com.talixa.techlib.ai.prng.RandomLCG;
import com.talixa.techlib.ai.shared.OffspringPair;
import com.talixa.techlib.ai.shared.ScoredObjectVector;
import com.talixa.techlib.geo.DistanceCalculator;
import com.talixa.techlib.geo.data.City;

public class TravelingSalesmanProblem {
	
	private static boolean debugging = false;
	
	private static final int STARTING_PATH_COUNT = 1024;
	private static final int GENERATIONS = 512;
	
	/**
	 * Solve the traveling salesman problem (TSP) for the list of cities provided.
	 * This algorithm uses genetic artificial intelligence algorithms to find
	 * an optimal solution.  Initially, 1024 random paths are created from the input
	 * data. Each path is scored, and paths are ranked by the score. The best scoring
	 * algorithm survives to the next generation. Then, the remaining top 50% are
	 * mutated and placed in the next generation as well. Finally, randomly selected
	 * members of the entire pool are selected to be spliced together to create 
	 * children. These children are then placed in the next generation. All three 
	 * groups - the highest ranking, mutated, and spliced, are created for each
	 * generation. After the next generation is created, the process begins again
	 * with scoring, ranking, then determining the next generation. This process
	 * continues for 512 generations at which time the algorithm returns the
	 * best scoring path.
	 * 
	 * @param citiesToVisit array of all cities to visit
	 * @return cities placed in the order they should be visited
	 */
	@SuppressWarnings("unchecked")
	public static City[] solve(City[] citiesToVisit) {
		RandomLCG.seed(System.currentTimeMillis());
		
		float bestScore = Float.MAX_VALUE;
		
		// start by creating starting paths					
		City[][] startingPaths = new City[STARTING_PATH_COUNT][];
		for(int i = 0; i < STARTING_PATH_COUNT; ++i) {
			startingPaths[i] = Arrays.copyOf(citiesToVisit, citiesToVisit.length);
			startingPaths[i] = Mutation.shuffle(startingPaths[i], startingPaths[i].length);
		}
		
		// now created scored vectors
		List<ScoredObjectVector<City>> population = new ArrayList<ScoredObjectVector<City>>();
		for(int i = 0; i < STARTING_PATH_COUNT; ++i) {
			float score = (float)DistanceCalculator.routeDistance(startingPaths[i]);
			ScoredObjectVector<City> sov = new ScoredObjectVector<City>(startingPaths[i], score);
			population.add(sov);
		}
		
		// iterate through generations
		for(int i = 0; i < GENERATIONS; ++i) {
			// order population by score
			Collections.sort(population);
			
			if (population.get(0).getScore() < bestScore && debugging) {
				bestScore = population.get(0).getScore();
				System.out.println("New Best Score: " + bestScore);
			}
			
			// next generation will include 
			// 1) top item unchanged (elitism)
			// 2) 50% of items mutated 
			// 3) remainder of items spliced (no repeat) by random selection
			List<ScoredObjectVector<City>> newPopulation = new ArrayList<ScoredObjectVector<City>>();
			
			// top scorer
			newPopulation.add(population.get(0));
			
			// 50% mutations
			for(int j = 0; j < (population.size() / 2) -1; ++j)  {
				City[] path = population.get(j).getData();						// get data
				Mutation.shuffle(path, path.length/3);							// mutate
				float score = (float)DistanceCalculator.routeDistance(path);	// get score
				newPopulation.add(new ScoredObjectVector<City>(path, score));
			}
			
			// remainder spliced - iterating through a number of times
			// equal to 1/4 of the population will use 1/2 the population
			// and generate 1/2 the population in children
			for(int j = 0; j < population.size() / 4; ++j) {
				// select 2 parents from the entire population
				ScoredObjectVector<City> p1 = (ScoredObjectVector<City>)Selection.truncation(population, 1);
				ScoredObjectVector<City> p2 = (ScoredObjectVector<City>)Selection.truncation(population, 1);
				
				// mutate - no repeat splice
				OffspringPair<City> children = Mutation.spliceNoRepeat(p1.getData(), p2.getData(), citiesToVisit.length/3);
				
				// score children
				float score1 = (float)DistanceCalculator.routeDistance(children.getChild1());
				float score2 = (float)DistanceCalculator.routeDistance(children.getChild2());
				
				// add children to population
				newPopulation.add(new ScoredObjectVector<City>(children.getChild1(), score1));
				newPopulation.add(new ScoredObjectVector<City>(children.getChild2(), score2));
			}
			
			// new population takes over
			population = newPopulation;
		}
		
		// order population by score
		Collections.sort(population);
		
		// return highest scoring path
		return population.get(0).getData();
	}
	
	/**
	 * Enable or disable debugging information.
	 * 
	 * @param enable true to enable debugging
	 */
	public static void setDebugging(boolean enable) {
		debugging = enable;
	}
	
	/**
	 * Return the value of the debugging flag.
	 * 
	 * @return true if debugging enabled
	 */
	public static boolean isDebuggingEnabled() {
		return debugging;
	}
}
