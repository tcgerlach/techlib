package com.talixa.techlib.ai.flock;

import com.talixa.techlib.shared.Vector2D;

/**
 * This behavior works to keep all boids facing in the same direction
 * @author tcgerlach
 */
public class AlignmentBehavior extends Behavior {

	private float turnRate;
	
	/**
	 * Keep boids in a group going in the same direction
	 * @param turnRate max rate of turn
	 */
	public AlignmentBehavior(float turnRate) {
		this.turnRate = turnRate;
	}
	
	@Override
	public void updateBoid(Boid boid) {				
		Vector2D desiredMoveAdj = getAverageVelocity(boid.getGroup());
		desiredMoveAdj.normalize();
		desiredMoveAdj.scale(turnRate);
		boid.getDesiredMove().addVector(desiredMoveAdj);
	}
}
