package com.talixa.techlib.ai.genetic.algorithms;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.talixa.techlib.ai.shared.OffspringPair;
import com.talixa.techlib.ai.prng.RandomLCG;

/**
 * Functions for vector mutation.
 * 
 * @author tcgerlach
 */
public class Mutation {
	
	/**
	 * Mutate the vector by making small changes to each element
	 * 
	 * @param input list of elements to change
	 * @param maxChangePercent maximum percentage of change
	 * @return mutated input data
	 */
	public static float[] perturb(final float[] input, final int maxChangePercent) {
		float[] output = Arrays.copyOf(input, input.length);
		
		for(int i = 0; i < input.length; ++i) {
		 	// number between -maxChangePercent and maxChangePercent as percent
			float percent = (RandomLCG.getNextInt(maxChangePercent * 2) - maxChangePercent) / 100f; 
			
			// apply change
			float delta = output[i] * percent;
			output[i] += delta;
		}
		
		return output;
	}
	
	/**
	 * Mutate the vector by changing the order of the elements
	 * 
	 * @param input list of elements to change
	 * @param flips number of items to flip
	 * @return mutated input data
	 */
	public static float[] shuffle(final float[] input, final int flips) {
		float[] output = Arrays.copyOf(input, input.length);
		
		for (int i = 0; i < flips; ++i) {
			// find two random items to flip
			int index1 = RandomLCG.getNextInt(input.length);
			int index2 = RandomLCG.getNextInt(input.length);
			
			// don't want both indices to be identical
			while (index2 == index1) {
				index2 = RandomLCG.getNextInt(input.length);
			}
			
			float temp = output[index2];
			output[index2] = output[index1];
			output[index1] = temp;
	    }
		
		return output;
	}
	
	/**
	 * Splices data from both parents to create children.
	 * A cut will be made at a random point in each parent and a number 
	 * of elements equal to cutLength will be copied from the other parent.
	 * 
	 * @param parent1 first parent to get data from
	 * @param parent2 second parent to get data from
	 * @param cutLength length of splice
	 * @return pair of children produced from splice
	 */
	public static float[][] splice(final float[] parent1, final float[] parent2, final int cutLength) {
		// create empty child objects
		float[] child1 = new float[parent1.length];
		float[] child2 = new float[parent2.length];
		
		// determine cut points for splice
		int cutPoint1 = RandomLCG.getNextInt(parent1.length - cutLength);
		int cutPoint2 = cutPoint1 + cutLength;
		
		// handle splice
		for(int i = 0; i < parent1.length; ++i) {
			if (i >= cutPoint1 && i < cutPoint2) {
				// if between cutpoints, splice data
				child1[i] = parent2[i];
				child2[i] = parent1[i];
			} else {
				// otherwise copy data
				child1[i] = parent1[i];
				child2[i] = parent2[i];
			}
		}
		
		return new float[][] {child1,child2};
	}
	
	/**
	 * Splices data from both parents to create children.
	 * A cut will be made at a random point in each parent and a number 
	 * of elements equal to cutLength will be copied from the other parent.
	 * No data elements will be duplicated in either child.
	 * 
	 * @param parent1 first parent to get data from
	 * @param parent2 second parent to get data from
	 * @param cutLength length of splice
	 * @return pair of children produced from splice
	 */
	public static float[][] spliceNoRepeat(final float[] parent1, final float[] parent2, final int cutLength) {
		// create empty child objects
		float[] child1 = new float[parent1.length];
		float[] child2 = new float[parent2.length];
		
		Map<Float,Boolean> used1 = new HashMap<Float,Boolean>();
		Map<Float,Boolean> used2 = new HashMap<Float,Boolean>();
		
		// determine cut points for splice
		int cutPoint1 = RandomLCG.getNextInt(parent1.length - cutLength);
		int cutPoint2 = cutPoint1 + cutLength;
		
		// handle splice middle
		for(int i = 0; i < parent1.length; ++i) {
			if (i >= cutPoint1 && i < cutPoint2) {
				// if between cutpoints, splice data
				child1[i] = parent2[i];
				child2[i] = parent1[i];
				
				// mark items used
				used2.put(parent1[i], true);
				used1.put(parent2[i], true);
			} 
		}
		
		// handle splice ends
		for(int i = 0; i < parent1.length; ++i) {
			if (i < cutPoint1 || i >= cutPoint2) {
				// otherwise copy data
				child1[i] = getUnused(parent1,used1);
				child2[i] = getUnused(parent2,used2);
			}
		}
		
		return new float[][] {child1,child2};
	}
	
	private static float getUnused(final float[] source, Map<Float,Boolean> used) {
		for(float f : source) {
			if (!used.containsKey(f)) {
				used.put(f, true);
				return f;
			}
		}
		return -1;
	}
	
	//**********************************
	// Versions of the above for objects
	//**********************************
	
	/**
	 * Mutate the vector by changing the order of the elements.
	 * 
	 * @param input list of elements to change
	 * @param flips number of items to flip
	 * @return mutated input data
	 */
	public static<T> T[] shuffle(final T[] input, final int flips) {
		T[] output = Arrays.copyOf(input, input.length);
		
		for (int i = 0; i < flips; ++i) {
			// find two random items to flip
			int index1 = RandomLCG.getNextInt(input.length);
			int index2 = RandomLCG.getNextInt(input.length);
			
			// don't want both indices to be identical
			while (index2 == index1) {
				index2 = RandomLCG.getNextInt(input.length);
			}
			
			T temp = output[index2];
			output[index2] = output[index1];
			output[index1] = temp;
	    }
		
		return output;
	}
	
	/**
	 * Splices data from both parents to create children.
	 * A cut will be made at a random point in each parent and a number 
	 * of elements equal to cutLength will be copied from the other parent.
	 * 
	 * @param parent1 first parent to get data from
	 * @param parent2 second parent to get data from
	 * @param cutLength length of splice
	 * @return pair of children produced from splice
	 */
	public static<T> OffspringPair<T> splice(final T[] parent1, final T[] parent2, final int cutLength) {
		// create child arrays - must copy since Java doesn't allow 
		// creating arrays of type <T>
		T[] child1 = Arrays.copyOf(parent1, parent1.length);
		T[] child2 = Arrays.copyOf(parent2, parent2.length);
		
		// determine cut points for splice
		int cutPoint1 = RandomLCG.getNextInt(parent1.length - cutLength);
		int cutPoint2 = cutPoint1 + cutLength;
		
		// handle splice
		for(int i = 0; i < parent1.length; ++i) {
			if (i >= cutPoint1 && i < cutPoint2) {
				// if between cutpoints, splice data
				child1[i] = parent2[i];
				child2[i] = parent1[i];
			} else {
				// otherwise copy data
				child1[i] = parent1[i];
				child2[i] = parent2[i];
			}
		}
		
		return new OffspringPair<T>(child1,child2);
	}
	
	/**
	 * Splices data from both parents to create children.
	 * A cut will be made at a random point in each parent and a number 
	 * of elements equal to cutLength will be copied from the other parent.
	 * No data elements will be duplicated in either child.
	 * 
	 * @param parent1 first parent to get data from
	 * @param parent2 second parent to get data from
	 * @param cutLength length of splice
	 * @return pair of children produced from splice
	 */
	public static<T> OffspringPair<T> spliceNoRepeat(final T[] parent1, final T[] parent2, final int cutLength) {
		// create child arrays - must copy since Java doesn't allow 
		// creating arrays of type <T>
		T[] child1 = Arrays.copyOf(parent1, parent1.length);
		T[] child2 = Arrays.copyOf(parent2, parent2.length);
		
		Map<T,Boolean> used1 = new HashMap<T,Boolean>();
		Map<T,Boolean> used2 = new HashMap<T,Boolean>();
		
		// determine cut points for splice
		int cutPoint1 = RandomLCG.getNextInt(parent1.length - cutLength);
		int cutPoint2 = cutPoint1 + cutLength;
		
		// handle splice middle
		for(int i = 0; i < parent1.length; ++i) {
			if (i >= cutPoint1 && i < cutPoint2) {
				// if between cutpoints, splice data
				child1[i] = parent2[i];
				child2[i] = parent1[i];
				
				// mark items used
				used2.put(parent1[i], true);
				used1.put(parent2[i], true);
			} 
		}
		
		// handle splice ends
		for(int i = 0; i < parent1.length; ++i) {
			if (i < cutPoint1 || i >= cutPoint2) {
				// otherwise copy data
				child1[i] = getUnused(parent1,used1);
				child2[i] = getUnused(parent2,used2);
			}
		}
		
		return new OffspringPair<T>(child1,child2);
	}
	
	private static<T> T getUnused(final T[] source, Map<T,Boolean> used) {
		for(T t : source) {
			if (!used.containsKey(t)) {
				used.put(t, true);
				return t;
			}
		}
		return null;
	}
	
	private Mutation() {
		// private constructor - static methods only
	}
}
