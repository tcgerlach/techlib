package com.talixa.techlib.ai.shared;

import java.util.List;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A class representing a scored vector. Fields include 
 * the vector and the score of the vector. This can be 
 * used for AI algorithms that require algorithm scoring.
 * 
 * The data vector type will be determined by the subclass
 * 
 * @author tcgerlach
 */
public abstract class ScoredVector implements Comparable<ScoredVector> {
	
	protected float score;
	
    /**
     * Retrieve the score associated with this vector.
     * 
     * @return vector score
     */
	public float getScore() {
		return score;
	}

	@Override
    @SuppressFBWarnings(value="EQ_COMPARETO_USE_OBJECT_EQUALS", justification="Only need compareTo to sort data, equals not needed")
	public int compareTo(ScoredVector o) {
		return Double.compare(this.getScore(), o.getScore());
	}
	
	/**
	 * Some algorithms expect low score to be good, others want high score.
	 * This function will switch high and low values. 
	 * First, highest value and lowest value will be determined.
	 * Then, new values will be assigned as 
	 * newScore = maxScore - myScore + minScore
	 * The new numbers will be in the same range, just reversed.
	 * @param population list of all population members
	 */
	public static void reverseScores(List<ScoredVector> population) {
		// first, identify high & low score
		float highScore = 0;
		float lowScore = Float.MAX_VALUE;
		for(ScoredVector v : population) {
			if (v.getScore() > highScore) {
				highScore = v.getScore();
			}
			if (v.getScore() < lowScore) {
				lowScore = v.getScore();
			}
		}
		
		// now, update every element
		for(ScoredVector v : population) {
			v.score = highScore - v.score + lowScore;
		}
	}
	
	/**
	 * Return the total score of the entire population
	 * @param population input list of population
	 * @return sum of all members of population
	 */
	public static float getPopulationScore(List<ScoredVector> population) {
		float totalScore = 0;
		for(ScoredVector v : population) {
			totalScore += v.getScore();
		}
		return totalScore;
	}
}
