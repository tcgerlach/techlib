package com.talixa.techlib.ai.general;

/**
 * Standard radial functions.
 * 
 * @author tcgerlach
 */
public final class RadialFunctions {
	
	/**
	 * Compute the gausian of the distance of two vectors.
	 * 
	 * @param r vector distance 
	 * @return gaussian of distance
	 */
	public static float gaussian(final float r) {
		return (float)Math.pow(Math.E, (-1*r*r));  
	}

	/**
	 * Compute the Ricker wavelet function of the distance of two vectors.
	 * 
	 * @param r distance between two vectors
	 * @return Ricker wavelet function 
	 */
	public static float rickerWavelet(final float r) {
		return (float)((1 - r * r) * Math.pow(Math.E, (-1*r*r)/2));
	}
	
	private RadialFunctions() {
		// private constructor - static methods only
	}
}
