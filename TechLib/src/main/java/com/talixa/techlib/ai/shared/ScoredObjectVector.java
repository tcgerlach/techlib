package com.talixa.techlib.ai.shared;

import java.util.Arrays;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Scored vector for objects.
 * 
 * @author tcgerlach
 */
public class ScoredObjectVector<T> extends ScoredVector {
	
	private T[] vector;
	
    @SuppressFBWarnings(value="EI_EXPOSE_REP2", justification="Do not want to copy, could be very large data set")
	public ScoredObjectVector(T[] vector, float score) {
		this.vector = vector;
		this.score = score;
	}
	
	/**
	 * Retrieve the data elements of this vector.
	 * 
	 * @return vector data elements
	 */
    @SuppressFBWarnings(value="EI_EXPOSE_REP", justification="Intentionaly exposing so other tools may directly access and modify")
	public T[] getData() {
		return vector;
	}

	@Override
	public String toString() {
		return "ScoredObjectVector [score=" + score + ", " + "vector=" + Arrays.toString(vector) + "]";
	}
}
