package com.talixa.techlib.ai.genetic.programming;

import java.util.Random;

/**
 * Implementation of a tree for mathematical calculations
 * 
 * @author tcgerlach
 */
public class Tree {
	
	private static Random r = new Random(System.currentTimeMillis());

	public static final int OPCODE_ADD = 0;
	public static final int OPCODE_SUBTRACT = 1;
	public static final int OPCODE_MULTIPLY = 2;
	public static final int OPCODE_DIVIDE = 3;
	public static final int OPCODE_NEGATE = 4;
	public static final int OPCODE_SQRT = 5;
	public static final int OPCODE_POW = 6;
	public static final int OPCODE_MEMORY = 7;
	
	/**
	 * Evaluate a tree and return the result.
	 * 
	 * @param node tree node to evaluate
	 * @param memory contents of variables and constants for tree
	 * @return value of result - could be Inf or NaN depending on operations
	 */
	public static float evaluate(final TreeNode node, final float[] memory) {
		if (node.getOpcode() == OPCODE_ADD) {
			return evaluate(node.getLeft(), memory) + evaluate(node.getRight(), memory);
		} else if (node.getOpcode() == OPCODE_SUBTRACT) {
			return evaluate(node.getLeft(), memory) - evaluate(node.getRight(), memory);
		} else if (node.getOpcode() == OPCODE_MULTIPLY) {
			return evaluate(node.getLeft(), memory) * evaluate(node.getRight(), memory);
		} else if (node.getOpcode() == OPCODE_DIVIDE) {
			return evaluate(node.getLeft(), memory) / evaluate(node.getRight(), memory);
		} else if (node.getOpcode() == OPCODE_NEGATE) {
			return -evaluate(node.getLeft(), memory);
		} else if (node.getOpcode() == OPCODE_SQRT) {
			return (float)Math.sqrt(evaluate(node.getLeft(), memory));
		} else if (node.getOpcode() == OPCODE_POW) {
			return (float)Math.pow(evaluate(node.getLeft(), memory), evaluate(node.getRight(), memory));
		} else {
			return memory[node.getOpcode() - OPCODE_MEMORY];
		}
	}
	
	/**
	 * Convert a tree to a string representation.
	 * The result of this can be run through Python.
	 * To execute in python, use 'import math' so as
	 * to include the square root function.
	 * 
	 * @param node tree node to evaluate
	 * @param memory contents of variables and constants for tree
	 * @return string representation of function
	 */
	public static String getEquation(final TreeNode node, final float[] memory) {
		if (node.getOpcode() == OPCODE_ADD) {
			return "(" + getEquation(node.getLeft(), memory) + " + " + getEquation(node.getRight(), memory) + ")";
		} else if (node.getOpcode() == OPCODE_SUBTRACT) {
			return "(" + getEquation(node.getLeft(), memory) + " - " + getEquation(node.getRight(), memory) + ")";
		} else if (node.getOpcode() == OPCODE_MULTIPLY) {
			return "(" + getEquation(node.getLeft(), memory) + " * " + getEquation(node.getRight(), memory) + ")";
		} else if (node.getOpcode() == OPCODE_DIVIDE) {
			return "(" + getEquation(node.getLeft(), memory) + " / " + getEquation(node.getRight(), memory) + ")";
		} else if (node.getOpcode() == OPCODE_NEGATE) {
			return "(-" + getEquation(node.getLeft(), memory) + ")";
		} else if (node.getOpcode() == OPCODE_SQRT) {
			return "sqrt(" + getEquation(node.getLeft(), memory) + ")";
		} else if (node.getOpcode() == OPCODE_POW) {
			return "(" + getEquation(node.getLeft(), memory) + " ** " + getEquation(node.getRight(), memory) + ")";
		} else {
			return Float.toString(memory[node.getOpcode() - OPCODE_MEMORY]);
		}
	}
	
	/**
	 * Count the nodes in a tree.
	 * 
	 * @param root start point for count
	 * @return number of nodes
	 */
	public static int countNodes(final TreeNode root) {
		int totalCount = 1;
		if (root.getLeft() != null) {
			totalCount += countNodes(root.getLeft());
		}
		if (root.getRight() != null) {
			totalCount += countNodes(root.getRight());
		}
		return totalCount;
	}
	
	private static TreeNode reservoir = null;
	
	/**
	 * Select a random node in the tree using reservoir selection.
	 * 
	 * NOTE: This function is NOT thread safe
	 * 
	 * @param root starting point for selection
	 * @return a randomly selected node
	 */
	public static TreeNode getRandomNode(final TreeNode root) {
		getRandomNode(root, 0);
		return reservoir;
	}
	
	private static void getRandomNode(final TreeNode root, int index) {
		// increment position
		index++;
		
		// determine if we replace reservoir
		int j = r.nextInt(index);
		if (j == 0) {
			reservoir = root;
		}
		
		// traverse children
		if (root.getLeft() != null) {
			getRandomNode(root.getLeft(), index);
		}
		if (root.getRight() != null) {
			getRandomNode(root.getRight(), index);
		}
	}
	
	public static TreeNode clone(final TreeNode root) {
		TreeNode newRoot = new TreeNode();
		newRoot.setOpcode(root.getOpcode());
		
		if (root.getLeft() != null) {
			newRoot.setLeft(clone(root.getLeft()));
		}
		
		if (root.getRight() != null) {
			newRoot.setRight(clone(root.getRight()));
		}
		
		return newRoot;
	}
	
	/**
	 * Create a mutated tree from parents t1 and t2.
	 * A randomly selected branch from t2 will be spliced
	 * onto a randomly selected node from t1.
	 * 
	 * @param t1 root tree
	 * @param t2 mutation tree
	 * @return a new mutated tree
	 */
	public static TreeNode mutate(final TreeNode t1, final TreeNode t2) {
		// clone t1
		TreeNode baseTree = clone(t1);
		
		// find node to mutate
		TreeNode randomDest = getRandomNode(baseTree);
		
		// find mutation
		TreeNode randomSrc = getRandomNode(t2);
		
		// clone mutation
		TreeNode treeBranch = clone(randomSrc);
		
		// pick left or right for insertion and set mutation
		if (r.nextFloat() < .5f) {
			randomDest.setRight(treeBranch);
		} else {
			randomDest.setLeft(treeBranch);
		}
		
		// return new tree
		return baseTree;
	}
}
