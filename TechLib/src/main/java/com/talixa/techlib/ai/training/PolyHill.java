package com.talixa.techlib.ai.training;

import java.util.Arrays;

/**
 * Find polynomial using hill climbing algorithm.
 * This works well for high degree polynomials
 * or polynomials with large coefficients.
 * 
 * @author tcgerlach
 */
public class PolyHill extends PolyFinder {
	
	private static final int INITIAL_VELOCITY = 1;
	private static final int ACCELERATION = 1;

	private float[] candidate = new float[5];
	private float[] stepSize;
	private float bestScore = Float.MAX_VALUE;
	
	/**
	 * Create a hill climbing polynomial finder.
	 * @param trainingInput input data for training
	 * @param idealOutput ideal output data for given input
	 * @param degree polynomial degree
	 */
	public PolyHill(float[] trainingInput, float[] idealOutput, int degree) {
		super(trainingInput, idealOutput, degree);
		stepSize = new float[degree+1];
		for(int i = 0; i < stepSize.length; ++i) {
			stepSize[i] = INITIAL_VELOCITY;
		}
		
		candidate[0] = -ACCELERATION;
		candidate[1] = -1/ACCELERATION;
		candidate[2] = 0;
		candidate[3] = 1/ACCELERATION;
		candidate[4] = ACCELERATION;
	}
	
	/**
     * Determine coefficients for poly.
     * @param numIterations maximum number of iterations of algorithm
     * @return best fit for poly coefficients
     */
    public float[] getCoefficients(int maxIterations) {
    	// reset best score
    	bestScore = Float.MAX_VALUE;
    	
    	// limit to max iterations
		for(int i = 0; i < maxIterations; ++i) {
			// if best score found, exit
			boolean done = iterate();
			if (done) {
				break;
			}
		}
		return Arrays.copyOf(bestCoefficients, bestCoefficients.length);
	}
        
    /** 
     * Iterate the hill climber.
     * @return if no changes were made, return true
     */
	private boolean iterate() {
		int length = bestCoefficients.length;
		boolean moveMade = false;
		
		// for each coefficient
		for(int i = 0; i < length; ++i) {
			int best = -1;
			
			
			// try every possible step
			for(int j = 0; j < candidate.length; ++j) {
				bestCoefficients[i] += (stepSize[i] * candidate[j]);
				float score = calculateScore(bestCoefficients);
				bestCoefficients[i] -= (stepSize[i] * candidate[j]);
				
				// did it improve? keep track of improvement
				if (score < bestScore) {
					bestScore = score;
					best = j;
				}
			}
			
			if (best != -1) {
				bestCoefficients[i] += (stepSize[i] * candidate[best]);
				stepSize[i] *= candidate[best];
				moveMade = true;
			}
		}
		
		return !moveMade;
	}
}
