package com.talixa.techlib.ai.cluster;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.talixa.techlib.ai.general.Distance;
import com.talixa.techlib.ai.prng.RandomLCG;

/**
 * Use the K-Means algorithm for sorting data into clusters.
 * 
 * @author tcgerlach
 */
public class KMeans {
	
	// if we are preventing empty clusters, it is possible
	// that we keep moving things around an infinite number of times
	// this will limit the number of attempts to fix empty cluster
	private static final int MAX_EMPTY_SHUFFLES = 10;
	private int emptyShuffles = 0;
	
	private List<Cluster> clusters = new ArrayList<Cluster>();
	private int valuesPerObservation;			// number of floats in every observation
	
	private boolean allowEmpty = false;			// disallow empty clusters
	
	// needed to randomly select members to replace empty clusters
	private static Random r = new Random(System.currentTimeMillis());
	
	/**
	 * Initialize the KMeans algorithm.
	 * @param observations list of all data points for analysis
	 * @param k number of clusters
	 */
	public KMeans(float[][] observations, int k) {
		init(observations, k);								// init kmeans
		valuesPerObservation = observations[0].length;		// save number of items per observation
		update(valuesPerObservation);						// set initial centroid
	}
	
	/**
	 * Run algorithm and retrieve clusters.
	 * @return list of clusters
	 */
	public List<Cluster> cluster() {
		// reset the number of shuffles
		emptyShuffles = 0;
		
		// if update has finished, we are done
		while (!assign()) {
			update(valuesPerObservation);
		}
		
		// update one last time so centroid is right
		update(valuesPerObservation);
		
		// return clusters
		return clusters;
	}
	
	/**
	 * Initialize the cluster with the observations.
	 * Observations are assigned to random clusters and centroid is set.
	 * @param observations list of all observations
	 * @param k number of cluster
	 */
	private void init(float[][] observations, int k) {
		// create clusters
		for (int i = 0; i < k; ++i) {
			clusters.add(new Cluster());
		}
		
		// assign observations to random clusters
		for(int i = 0; i < observations.length; ++i) {
			Observation o = new Observation(observations[i]);
			int index = RandomLCG.getNextInt() % k;
			clusters.get(index).getObservations().add(o);
		}
	}
	
	/**
	 * Move all observations to their closest cluster.
	 * @return true if no observations moved to a new cluster
	 */
	private boolean assign() {
		boolean done = true;
		
		for (Cluster c : clusters) {
			List<Observation> observationsToMove = new ArrayList<Observation>();
			
			for(Observation o : c.getObservations()) {
				
				Cluster closest = findNearestCluster(o);
				if (closest != c) {
					// found a better cluster
					observationsToMove.add(o);
					closest.getObservations().add(o);
					done = false;
				}
			}
					
			// now remove observations - outside for loop
			c.getObservations().removeAll(observationsToMove);
		}
		
		// fix any empty clusters
		if (!allowEmpty && emptyShuffles < MAX_EMPTY_SHUFFLES) {
			Cluster largest = clusters.get(0);
			List<Cluster> empty = new ArrayList<Cluster>();
			for(Cluster c : clusters) {
				if (c.getObservations().size() > largest.getObservations().size()) {
					largest = c;
				}
				if (c.getObservations().isEmpty()) {
					empty.add(c);
					done = false;
					++emptyShuffles;
				}
			}
			for(Cluster e : empty) {
				int randomIndex = r.nextInt(largest.getObservations().size()-1);
				e.getObservations().add(largest.getObservations().get(randomIndex));
				largest.getObservations().remove(randomIndex);
			}
		}
	
		return done;
	}
	
	/**
	 * Find the nearest cluster for an observation.
	 * @param o observation to find nearest cluster for
	 * @return nearest cluster
	 */
	private Cluster findNearestCluster(Observation o) {
		float shortestDistance = Float.MAX_VALUE;
		Cluster target = null;
		
		// check each cluster
		for (Cluster c : clusters) {
			// find distance
			float distance = Distance.euclidean(o.getDataPoints(), c.getCentroid().getDataPoints());
			
			// check if shortest and update variables
			if (distance < shortestDistance) {
				shortestDistance = distance;
				target = c;
			}
		}
		
		// return nearest cluster
		return target;
	}

	/**
	 * Update the observations assigned to each cluster based on distance.
	 * @param valuesPerObservation number of items that each observation contains
	 */
	private void update(int valuesPerObservation) {
		for (Cluster c : clusters) {
			int numObservations = c.getObservations().size();
			
			// create new centroid
			float[] centroid = new float[valuesPerObservation];
			
			// average each observation value for centroid
			for(int dataPointIndex = 0; dataPointIndex < valuesPerObservation; ++dataPointIndex) {
				float dataPointSum = 0;
				for(int observationIndex = 0; observationIndex < numObservations; ++observationIndex) {
					dataPointSum += c.getObservations().get(observationIndex).getDataPoints()[dataPointIndex];
				}
				float average = dataPointSum / numObservations;
				centroid[dataPointIndex] = average;
			}
			
			// set centroid
			c.setCentroid(new Observation(centroid));
		}
	}

	public boolean isAllowEmpty() {
		return allowEmpty;
	}

	public void setAllowEmpty(boolean allowEmpty) {
		this.allowEmpty = allowEmpty;
	}
}
