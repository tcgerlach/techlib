package com.talixa.techlib.ai.flock;

import com.talixa.techlib.shared.Vector2D;

/**
 * This behavior allows a boid to keep it's distance from enemy boids.
 * @author tcgerlach
 */
public class AvoidanceBehavior extends Behavior {
	
	private float avoidanceDistance;
	private float avoidanceSpeed;
	
	/**
	 * Create a instance of this behavior to assign to a boid.
	 * @param avoidanceDistance distance to stay away from enemies
	 * @param avoidanceSpeed speed to move away from enemy
	 */
	public AvoidanceBehavior(float avoidanceDistance, float avoidanceSpeed) {
		this.avoidanceDistance = avoidanceDistance;
		this.avoidanceSpeed = avoidanceSpeed;
	}
	
	@Override
	public void updateBoid(Boid boid) {
		findNearestBoidInGroup(boid.getEnemies(), boid);
		
		if (distanceToNearest < avoidanceDistance) {
			Vector2D desiredMoveAdj = Vector2D.subtractVectors(boid.getLocation(), nearestBoid.getLocation());
			desiredMoveAdj.normalize();
			desiredMoveAdj.scale(avoidanceSpeed);
			boid.getDesiredMove().addVector(desiredMoveAdj);
		}
	}
}
