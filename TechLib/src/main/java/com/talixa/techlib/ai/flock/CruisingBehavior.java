package com.talixa.techlib.ai.flock;

import java.util.Random;

import com.talixa.techlib.shared.Vector2D;

/**
 * Behavior to freely move the boid
 * @author tcgerlach
 */
public class CruisingBehavior extends Behavior {

	private float xMoveLeftChance;
	private float xMoveRightChance;
	private float yMoveUpChance;
	private float yMoveDownChance;
	private float minMove;
	private float maxRateChange;
	private float minRateChange;
	
	private static Random rand = new Random(System.currentTimeMillis());
	
	/**
	 * Create a behavior that allows the boid to freely cruise around
	 * @param xMoveChance chance for x movement
	 * @param yMoveChance chance for y movement
	 * @param minMove minimum movement
	 * @param minRateChange minimum rate of change
	 * @param maxRateChange maimum rate of change
	 */
	public CruisingBehavior(float xMoveChance, float yMoveChance, float minMove, float minRateChange, float maxRateChange) {
		xMoveLeftChance = xMoveChance/2;
		xMoveRightChance = xMoveChance;
		yMoveUpChance = (yMoveChance - xMoveChance) / 2 + xMoveChance;
		yMoveDownChance = yMoveChance;
		this.minMove = minMove;
		this.maxRateChange = maxRateChange;
		this.minRateChange = minRateChange;
	}

	@Override
	public void updateBoid(Boid b) {				
		float currentSpeed = b.getVelocity().getMagnitude();
		float percentDesiredSpeed = Math.abs((currentSpeed - b.getDesiredSpeed()) / b.getMaxSpeed());
		float sign = (currentSpeed - b.getDesiredSpeed()) > 0 ? -1 : 1;		
		
		// clamp
		if (percentDesiredSpeed < minRateChange) {
			percentDesiredSpeed = minRateChange;
		} else if (percentDesiredSpeed > maxRateChange) {
			percentDesiredSpeed = maxRateChange;
		}
		
		// determine direction
		Vector2D desiredMoveAdj = new Vector2D(0,0);
		int random = Math.abs(rand.nextInt(100));
		if (random < xMoveLeftChance) {
			desiredMoveAdj.setX(minMove*sign*-1);
		} else if (random < xMoveRightChance) {
			desiredMoveAdj.setX(minMove*sign);			
		} else if (random < yMoveUpChance) {
			desiredMoveAdj.setY(minMove*sign*-1);
		} else if (random < yMoveDownChance) {
			desiredMoveAdj.setY(minMove*sign);
		}	
		
		// normalize then scale
		desiredMoveAdj.normalize();
		desiredMoveAdj.scale(minRateChange*sign);
		
		b.getDesiredMove().addVector(desiredMoveAdj);		
	}
}
