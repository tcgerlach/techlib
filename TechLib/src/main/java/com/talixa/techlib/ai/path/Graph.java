package com.talixa.techlib.ai.path;

/**
 * A graph is a collection of nodes for a path finding algorithm.
 * This class takes a list of strings and converts it into a graph.
 * This graph could represent a board for a game where the graph is
 * fed into the path finding algorithm to determine the movement of
 * enemy characters in the game.
 * 
 * @author tcgerlach
 */
public class Graph {

	private int width;
	private int height;
	private Node[][] graphData;
	
	private static final int NW = 0;
	private static final int N  = 1;
	private static final int NE = 2;
	private static final int W  = 3;
	private static final int E  = 4;
	private static final int SW = 5;
	private static final int S  = 6;
	private static final int SE = 7;	
	
	/**
	 * Create a graph from an array of strings.
	 * Each string represents a row of data. 
	 * A 0 in the string represents an open node.
	 * A 1 in the string represents a blocked node.
	 * Data should be rectangular - no short rows.
	 * @param rowData graph data represented as strings
	 */
	public Graph(String[] rowData) {
		this.width = rowData[0].length();
		this.height = rowData.length;
		
		// create structure
		graphData = new Node[height][];
		for(int i = 0; i < height; ++i) {
			graphData[i] = new Node[width];
		}
		
		// add nodes
		for(int i = 0; i < height; ++i) {
			setGraphRow(i, rowData[i]);
		}
		
		// link nodes
		for(int row = 0; row < height; ++row) {
			for(int col = 0; col < width; ++col) {
				if (row != 0 && col != 0) {
					graphData[row][col].setNeighbor(NW, graphData[row-1][col-1]);
				}
				if (row != 0) {
					graphData[row][col].setNeighbor(N, graphData[row-1][col]);
				} 
				if (row != 0 && col != width-1) {
					graphData[row][col].setNeighbor(NE, graphData[row-1][col+1]);
				}
				if (col != 0) {
					graphData[row][col].setNeighbor(W, graphData[row][col-1]);
				}
				if (col != width-1) {
					graphData[row][col].setNeighbor(E, graphData[row][col+1]);
				}
				if (row != height-1 && col != 0) {
					graphData[row][col].setNeighbor(SW, graphData[row+1][col-1]);
				}
				if (row != height-1) {
					graphData[row][col].setNeighbor(S, graphData[row+1][col]);
				} 
				if (row != height-1 && col != width-1) {
					graphData[row][col].setNeighbor(SE, graphData[row+1][col+1]);
				}
			}
		}
	}
	
	private void setGraphRow(int row, String rowData) {
		for(int i = 0; i < width; ++i) {
			boolean blocked = rowData.charAt(i) == '1';	// a char 1 is a blocked node
			Node n = new Node(i,row,blocked);
			graphData[row][i] = n;
		}
	}
	
	/**
	 * Return the height of the graph
	 * @return height
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Return the width of the graph
	 * @return width
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Retrieve the node at the given position.
	 * This node can then be fed into the PathFinder algorithm. 
	 * @param x column 
	 * @param y row
	 * @return node at given position
	 */
	public Node getNode(int x, int y) {
		if (x < 0 || y < 0 || y >= height || x >= width) {
			return null;
		} else {
			return graphData[y][x];
		}
	}	
	
	/**
	 * Reset the node data after path finding finished.
	 */
	public void reset() {
		for(int row = 0; row < height; ++row) {
			for (int col = 0; col < width; ++col) {
				graphData[row][col].setVisited(false);
				graphData[row][col].setParent(null);
				graphData[row][col].setCost(Integer.MAX_VALUE);
			}
		}
	}
}
