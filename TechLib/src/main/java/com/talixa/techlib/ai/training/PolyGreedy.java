package com.talixa.techlib.ai.training;

import java.util.Arrays;

import com.talixa.techlib.ai.prng.RandomLCG;

/**
 * Find a polynomial using Greedy Random Training.
 * This works fine for a low degree polynomial with 
 * relatively small coefficients.
 * 
 * @author tcgerlach
 */
public class PolyGreedy extends PolyFinder {
	
	private float minX;
	private float maxX;
	
	/**
	 * Create a polynomial finder that will use greedy random training
	 * @param trainingInput input training data
	 * @param idealOutput output data for given training data
	 * @param degree polynomial degree
	 * @param minX minimum value to try for each coefficient
	 * @param maxX maximum value to try for each coefficient
	 */
	public PolyGreedy(float[] trainingInput, float[] idealOutput, int degree, float minX, float maxX) {
		super(trainingInput, idealOutput, degree);
		this.minX = minX;
		this.maxX = maxX;
	}
	
	/**
     * Determine coefficients for poly.
     * @param numIterations maximum number of iterations of algorithm
     * @return best fit for poly coefficients
     */
    public float[] getCoefficients(int maxIterations) {
		for(int i = 0; i < maxIterations; ++i) {
			iterate();
		}
		return Arrays.copyOf(bestCoefficients, bestCoefficients.length);
	}
	
    /**
     * Iterate the GRT
     */
	private void iterate() {
		// get score with current values
		float oldScore = calculateScore(bestCoefficients);
		
		// randomly determine new values
		float[] newCoefficients = new float[degree+1];
		for(int i = 0; i < (degree+1); ++i) {
			newCoefficients[i] = RandomLCG.getNextInt() % (maxX - minX) + minX;
		}
		
		// test score with new values
		float newScore = calculateScore(newCoefficients);
		
		// determine if better match
		if (newScore < oldScore) {
			bestCoefficients = newCoefficients;
		}
	}
}
