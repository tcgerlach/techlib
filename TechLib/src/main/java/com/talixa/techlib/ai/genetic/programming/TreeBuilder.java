package com.talixa.techlib.ai.genetic.programming;

import java.util.Random;

/**
 * Functions to build random trees
 * 
 * @author tcgerlach
 */
public class TreeBuilder {

	private static Random r = new Random(System.currentTimeMillis());
	
	/**
	 * Initialize a tree where all terminal nodes contain numbers
	 * and non-terminal nodes contain operations
	 * 
	 * @param depth number of levels deep for tree
	 * @param memorySize number of values in memory
	 * @return root node of tree
	 */
	public static TreeNode fullTreeInitalization(final int depth, final int memorySize) {
		TreeNode newNode;
		
		if (depth > 0) {
			// opcodes for all non-terminal
			newNode = new TreeNode(null, null, r.nextInt(Tree.OPCODE_MEMORY));
		} else {
			// memory for all terminal
			newNode = new TreeNode(null, null, Tree.OPCODE_MEMORY + r.nextInt(memorySize));
		}
		
		if (depth != 0) {
			newNode.setLeft(fullTreeInitalization(depth-1,memorySize));
			newNode.setRight(fullTreeInitalization(depth-1,memorySize));
		}
		
		return newNode;
	}
	
	/**
	 * Initialize a tree where any node can have any value.
	 * This may create trees that are not full.
	 * 
	 * @param depth number of levels deep for tree
	 * @param memorySize number of values in memory
	 * @return root node of tree
	 */
	public static TreeNode growTreeInitialization(final int depth, final int memorySize) {
		TreeNode newNode;
		
		if (depth > 0) {
			// anything for non-terminal
			newNode = new TreeNode(null, null, r.nextInt(Tree.OPCODE_MEMORY + memorySize));
		} else {
			// memory for all terminal
			newNode = new TreeNode(null, null, Tree.OPCODE_MEMORY + r.nextInt(memorySize));
		}
		
		if (depth != 0) {
			newNode.setLeft(growTreeInitialization(depth-1,memorySize));
			newNode.setRight(growTreeInitialization(depth-1,memorySize));
		}
		
		return newNode;
	}
}
