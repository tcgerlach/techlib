package com.talixa.techlib.ai.genetic.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.talixa.techlib.ai.prng.RandomLCG;
import com.talixa.techlib.ai.shared.ScoredVector;

/**
 * Functions for selecting members of a population.
 * 
 * @author tcgerlach
 */
public class Selection {
	
	/**
	 * Select a member based from the top percentile of the pool
	 * 
	 * @param population list of all members of the population
	 * @param breedingPercentage top percentage that will be considered for selection
	 * @return a member of the population in within the breeding percentage 
	 */
	public static ScoredVector truncation(List<? extends ScoredVector> population, final float breedingPercentage) {
		Collections.sort(population);
		int eligibleBreeders = (int)(population.size() * breedingPercentage);
		int randomIndex = RandomLCG.getNextInt() % eligibleBreeders;
		return population.get(randomIndex);
	}
	
	/**
	 * Select a member of the population based on a tournament style selection.
	 * 
	 * @param population list of all members of the population
	 * @param rounds number of tournament rounds to run
	 * @return population member that wins the tournament
	 */
	public static ScoredVector tournament(List<ScoredVector> population, final int rounds) {
		int randomIndex = RandomLCG.getNextInt() % population.size();
		ScoredVector champ = population.get(randomIndex);
		for(int i = 0; i < rounds; ++i) {
			randomIndex = RandomLCG.getNextInt() % population.size();
			ScoredVector challenger = population.get(randomIndex);
			// assuming lower score is better
			if (champ.compareTo(challenger) > 0) {
				champ = challenger;
			}
		}
		return champ;
	}
	
	/**
	 * Fitness Proportionate selection is like a roulette wheel where each slice is
	 * as wide as the score for that element. All elements are eligible to be selected,
	 * but a higher score will weigh more in the search.
	 * Note that better scores are represented by higher numbers.
	 * @param population list of all eligible members
	 * @return selected vector
	 */
	public static ScoredVector fitnessProportionate(final List<ScoredVector> population) {
		// start by calculating score of all vectors
		float totalScore = ScoredVector.getPopulationScore(population);
		
		// determine random percentage
		float p = RandomLCG.getNextFloat();
		
		// find element at that percentage along the total score
		float coveredSoFar = 0;
		for(ScoredVector v : population) {
			coveredSoFar += (v.getScore()/totalScore);
			// we are now coveredSoFar percent through the list
			// is this where we randomly chose to stop?
			if (p < coveredSoFar) {
				return v;
			}
		}
		
		// should not get here
		return null;
	}
	
	/**
	 * Select a group of samples equally spaced.
	 * Input data should meet the following criteria:
	 * 1) Data should be sorted.
	 * 2) Better scores are represented by larger numbers.
	 * @param population input list of population members
	 * @param samples number of samples to return
	 * @return list of samples
	 */
	public static List<ScoredVector> stochasticUniversalSampling(List<ScoredVector> population, int samples) {
		// start by calculating score of all vectors
		float totalScore = ScoredVector.getPopulationScore(population);
		
		// calculate distance between pointers
		float p = totalScore/samples;
		
		// random starting point before first pointer
		float start = RandomLCG.getNextFloat(p);
		
		// calculate all points based on start
		float[] points = new float[samples];
		for (int i = 0; i < samples; ++i) {
			points[i] = start + (i * p);	// start plus distance between items
		}
		
		// now collect points defined above
		List<ScoredVector> results = new ArrayList<ScoredVector>();
		int currentIndex = 0;								// start with point 0
		float runningScore = 0;								// start with no score
		for(ScoredVector s : population) {					// iterate through population
			runningScore += s.getScore();					// increment running score
			while (runningScore > points[currentIndex]) {	// did we pass the desired point?
				results.add(s);								// if so, add and go to next point
				++currentIndex;
				
				// don't go past end!
				if (currentIndex >= samples) {
					break;
				}
			}
			
			// don't go past end!
			if (currentIndex >= samples) {
				break;
			}
		}
		
		return results;
	}
	
	private Selection() {
		// private constructor
	}
}
