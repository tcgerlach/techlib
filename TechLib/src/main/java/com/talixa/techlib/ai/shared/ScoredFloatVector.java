package com.talixa.techlib.ai.shared;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Scored vector for floats.
 * 
 * @author tcgerlach
 */
public class ScoredFloatVector extends ScoredVector {
	
	private float[] vector;
	
    @SuppressFBWarnings(value="EI_EXPOSE_REP2", justification="Do not want to copy, could be very large data set")
	public ScoredFloatVector(float[] vector, float score) {
		this.vector = vector;
		this.score = score;
	}
		
	/**
	 * Retrieve the data elements of this vector.
	 * 
	 * @return vector data elements
	 */
    @SuppressFBWarnings(value="EI_EXPOSE_REP", justification="Intentionaly exposing so other tools may directly access and modify")
	public float[] getData() {
		return vector;
	}
}
