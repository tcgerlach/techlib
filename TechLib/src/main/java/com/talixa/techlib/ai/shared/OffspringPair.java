package com.talixa.techlib.ai.shared;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * OffspringPair is a pair of children for genetic AI algorithms.
 * Each child is an array of object of type <T>. This object is
 * returned from mutation functions for splice and shuffle.
 * 
 * @author tcgerlach
 *
 * @param <T> Object type of child arrays
 */
public class OffspringPair<T> {
	private T[] child1;
	private T[] child2;
	
	/**
	 * Create a new offspring pair.
	 * 
	 * @param child1 first child created
	 * @param child2 second child created
	 */
	@SuppressFBWarnings(value="EI_EXPOSE_REP2", justification="Do not want to copy, could be very large data set")
	public OffspringPair(T[] child1, T[] child2) {
		super();
		this.child1 = child1;
		this.child2 = child2;
	}
	
	/**
	 * Retrieve the first child array.
	 * 
	 * @return array of objects representing child 1
	 */
	@SuppressFBWarnings(value="EI_EXPOSE_REP", justification="Intentionaly exposing so other tools may directly access and modify")
	public T[] getChild1() {
		return child1;
	}
	
	/**
	 * Retrieve the second child array.
	 * 
	 * @return array of objects representing child 2
	 */
	@SuppressFBWarnings(value="EI_EXPOSE_REP", justification="Intentionaly exposing so other tools may directly access and modify")
	public T[] getChild2() {
		return child2;
	}
}
