package com.talixa.techlib.ai.path;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A node for a path finding algorithm. 
 * 
 * @author tcgerlach
 */
public class Node implements Comparable<Node> {
	
	private int x;
	private int y;
	private boolean blocked;
	private boolean visited;
	private int cost = 9999;
	private boolean path = false;	// set by search algorithm when path found
	private Node parent;			// set by search algorithm when path found
	private Node[] neighbors;		// numbered top left to bottom right
	
	/**
	 * Create a node for path finding
	 * @param x column of the node graph
	 * @param y row of the node graph
	 * @param blocked true if this node is blocked
	 */
	public Node(int x, int y, boolean blocked) {
		this.x = x;
		this.y = y;
		this.blocked = blocked;
		this.neighbors = new Node[8];
	}
	
	/**
	 * Check if this node is part of the path
	 * @return true if included in path
	 */
	public boolean isPath() {
		return path;
	}
	
	/**
	 * Add this node to the path - this is called by {@link com.talixa.techlib.ai.path.PathFinder}.
	 */
	public void addToPath() {
		this.path = true;
	}
	
	/**
	 * Set a neighbor node.
	 * @param position cardinal position of node in relation to this one
	 * @param node neighbor node
	 */
	public void setNeighbor(int position, Node node) {
		this.neighbors[position] = node;
	}

	/**
	 * Retrieve a neighbor at a given position.
	 * @param position location of the desired neighbor
	 * @return neighbor at given position
	 */
	public Node getNeighbor(int position) {
		return this.neighbors[position];
	}

	/**
	 * Return the x value of this node in the graph.
	 * @return column indicator
	 */
	public int getX() {
		return x;
	}

	/**
	 * Set column of node in graph.
	 * @param x column indicator
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Return the y value of this node in the graph.
	 * @return row indicator
	 */
	public int getY() {
		return y;
	}

	/**
	 * Set row of node in graph.
	 * @param y row indicator
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Query if this node is blocked.
	 * @return true if blocked
	 */
	public boolean isBlocked() {
		return blocked;
	}

	/**
	 * Set blocked indicator for this node.
	 * @param blocked true if this node is blocked
	 */
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	/**
	 * Query if this node has been visited.
	 * This is used by {@link com.talixa.techlib.ai.path.PathFinder}
	 * @return
	 */
	public boolean isVisited() {
		return visited;
	}

	/** 
	 * Set this node as visited.
	 * This is used by {@link com.talixa.techlib.ai.path.PathFinder}
	 * @param visited true if this node visited
	 */
	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	/**
	 * Retrieve the parent of this node.
	 * This is used by {@link com.talixa.techlib.ai.path.PathFinder}
	 * @return parent of this node
	 */
	public Node getParent() {
		return parent;
	}

	/**
	 * Set the parent of this node.
	 * Used by {@link com.talixa.techlib.ai.path.PathFinder}
	 * @param parent node that parents this node
	 */
	public void setParent(Node parent) {
		this.parent = parent;
	}
	
	/**
	 * Retrieve the cost to traverse this node
	 * @return cost of traveling this node
	 */
	public int getCost() {
		return cost;
	}
	
	/**
	 * Set the cost to traverse this node
	 * @param cost indicator of difficulty of crossing this node
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	/** 
	 * Return this node as a string.
	 * This can be used for printing a graph as a series of 1's and 0's.
	 */
	public String toString() {
		return blocked ? "1" : "0";
	}

	@Override
	@SuppressFBWarnings(value="EQ_COMPARETO_USE_OBJECT_EQUALS", justification="Not concerned about this.")
	public int compareTo(Node other) {
		return this.cost - other.cost;
	}
}
