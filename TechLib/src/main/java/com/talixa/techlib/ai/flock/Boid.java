package com.talixa.techlib.ai.flock;

import java.util.List;

import com.talixa.techlib.ai.flock.Behavior;
import com.talixa.techlib.shared.Vector2D;

/**
 * Boid data class. 
 * The word boid comes from an artificial life simulation of 
 * bird-oid objects written to show flocking behaviors of birds.
 * 
 * @author tcgerlach
 */
public class Boid {	
	private static final int SPEED = 5;
	
	private List<Behavior> behaviors;		// behaviors this boid must follow
	private List<Boid> group;				// boids in this group (including self)
	private List<Boid> enemies;				// enemies of this boid
	
	// location, vector, & color of this boid
	private Vector2D location;
	private Vector2D velocity = new Vector2D(0,0);
	private int id;
	
	// variables to determine movement
	private Vector2D desiredMove = new Vector2D(0,0);
	private float desiredSpeed;
	private float maxSpeed;
	
	/**
	 * Create a new boid.
	 * @param x initial x location
	 * @param y initial y location
	 * @param vx x portion of boid movement direction
	 * @param vy y portion of boid movement direction
	 * @param desiredSpeed speed boid will move
	 * @param maxSpeed maximum speed boid will move
	 * @param uniqueBoidId unique id for this boid
	 */
	public Boid(float x, float y, float vx, float vy, float desiredSpeed, float maxSpeed, int uniqueBoidId) {
		location = new Vector2D(x, y);
		velocity = new Vector2D(vx,vy);
		this.desiredSpeed = desiredSpeed;
		this.maxSpeed = maxSpeed;
		this.id = uniqueBoidId;
	}
	
	/**
	 * Update the desired speed of this boid.
	 * @param desiredSpeed new speed
	 */
	public void setDesiredSpeed(float desiredSpeed) {
		this.desiredSpeed = desiredSpeed;
	}
	
	/**
	 * Retrieve currently set boid speed
	 * @return current boid desired speed
	 */
	public float getDesiredSpeed() {
		return desiredSpeed;
	}
	
	/** 
	 * Set a list of behaviors for a boid
	 * @param behaviors list of the behaviors this boid exhibits
	 */
	public void setBehaviors(List<Behavior> behaviors) {
		this.behaviors = behaviors;
	}
	
	/**
	 * Return the current location of this boid
	 * @return location of boid
	 */
	public Vector2D getLocation() {
		return location;
	}
	
	/**
	 * Set location of boid
	 * @param location new location
	 */
	public void setLocation(Vector2D location) {
		this.location = location;
	}
	
	/**
	 * Get current velocity of boid.
	 * @return velocity of boid movement
	 */
	public Vector2D getVelocity() {
		return velocity;
	}
	
	/**
	 * Set boid velocity.
	 * @param v new velocity for boid
	 */
	public void setVelocity(Vector2D v) {
		this.velocity = v;	
	}
	
	/**
	 * Get currently set maximum boid speed.
	 * @return maximum boid speed
	 */
	public float getMaxSpeed() {
		return maxSpeed;
	}
	
	/**
	 * Set the group this boid belongs to.
	 * @param group list of all boids in my group
	 */
	public void setGroup(List<Boid> group) {
		this.group = group;
	}
	
	/**
	 * Get the group this boid belongs to.
	 * @return boid roup
	 */
	public List<Boid> getGroup() {
		return group;
	}
	
	/**
	 * Set the enemies of this boid.
	 * @param enemies list of boid enemies
	 */
	public void setEnemies(List<Boid> enemies) {
		this.enemies = enemies;
	}
	
	/**
	 * Retrieve the boid enemy list.
	 * @return list of boid enemies
	 */
	public List<Boid> getEnemies() {
		return enemies;
	}
	
	/**
	 * Get currently set desired movement of boid.
	 * @return current movement vector
	 */
	public Vector2D getDesiredMove() {
		return desiredMove;
	}
	
	/**
	 * Set boid movement vector
	 * @param desiredMove new desired movement
	 */
	public void setDesiredMove(Vector2D desiredMove) {
		this.desiredMove = desiredMove;
	}
	
	/**
	 * Apply behaviors to boid.
	 * @param time amount of time elapsed since last update
	 */
	public void update(float time) {
		// determine velocity, and set new position
		Vector2D curVel = new Vector2D(velocity.getX(), velocity.getY());
		curVel.scale(SPEED/time);
		location.addVector(curVel);
		
		// reset desired move
		desiredMove = new Vector2D(0,0);
		
		// determine new direction based on behaviors
		for(Behavior b : behaviors) {
			b.updateBoid(this);
		}
		
		// update velocity with changes from behavior
		velocity.addVector(desiredMove);
		float speed = velocity.getMagnitude();
		if (speed > maxSpeed) {
			velocity.normalize();
			velocity.scale(maxSpeed);
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Boid other = (Boid) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	@Override 
	public String toString() {
		return location.toString();
	}
}
