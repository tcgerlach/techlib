package com.talixa.techlib.data;

import com.talixa.techlib.math.interfaces.Function;

/**
 * Functions for gradient descent min/max calculations.
 * 
 * @author tcgerlach
 */
public final class GradientDescent {

	/**
	 * The maximum possible precision for the values.
	 */
	public static final Double MAX_PRECISION = Double.MIN_VALUE;

	// we need to have a limit to the iterations or we go into an infinite loop
	private static final int MAX_ATTEMPTS = 1000;

	// number steps
	private static final double[] STEPS = {100, 10, 1, .1, .01, .001, .0001, .00001};

	/**
	 * Function to return the input data set to produce the smallest value for the targetFunction.
	 * Precision is set set to the maximum possible precision.
	 * @param targetFunction function to find minimum values for
	 * @param gradientFunction derivative function of target function
	 * @param startingTheta starting data set
	 * @return input that will achieve minimum result for the input function
	 */
	public static double[] minimize(final Function targetFunction, final Function gradientFunction, final double[] startingTheta) {
		return descent(targetFunction, gradientFunction, startingTheta, MAX_PRECISION, false);
	}
	
	/**
	 * Function to return the input data set to produce the smallest value for the targetFunction.
	 * @param targetFunction function to find minimum values for
	 * @param gradientFunction derivative function of target function
	 * @param startingTheta starting data set
	 * @param tolerance minimum acceptable tolerance for comparing values
	 * @return input that will achieve minimum result for the input function
	 */
	public static double[] minimize(final Function targetFunction, final Function gradientFunction, final double[] startingTheta, final double tolerance) {
		return descent(targetFunction, gradientFunction, startingTheta, tolerance, false);
	}
	
	/**
	 * Function to return the input data set to produce the largest value for the targetFunction.
	 * Precision is set set to the maximum possible precision.
	 * @param targetFunction function to find maximum values for
	 * @param gradientFunction derivative function of target function
	 * @param startingTheta starting data set
	 * @return input that will achieve maximum result for the input function
	 */
	public static double[] maximize(final Function targetFunction, final Function gradientFunction, final double[] startingTheta) {
		return descent(targetFunction, gradientFunction, startingTheta, MAX_PRECISION, true);
	}

	/**
	 * Function to return the input data set to produce the largest value for the targetFunction.
	 * @param targetFunction function to find maximum values for
	 * @param gradientFunction derivative function of target function
	 * @param startingTheta starting data set
	 * @param tolerance minimum acceptable tolerance for comparing values
	 * @return input that will achieve maximum result for the input function
	 */
	public static double[] maximize(final Function targetFunction, final Function gradientFunction, final double[] startingTheta, final double tolerance) {
		return descent(targetFunction, gradientFunction, startingTheta, tolerance, true);
	}

	// ascend variable inverts all functions to find max instead of min
	private static double[] descent(final Function targetFunction, final Function gradientFunction, final double[] startingTheta, final double tolerance, final boolean inverse) {
		double[] theta = startingTheta;
		double value = targetFunction.execute(theta);	// starting funtion value

		for (int i = 0; i < MAX_ATTEMPTS; ++i) {
			double gradient = gradientFunction.execute(theta);				// determine gradient
			double[][] nextThetas = stepDown(theta, gradient, inverse);		// find list of possible next values
			double[] nextTheta = min(targetFunction, nextThetas, inverse);	// find smallest to be next test set
			double nextValue = targetFunction.execute(nextTheta);			// calculate value of test set
			if (Math.abs(value - nextValue) < tolerance) {					// see if this is precise enough
				return theta;
			} else {
				value = nextValue;
				theta = nextTheta;
			}
		}
		return null;
	}

	// select the thetas that produce the minimum possible values for the target function
	private static double[] min(final Function targetFunction, final double[][] thetas, final boolean inverse) {
		// save minimum values
		double minValue = inverse ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY;
		double[] minThetaSet = null;
	
		// iterate through this like it is a number base
		int numberBase = thetas[0].length;

		// this is how many combinations we have
		int totalTries = (int)Math.pow(numberBase, thetas.length);

		// index for each value
		int[] position = new int[thetas.length];

		// try all possible combinations
		for (int i = 0; i < totalTries; ++i) {
			// convert to new base
			String numInNewBase = "0000000000" + Integer.toString(i, numberBase);

			for (int thetaIndex = 0; thetaIndex < thetas.length; ++thetaIndex) {
				// get value from numInNewBase for this index
				int positionValue = Integer.valueOf(numInNewBase.charAt(numInNewBase.length() - thetaIndex - 1) - 48);
				position[thetaIndex] = positionValue;
			}

			// now that we have calculated the positions, get test set		
			double[] thetaSet = new double[thetas.length];
			for (int thetaIndex = 0; thetaIndex < thetas.length; ++thetaIndex) {
				thetaSet[thetaIndex] = thetas[thetaIndex][position[thetaIndex]];
			}

			// determine the function value for test set
			double value = targetFunction.execute(thetaSet);

			// determine if this is lower than other values
			if (value < minValue && !inverse) {
				minValue = value;
				minThetaSet = thetaSet;
			} else if (value > minValue && inverse) {
				minValue = value;
				minThetaSet = thetaSet;
			}
		}

		return minThetaSet;
	}

	// descend to find a lower value
	private static double[][] stepDown(final double[] theta, final double gradient, final boolean inverse) {
		double[][] results = new double[theta.length][];
		for (int thetaIndex = 0; thetaIndex < theta.length; ++thetaIndex) {
			results[thetaIndex] = new double[STEPS.length];
			for (int stepIndex = 0; stepIndex < STEPS.length; ++stepIndex) {
				results[thetaIndex][stepIndex] = theta[thetaIndex] + STEPS[stepIndex] * gradient * (inverse ? 1 : -1);
			}
		}
		return results;
	}

	private GradientDescent() {
		// no constructor
	} 
}
