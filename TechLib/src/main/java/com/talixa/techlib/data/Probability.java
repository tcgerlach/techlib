package com.talixa.techlib.data;

import org.apache.commons.math3.special.Erf;

/**
 * Standard probability functions.
 * These were converted from Python to Java.
 * Original code appears in Data Science from Scratch by Joel Grus
 * 
 * @author tcgerlach
 */
public final class Probability {

	/**
	 * Uniform probability density function.
	 * All input values from 0 to 1 are equally likely to appear.
	 * Thus, the probability of seeing a value in that that interval
	 * equals the integral of the density function of the interval.
	 * @param x input value
	 * @return 1 if the input value is within the range 0 <= x < 1
	 */
	public static double uniformPdf(final double x) {
		if (x >= 0 && x < 1) {
			return 1;
		} else {
			return 0;
		}
	}

	/**
	 * Uniform cumulative density function.
	 * Shows the probability that a random variable is less than or
	 * equal to a certain value.
	 * @param x input value
	 * @return probability that a number is less than or equal to x
	 */
	public static double uniformCdf(final double x) {
		if (x < 0) {
			return 0;
		} else if (x < 1) {
			return x;
		} else {
			return 1;
		}
	}

	/**
	 * Normal probability distribution.
	 * Classic bell curve with standard deviation of 1 and mean of 0
	 * @param x input value
	 * @return probability of value
	 */
	public static double normalPdf(final double x) {
		return normalPdf(x, 0, 1);
	}

	private static final double SQRT_TWO_PI = Math.sqrt(2.0 * Math.PI);

	/**
	 * Normal probability distribution.
	 * Classic bell curve with standard deviation of signa and a mean of mu
	 * @param x input value
	 * @param mu mean of data
	 * @param sigma standard deviation
	 * @return probability of input value occurring
	 */
	public static double normalPdf(final double x, final double mu, final double sigma) {
		return (Math.exp( - Math.pow((x - mu), 2) / 2 / Math.pow(sigma, 2)) / (SQRT_TWO_PI * sigma));
	}

	/**
	 * Normal continuous distribution function.
	 * Shows the likelihood that a value is <= the given value
	 * @param x input test value
	 * @return probability of a value being lower
	 */
	public static double normalCdf(final double x) {
		return normalCdf(x, 0, 1);
	}

	/**
	 * Normal continuous distribution function.
	 * SHows the probability of a value being <= input value
	 * @param x input value
	 * @param mu mean
	 * @param sigma standard deviation
	 * @return probability of x
	 */
	public static double normalCdf(final double x, final double mu, final double sigma) {
		return (1 + Erf.erf((x - mu) / Math.sqrt(2) / sigma)) / 2;
	}

	/**
	 * Find the value at a particular probability.
	 * @param p probability to search for
	 * @return value with that probability
	 */
	public static double inverseNormalCdf(final double p) {
		return inverseNormalCdf(p, 0, 1, .000001);
	}

	@SuppressWarnings("unused")
	/**
	 * Find the value at a particular probability.
	 * @param p probability to search for
	 * @param mu mean value
	 * @param sigma standard deviation
	 * @param tolerance maximum tolerance for search
	 * @return value with given probability
	 */
	public static double inverseNormalCdf(final double p, final double mu, final double sigma, final double tolerance) {
		// find approximate inverse using binary search

	    // if not standard, compute standard and rescale
	    if (mu != 0 || sigma != 1) {
	        return mu + sigma * inverseNormalCdf(p, 0, 1, tolerance);
	    }

	    // normalCdf(-10) is (very close to) 0
	    double lowZ = -10.0;
	    double lowP = 0;

	    // normalCdf(10)  is (very close to) 1
	    double hiZ =  10.0;
	    double hiP = 1;

	    // calculated below
	    double midZ = 0;
	    double midP;

	    while (hiZ - lowZ > tolerance) {
	        midZ = (lowZ + hiZ) / 2.0;    	// consider the midpoint
	        midP = normalCdf(midZ);     	// and the cdf's value there
	        if (midP < p) {
	            // midpoint is still too low, search above it
	            lowZ = midZ;
	            lowP = midP;
	        } else if (midP > p) {
	            // midpoint is still too high, search below it
	            hiZ = midZ;
	            hiP = midP;
	        } else {
	            break;
	        }
	    }

	    return midZ;
	}
	
	/**
	 * Calculate the size of the event space for the given parameters.
	 * 
	 * For example, to calculate the number of events possible
	 * for a 4 digit binary number, the number of positions is 4 and
	 * the number of items is 2 (binary). Thus, the number of 
	 * events is 2^4 or 16.
	 * 
	 * @param numberOfPositions number of positions in the output space
	 * @param numberOfItems number of items available for each position
	 * @return number of possible events
	 */
	public static long getEventCount(int numberOfPositions, int numberOfItems) {
		return (long)Math.pow(numberOfItems, numberOfPositions);
	}
	
	/**
	 * Calculate the number of events where various positions have
	 * different numbers of options.
	 * 
	 * For example, how many events of the numbers 0-9 exist where
	 * the first number is not 0, the second number is anything, and the
	 * third number is odd? 9 * 10 * 5 = 450
	 * 
	 * @param optionsPerPosition number of choices for each position
	 * @return events available
	 */
	public static long getEventCount(int[] optionsPerPosition) {
		long count = optionsPerPosition[0];
		for(int i = 1; i < optionsPerPosition.length; ++i) {
			count *= optionsPerPosition[i];
		}
		return count;
	}
	
	/**
	 * Determine the number of permutations available for a given number of items.
	 * For any given number of items - n, the answer is n!
	 * 
	 * @param numberOfItems number of items in the set
	 * @return number of permutations available
	 */
	public static long getPermCount(int numberOfItems) {
		int count = numberOfItems--;
		while (numberOfItems > 0) {
			count *= numberOfItems--;
		}
		return count;
	}
	
	/**
	 * Determine number of possible outcomes exit for a desired number of items
	 * to exist in a larger set. For example, if there are 15 different Olympians
	 * competing, how many permutations exist for gold, silver, and bronze?
	 * Answer: 15 * 14 * 13 = 2730
	 *  
	 * @param numberOfAvailableItems
	 * @param numberOfDesiredItems
	 * @return
	 */
	public static long getPermCount(int numberOfAvailableItems, int numberOfDesiredItems) {
		int count = numberOfAvailableItems--;	
		while (--numberOfDesiredItems > 0) {
			count *= numberOfAvailableItems--;
		}
		return count;
	}
	
	/**
	 * The Binomial Distribution Theorem:
	 * Suppose an experiment consists of n independent trials. If the event E being
	 * considered is the same for each trial and if its probability for any single
	 * trial is p, then the probability that E will occur exactly r times is given by:
	 * 
	 * (nCr) * (p^r) * (1 - p)^(n-r)
	 * 
	 * To calculate the number of combinations: nCr
	 * nCr = (n!) / ((n-r)! * n!)
	 *
	 * @param n number of trials
	 * @param p probability for each trial
	 * @param r number of occurrences 
	 * @return probability of r occurrences happening in n trials
	 */
	public static double binomialDistributionTheoremProbability(int n, float p, int r) {
		// Since n! may be very large, divide the (n-r)! out before performing factorial
		// this can be accomplished by only calculating the factorial for the first
		// r terms of the problem
		double nCr = n;
		for(int i = 1; i < r; ++i) {
			nCr *= n-i;
		}
		nCr /= factorial(r);
		
		double pPOWr = Math.pow(p, r);
		double pPowNminR = Math.pow(1 - p, n-r);
		return nCr * pPOWr * pPowNminR;
	}
	
	private static long factorial(long n) {
		if (n == 1) {
			return 1;
		} else {
			return n * factorial(n-1);
		}
	}

	private Probability() {
		// hide constructor
	}
}

