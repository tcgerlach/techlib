package com.talixa.techlib.data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.talixa.techlib.math.Vector;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Functions for statistical analysis of data.
 * 
 * @author tcgerlach
 */
public final class Statistics {

	/**
	 * Returns the sum of the array of numbers in data.
	 * @param data a list of the numbers to sum
	 * @return the sum of all numbers in data
	*/
	public static double sum(final double[] data) {
		double sum = 0;
		for (double d : data) {
			sum += d;
		}
		return sum;
	}
	
	/**
	 * Returns the sum of the array of numbers in data.
	 * @param data a list of the numbers to sum
	 * @return the sum of all numbers in data
	*/
	public static float sum(final float[] data) {
		float sum = 0;
		for (float d : data) {
			sum += d;
		}
		return sum;
	}

	/**
	 * Return the mean (average) of all numbers in the data array.
	 * @param data a list of the numbers to average
	 * @return the mean value of the numbers in data
	*/
	public static double mean(final double[] data) {
		return sum(data) / data.length;
	}
	
	/**
	 * Return the mean (average) of all numbers in the data array.
	 * @param data a list of the numbers to average
	 * @return the mean value of the numbers in data
	*/
	public static float mean(final float[] data) {
		return sum(data) / data.length;
	}

	/**
	 * Return the median value of the set of numbers in array.
	 * @param data a list of number
	 * @return the median value in the list
	*/
	public static double median(final double[] data) {
		double[] localData = Arrays.copyOf(data, data.length);
		Arrays.sort(localData);
		int midPoint = data.length / 2;
		if (localData.length % 2 == 1) {
			return localData[midPoint];
		} else {
			return (localData[midPoint - 1] + localData[midPoint]) / 2;
		}
	}

	/**
	 * Determine accuracy of predictions. 
	 * Accuracy defines how often we are right. 
	 * @param truePositive how many positive hits were correct
	 * @param falsePositive how many positive hits were incorrect
	 * @param falseNegative how many false hits were incorrect
	 * @param trueNegative how many false hits were correct
	 * @return accuracy of predictions
	 */
	public static double accuracy(final int truePositive, final int falsePositive, final int falseNegative, final int trueNegative) {
		int correct = truePositive + trueNegative;
		int total = truePositive + falsePositive + falseNegative + trueNegative;
		return (double)correct / (double)total;
	}

	/**
	 * Determine the precision of predictions.
	 * Precision defines how accurate our positive results are.
	 * @param truePositive how many positive hits were correct
	 * @param falsePositive how many positive hits were incorrect
	 * @param falseNegative how many false hits were incorrect
	 * @param trueNegative how many false hits were correct
	 * @return precision of predictions
	 */
	public static double precision(final int truePositive, final int falsePositive, final int falseNegative, final int trueNegative) {
		return (double)truePositive / (double)(truePositive + falsePositive);
	}

	/**
	 * Determine the recall value of our predictions.
	 * Recall defines how many of the positives we caught.
	 * @param truePositive how many positive hits were correct
	 * @param falsePositive how many positive hits were incorrect
	 * @param falseNegative how many false hits were incorrect
	 * @param trueNegative how many false hits were correct
	 * @return recall value of predictions
	 */
	public static double recall(final int truePositive, final int falsePositive, final int falseNegative, final int trueNegative) {
		return (double)truePositive / (double)(truePositive + falseNegative);
	}
 
	/**
	 * Calculate the F1 score of predictions
	 * F1 score is based on recall & precision
	 * @param truePositive how many positive hits were correct
	 * @param falsePositive how many positive hits were incorrect
	 * @param falseNegative how many false hits were incorrect
	 * @param trueNegative how many false hits were correct
	 * @return F1 score of predictions
	 */
	public static double f1Score(final int truePositive, final int falsePositive, final int falseNegative, final int trueNegative) {
		double precision = precision(truePositive, falsePositive, falseNegative, trueNegative);
		double recall = recall(truePositive, falsePositive, falseNegative, trueNegative);
		return (2 * precision * recall) / (precision + recall);
	}

	/**
	 * Find the quantile for the given parameters.
	 * @param data input data to examine
	 * @param percentile value for the quantile we want
	 * @return quantile data at the given percent
	 */
	public static double quantile(final double[] data, final float percentile) {
		int pIndex = (int)(percentile * data.length);
		double[] localData = Arrays.copyOf(data, data.length);
		Arrays.sort(localData);
		return data[pIndex];
	}

	/**
	 * Return the range of a set of data. Range is the max value minus the min value.
	 * @param data a list of number to find the range of
	 * @return the range between min and max
	*/
	public static double range(final double[] data) {
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;

		for (double d : data) {
			if (d > max) {
				max = d;
			}

			if (d < min) {
				min = d;
			}
		}

		return max - min;
	}

	public static double[] deMean(final double[] data) {
		double[] localData = Arrays.copyOf(data, data.length);
		double mean = mean(data);
		for (int i = 0; i < data.length; ++i) {
			localData[i] -= mean;
		}
		return localData;
	}
	
	public static float[] deMean(final float[] data) {
		float[] localData = Arrays.copyOf(data, data.length);
		float mean = mean(data);
		for (int i = 0; i < data.length; ++i) {
			localData[i] -= mean;
		}
		return localData;
	}

	/**
	 * Calculate the interquartile range of a data set.
	 * @param data input data to calculate range of
	 * @return interquartile range of data set
	 */
	public static double interquartileRange(final double[] data) {
		return quantile(data, .75f) - quantile(data, .25f);
	}

	/**
	 * Find the variance of a data set.
	 * @param data input values
	 * @return variance
	 */
	public static double variance(final double[] data) {
		double[] deviations = deMean(data);
		return Vector.sumOfSquares(deviations) / (data.length - 1); 
	}
	
	/**
	 * Find the variance of a data set.
	 * @param data input values
	 * @return variance
	 */
	public static float variance(final float[] data) {
		float[] deviations = deMean(data);
		return Vector.sumOfSquares(deviations) / (data.length - 1); 
	}

	/**
	 * Calculate the standard deviation of a data set
	 * @param data input values to find standard deviation of
	 * @return standard deviation of data set
	 */
	public static double standardDeviation(final double[] data) {
		return Math.sqrt(variance(data));
	}
	
	/**
	 * Calculate the standard deviation of a data set
	 * @param data input values to find standard deviation of
	 * @return standard deviation of data set
	 */
	public static float standardDeviation(final float[] data) {
		return (float)Math.sqrt(variance(data));
	}

	/**
	 * Calculate covariance of two data sets.
	 * @param x input data set 1
	 * @param y input data set 2
	 * @return covariance
	 */
	public static double covariance(final double[] x, final double[] y) {
		return Vector.dotProduct(deMean(x), deMean(y)) / (x.length - 1);
	}

	/**
	 * Calculate correlation between x and y values.
	 * @param x data set 1
	 * @param y data set 2
	 * @return correlation of data sets
	 */
	public static double correlation(final double[] x, final double[] y) {
		double stdDevX = standardDeviation(x);
		double stdDevY = standardDeviation(y);

		if (stdDevX > 0 && stdDevY > 0) {
			return covariance(x, y) / stdDevX / stdDevY;
		} else {
			return 0;
		}
	}

	/**
	 * Calculate the mode of a group of integers.
	 * @param integers input data set
	 * @return mode of data set
	 */
	@SuppressFBWarnings(value="WMI_WRONG_MAP_ITERATOR", justification="Not worried about optimizations for this right now")
	public static int mode(final int[] integers) {
		// count values
		Map<Integer,Integer> counts = new HashMap<Integer,Integer>();
		for (int x : integers) {
			int curVal = 0;
			if (counts.containsKey(x)) {
				curVal = counts.get(x);
			} else {
				curVal = 0;
			}
			counts.put(x, curVal + 1);
		}

		// determine largest
		int largestValue = Integer.MIN_VALUE;
		int key = 0;

		for(int currentKey : counts.keySet()) {
			if (counts.get(currentKey) > largestValue) {
				largestValue = counts.get(currentKey);
				key = currentKey;
			}
		}

		return key;
	}

	private Statistics() {
		// hide constructor
	}
}

