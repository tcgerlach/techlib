package com.talixa.techlib.physics;

public class Temperature {
	
	private static final float K_TO_C = 273.15f;

	/**
	 * Convert temperature in Kelvin to Celsius
	 * @param k temperature in K
	 * @return temperature C
	 */
	public static float kelvinToCelsius(final float k) {
		return k + K_TO_C;
	}
	
	/**
	 * Convert temperature in Kelvin to Farenheit
	 * @param k temperature in K
	 * @return temperature F
	 */
	public static float kelvinToFarenheit(final float k) {
		return celsiusToFarenheit(kelvinToCelsius(k));
	}
	
	/**
	 * Convert temperature in Celsuis to Kelvin
	 * @param c temperature in C
	 * @return temperature K
	 */
	public static float celsiusToKelvin(final float c) {
		return c - K_TO_C;
	}
	
	/**
	 * Convert temperature in Celsius to Farenheit
	 * @param c temperature in C
	 * @return temperature F
	 */
	public static float celsiusToFarenheit(final float c) {
		return c  *  9f/5f + 32f;
	}
	
	/**
	 * Convert temperature in Farenheit to Kelvin
	 * @param f temperature in F
	 * @return temperature K
	 */
	public static float farenheitToKevin(final float f) {
		return celsiusToKelvin(farenheitToCelcius(f));
	}
	
	/**
	 * Convert temperature in Farenheit to Celsius
	 * @param f temperature in F
	 * @return temperature C
	 */
	public static float farenheitToCelcius(final float f) {
		return (f - 32f) * 5f/9f;
	}
}
