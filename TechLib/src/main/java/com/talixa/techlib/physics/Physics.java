package com.talixa.techlib.physics;

import com.talixa.techlib.shared.Vector2D;;

public class Physics {

	public static final float POUNDS_PER_NEWTON = .2248f/1f;
	public static final float GRAVITY_ON_EARTH = 9.8f;
	public static final float GRAVITY_ON_MERCURY = 3.61f;
	public static final float GRAVITY_ON_VENUS = 8.83f;
	public static final float GRAVITY_ON_MARS = 3.75f;
	public static final float GRAVITY_ON_JUPITER = 26f;
	public static final float GRAVITY_ON_SATURN = 11.2f;
	public static final float GRAVITY_ON_URANUS = 10.5f;
	public static final float GRAVITY_ON_NEPTUNE = 13.3f;
	public static final float GRAVITY_ON_PLUTO = .61f;
	
	/**
	 * @param mass in kilograms
	 * @param gravity in m/s^2
	 * @return weight in pounds
	 */
	public static double calculateWeightInPounds(final double mass, final double gravity) {
		return calculateWeightInNewtons(mass, gravity) * POUNDS_PER_NEWTON;
	}
	
	/**
	 * @param mass in kilograms
	 * @param gravity in m/s^2
	 * @return weight in newtons
	 */
	public static double calculateWeightInNewtons(final double mass, final double gravity) {
		return mass * gravity;
	}
	
	/**
	 * @param angle in degrees
	 * @param weight in pounds
	 * @param frictionCoeffecient
	 * @return true if object will move down hill
	 */
	public static boolean checkForDownhillMotion(final double angle, final double weight, final double staticFrictionCoeffecient) {
		// convert weight in pounds to newtons
		double weightInNewtons = weight * POUNDS_PER_NEWTON;
		
		// static friction = friction coeffecient * normal force 
		double friction = staticFrictionCoeffecient * calculateNormalForce(weightInNewtons, angle);
				
		// force perpendicular to normal force
		double perpendicularForce = calculatePerpendicularToNormalForce(weightInNewtons, angle);
		
		// if force against normal force exceeds friction, ball will roll
		return perpendicularForce > friction;
	}
	
	/**
	 * @param absoluteVelocity of movement
	 * @param angle of movement
	 * @return x/y velocities
	 */
	public static Vector2D getVelocity(final float absoluteVelocity, final float angle) {
		Vector2D v = new Vector2D();
		v.setX((float)(absoluteVelocity * Math.cos(Math.toRadians(angle))));
		v.setY((float)(absoluteVelocity * Math.sin(Math.toRadians(angle))));
		return v;
	}
		
	/**
	 * Calculate the current trajectory point using the following equations:
	 * x=x+vt 				GRAVITY DOES NOT EFFECT X MOTION
	 * y=y+vt+(1/2)at^2
	 * 
	 * @param initialLocation of projectile
	 * @param velocity of projectile
	 * @param gravity of projectile environment
	 * @param secondsElapsed since fired
	 * @return current trajectory point
	 */
	public static Vector2D getCurrentTrajectoryPosition(final Vector2D initialLocation, final Vector2D velocity, final float gravity, final float secondsElapsed) {
		Vector2D newPoint = new Vector2D();
		newPoint.setX((float)(initialLocation.getX() + (velocity.getX()*secondsElapsed)));
		newPoint.setY((float)(initialLocation.getY() + (velocity.getY()*secondsElapsed) + (.5f) * gravity * Math.pow(secondsElapsed, 2)));
		return newPoint;
	}
	
	public static Vector2D getCurrentInclineFallPosition(final Vector2D initialLocation, final float inclineAngle, final float mass, final float gravity, final float secondsElapsed, final double kineticFrictionCoeffecient) {
		double acceleration = calculateInclineAcceleration(mass, gravity, inclineAngle, kineticFrictionCoeffecient);	
		// acceleration is pulling the object down - so find the y coordinate.	
		float y = (float)(initialLocation.getY() + (.5f) * acceleration * Math.pow(secondsElapsed, 2));
		// tan(t) = y/x -- solve for x -- tan(90-t)*y = x
		// However, we don't need to know how long x is, but rather how far x is from the original x
		float triangleBottomLength = (float)(Math.tan((90f-inclineAngle) * Math.PI / 180)*y);
		// current x is equal to the initial x + (original bottom length - triangle bottom length)
		float originalBottomLength = (float)(Math.tan((90f-inclineAngle) * Math.PI / 180)*initialLocation.getY());
		// For coordinates on Android, triangleBottom - originalBottom gives the right answer (quadrant 4)
		float x = initialLocation.getX() + (triangleBottomLength - originalBottomLength);		 		
		return new Vector2D(x,y);
	}		
	
	/**
	 * @param mass of object in kilograms
	 * @param gravity in m/s^2
	 * @param angle in degrees
	 * @param kineticFrictionCoeffecient
	 * @return acceleration down the ramp
	 */
	public static double calculateInclineAcceleration(final double mass, final double gravity, final double angle, final double kineticFrictionCoeffecient) {
		double weightInNewtons = calculateWeightInNewtons(mass, gravity);
		double normalForce = calculateNormalForce(weightInNewtons, angle);
		double perpendicularForce  = calculatePerpendicularToNormalForce(weightInNewtons, angle);
		double kineticFriction = kineticFrictionCoeffecient * normalForce;
		double totalForce = perpendicularForce - kineticFriction;
		return totalForce / mass;
	}
	
	private static double calculateNormalForce(final double weightInNewtons, final double angle) {
		return weightInNewtons * Math.cos(Math.toRadians(angle));
	}
	
	private static double calculatePerpendicularToNormalForce(double weightInNewtons, double angle) {
		return weightInNewtons *  Math.sin(Math.toRadians(angle));
	}
}