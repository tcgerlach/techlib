package com.talixa.techlib.binary;

/**
 * Tools to manipulate data at a bit level.
 * 
 * @author tcgerlach
 */
public class BinaryManipulator {
	
	/**
	 * Reverse low 8 bits of input 
	 * @param input data to reverse
	 * @return value of int with 8 bits reversed
	 */
	public static int reverseBits(final int input) {
		return 	(input & 0x01) << 7 |
				(input & 0x02) << 5 |
				(input & 0x04) << 3 |
				(input & 0x08) << 1 |
				(input & 0x10) >> 1 |
				(input & 0x20) >> 3 |
				(input & 0x40) >> 5 |
				(input & 0x80) >> 7;
	}
}
