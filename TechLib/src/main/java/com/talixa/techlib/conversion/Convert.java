package com.talixa.techlib.conversion;

public class Convert {

	public static double feetToMeters(double feet) {
		return feet * .3048;
	}
	
	public static double metersToFeet(double meters) {
		return meters * 3.280839895;
	}
}
