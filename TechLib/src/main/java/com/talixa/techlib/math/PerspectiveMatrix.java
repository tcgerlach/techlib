package com.talixa.techlib.math;

public class PerspectiveMatrix {

	/**
	 * Create a perspective matrix.
	 * 
	 * @param outputMatrix output of matrix
	 * @param yFovInDegrees field of view in degrees
	 * @param aspect aspect ratio
	 * @param n near point
	 * @param f far point
	 */
	public static void get(float[] outputMatrix, final float yFovInDegrees, final float aspect, final float n, final float f) {
		float angleInRadians = (float)(yFovInDegrees * Math.PI / 180.0);
		float a = (float)(1.0/Math.tan(angleInRadians/2.0));	// focal length
		
		outputMatrix[0] = a / aspect;
		outputMatrix[1] = 0f;
		outputMatrix[2] = 0f;
		outputMatrix[3] = 0f;
		
		outputMatrix[4] = 0f;
		outputMatrix[5] = a;
		outputMatrix[6] = 0f;
		outputMatrix[7] = 0f;
		
		outputMatrix[8] = 0f;
		outputMatrix[9] = 0f;
		outputMatrix[10] = -((f+n)/(f-n));
		outputMatrix[11] = -1f;
		
		outputMatrix[12] = 0f;
		outputMatrix[13] = 0f;
		outputMatrix[14] = -((2f * f * n)/(f-n));
		outputMatrix[15] = 0f;
	}
}
