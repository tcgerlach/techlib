package com.talixa.techlib.math.interfaces;

public interface Function {
	/**
	 * Execute this function with the given input values.
	 * @param input values to input into function
	 * @return result of function
	 */
	public double execute(final double[] input);
}
