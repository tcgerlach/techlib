package com.talixa.techlib.math;

public class Mathematics {

	public static int factorCount(int number) {
		int count = 0;
		
		// Check all numbers from the square root down 
		// for divisibility. If a number is evenly divisible,
		// add 2 factors (the number, and the quotient)
		int sqrt = (int)Math.ceil(Math.sqrt(number));
		for(int i = 1; i <= sqrt; ++i) {
			if (number % i == 0) {
				++count;
				
				// don't add sqrt twice
				if (i != sqrt) {
					++count;
				}
			}
		}
		
		return count;
	}
	
	public static int[] getFactors(int number) {
		int count = factorCount(number);
		int[] factors = new int[count];
		int lastIndex = 0;
		
		// Check all numbers from the square root down 
		// for divisibility. If a number is evenly divisible,
		// add 2 factors (the number, and the quotient)
		int sqrt = (int)Math.ceil(Math.sqrt(number));
		for(int i = 1; i <= sqrt; ++i) {
			if (number % i == 0) {
				factors[lastIndex++] = i;
				
				if (i != sqrt) {
					factors[count-lastIndex] = number / i;
				}
			}
		}
		
		return factors;
	}
}
