package com.talixa.techlib.math;

public class Polynomial {
	
	/**
	 * Run the polynomial.
	 * @param input input x value
	 * @param coefficients list of coefficients for polynomial
	 * @return calculated result for polynomial
	 */
	public static float calculate(final float input, final float[] coefficients) {
		float sum = 0;
		int degree = coefficients.length-1;
		for(int i = 0; i < coefficients.length; ++i) {
			sum += Math.pow(input, (degree - i)) * coefficients[i];
		}
		return sum;
	}
}
