package com.talixa.techlib.math;

import com.talixa.techlib.math.interfaces.Function;

/**
 * Method to check the properties of functions.
 * @author tcgerlach
 */
public final class FunctionChecker {

	private static final double TOLERANCE = .000001;
	private static final double[] POSITIVE_INPUT = new double[] {1};
	private static final double[] NEGATIVE_INPUT = new double[] {-1};

	/**
	 * Check if a function is odd.
	 * @param func the function to check
	 * @return true if this function is odd
	 */
	public static boolean isOdd(final Function func) {
		return checkForEquality(func.execute(NEGATIVE_INPUT), (-1 * func.execute(POSITIVE_INPUT)));
	}

	/**
	 * Check if a function is even.
	 * @param func the function to check
	 * @return true if this function is even
	 */
	public static boolean isEven(final Function func) {
		return checkForEquality(func.execute(POSITIVE_INPUT), func.execute(NEGATIVE_INPUT));
	}

	/**
	 * Check if a given function has symmetry on the X axis.
	 * @param func input function to check
	 * @return true if x symmetry exists
	 */
	public static boolean hasSymmetryOnX(final Function func) {
		for(int i = 1; i < 10; ++i) {
			double y1 = func.execute(POSITIVE_INPUT);
			double y2 = func.execute(NEGATIVE_INPUT);
			if (!checkForEquality(y1,y2)) {
				return false;
			}
		}
		return true;
	}

	/** 
	 * Check if a given function has symmetry at the origin.
	 * @param func input function to check
	 * @return true if origin symmetry exists
	 */
	public static boolean hasSymmetryOnOrigin(final Function func) {
		for(int i = 1; i < 10; ++i) {
			double y1 = func.execute(POSITIVE_INPUT);
			double y2 = func.execute(NEGATIVE_INPUT);
			if (!checkForEquality(y1, (y2 * -1))) {
				return false;
			}
		}
		return true;
	}

	private static boolean checkForEquality(final double d1, final double d2) {
		return Math.abs(d1 - d2) < TOLERANCE;
	}

	private FunctionChecker() {
		// no public constructor
	}
} 
