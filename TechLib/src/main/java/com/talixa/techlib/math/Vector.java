package com.talixa.techlib.math;

public final class Vector {

	/**
	 * Add two vectors together and return the result. Both vectors must be
	 * the same size.
	 * 
	 * @param lhs first vector array
	 * @param rhs second vector array
	 * @return an array with the sum of the two vectors
	*/
	public static double[] addVectors(final double[] lhs, final double[] rhs) {
		if (lhs.length != rhs.length) {
			throw new RuntimeException("Vectors must be same size");
		}

		double[] result = new double[lhs.length];

		for (int i = 0; i < lhs.length; ++i) {
			result[i] = lhs[i] + rhs[i];
		}

		return result;
	}
	
	/**
	 * Add two vectors together and return the result. Both vectors must be
	 * the same size.
	 * 
	 * @param lhs first vector array
	 * @param rhs second vector array
	 * @return an array with the sum of the two vectors
	*/
	public static float[] addVectors(final float[] lhs, final float[] rhs) {
		if (lhs.length != rhs.length) {
			throw new RuntimeException("Vectors must be same size");
		}

		float[] result = new float[lhs.length];

		for (int i = 0; i < lhs.length; ++i) {
			result[i] = lhs[i] + rhs[i];
		}

		return result;
	}

	/**
	 * Subtract two vectors. Note, both vectors must be the same size.
	 * 
	 * @param lhs the first vector array
	 * @param rhs the second vector array
	 * @return an array with the difference of the two vectors
	*/
	public static double[] subtractVectors(final double[] lhs, final double[] rhs) {
		if (lhs.length != rhs.length) {
			throw new RuntimeException("Vectors must be same size");
		}

		double[] result = new double[lhs.length];

		for (int i = 0; i < lhs.length; ++i) {
			result[i] = lhs[i] - rhs[i];
		}

		return result;
	}
	
	/**
	 * Subtract two vectors. Note, both vectors must be the same size.
	 * 
	 * @param lhs the first vector array
	 * @param rhs the second vector array
	 * @return an array with the difference of the two vectors
	*/
	public static float[] subtractVectors(final float[] lhs, final float[] rhs) {
		if (lhs.length != rhs.length) {
			throw new RuntimeException("Vectors must be same size");
		}

		float[] result = new float[lhs.length];

		for (int i = 0; i < lhs.length; ++i) {
			result[i] = lhs[i] - rhs[i];
		}

		return result;
	}

	/**
	 * Multiple each element in the vector by a scaler. 
	 * 
	 * @param vector the array of numbers to scale
	 * @param scaler the number to scale the vector by
	 * @return a new array scaled by scaler
	*/
	public static double[] scaleVector(final double[] vector, final double scaler) {
		double[] result = new double[vector.length];
		for (int i = 0; i < vector.length; ++i) {
			result[i] = vector[i] * scaler;
		}
		return result;
	}
	
	/**
	 * Multiple each element in the vector by a scaler. 
	 * 
	 * @param vector the array of numbers to scale
	 * @param scaler the number to scale the vector by
	 * @return a new array scaled by scaler
	*/
	public static float[] scaleVector(final float[] vector, final float scaler) {
		float[] result = new float[vector.length];
		for (int i = 0; i < vector.length; ++i) {
			result[i] = vector[i] * scaler;
		}
		return result;
	}

	/**
	 * Return the sum of the squares of an array.
	 * 
	 * @param data the input dataset
	 * @return the sum of all squares of the dataset
	*/
	public static double sumOfSquares(final double[] data) {
		double soq = 0;
		for (int i = 0; i < data.length; ++i) {
			soq += (data[i] * data[i]);
		}
		return soq;
	}
	
	/**
	 * Return the sum of the squares of an array.
	 * 
	 * @param data the input dataset
	 * @return the sum of all squares of the dataset
	*/
	public static float sumOfSquares(final float[] data) {
		float soq = 0;
		for (int i = 0; i < data.length; ++i) {
			soq += (data[i] * data[i]);
		}
		return soq;
	}

	/**
	 * Calculate the dot product of two vectors.
	 * 
	 * @param lhs the first vector
	 * @param rhs the second vector
	 * @return the dot product of the two vectors.
	*/
	public static double dotProduct(final double[] lhs, final double[] rhs) {
		if (lhs.length != rhs.length) {
			throw new RuntimeException("Vectors must be same size");
		}

		double product = 0;
		for (int i = 0; i < lhs.length; ++i) {
			product += lhs[i] * rhs[i];
		}

		return product;
	}
	
	/**
	 * Calculate the dot product of two vectors.
	 * 
	 * @param lhs the first vector
	 * @param rhs the second vector
	 * @return the dot product of the two vectors.
	*/
	public static float dotProduct(final float[] lhs, final float[] rhs) {
		if (lhs.length != rhs.length) {
			throw new RuntimeException("Vectors must be same size");
		}

		float product = 0;
		for (int i = 0; i < lhs.length; ++i) {
			product += lhs[i] * rhs[i];
		}

		return product;
	}
	
	/**
	 * Calculate the sum of the elements of a vector.
	 * 
	 * @param vector input values to sum
	 * @return sum of all input values
	 */
	public static double sumVector(final double[] vector) {
		double sum = 0;
		for(int i = 0; i < vector.length; ++i) {
			sum += vector[i];
		}
		return sum;
	}
	
	/**
	 * Calculate the sum of the elements of a vector.
	 * 
	 * @param vector input values to sum
	 * @return sum of all input values
	 */
	public static float sumVector(final float[] vector) {
		float sum = 0;
		for(int i = 0; i < vector.length; ++i) {
			sum += vector[i];
		}
		return sum;
	}

	private Vector() {
		// hide default constructor
	}
}