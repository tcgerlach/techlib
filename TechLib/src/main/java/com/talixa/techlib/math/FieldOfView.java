package com.talixa.techlib.math;

import com.talixa.techlib.shared.Vector2D;

public class FieldOfView {
		
	/**
	 * Determine if an observer is able to see an object.
	 * @param observer position of the observer
	 * @param object position of the observed object
	 * @param facing direction the observer is looking in degrees
	 * @param fov observer field of view in degrees
	 * @return true if the object is visible
	 */
	public static boolean canSee(Vector2D observer, Vector2D object, int facing, int fov) {
		float opposite = observer.getY() - object.getY();
		float adjacent = object.getX() - observer.getX();		
		double angleToObject = Math.atan(opposite/adjacent);
						
		// Remember that coordinates in Java start with 0,0 in top left
		if (object.getX() >= observer.getX() && object.getY() >= observer.getY()) {
			// Quadrant 4
			angleToObject += (2*Math.PI);
		} else if (object.getX()  <= observer.getX() && object.getY() >= observer.getY()) {
			// Quadrant 3
			angleToObject += Math.PI;			
		} else if (object.getX() <= observer.getX() && object.getY() <= observer.getY()) {
			// Quadrant 2
			angleToObject += Math.PI;
		} else {
			// Quadrant 1
			// DO NOTHING
		}
		angleToObject *= (180/Math.PI);
		
		double maxObserverAngle = (facing + fov/2);
		double minObserverAngle = (facing - fov/2);
		
		return angleToObject >=  minObserverAngle && angleToObject <= maxObserverAngle;
	}
}
