package com.talixa.techlib.audio.exceptions;

/**
 * WAVE file format error
 * 
 * @author tcgerlach
 */
@SuppressWarnings("serial")
public class WaveFormatException extends RiffFormatException {
	public WaveFormatException(final String msg) {
		super(msg);
	}	
}
