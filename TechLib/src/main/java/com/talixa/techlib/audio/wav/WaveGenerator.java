package com.talixa.techlib.audio.wav;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.talixa.techlib.audio.riff.chunk.AudioDataChunk;
import com.talixa.techlib.audio.riff.chunk.FormatChunk;

/**
 * Functions to create a new wave file
 * 
 * @author tcgerlach
 */
public class WaveGenerator {
	
	public static WaveFile generateWaveFromRawMulaw(String mulawFile) throws IOException {
		return generateWaveFromRawMulaw(getDataFromFile(mulawFile));
	}
	
	public static WaveFile generateWaveFromRawAlaw(String alawFile) throws IOException {
		return generateWaveFromRawAlaw(getDataFromFile(alawFile));
	}
	
	public static WaveFile generateWaveFromRaw16bitPcm(String pcmFile) throws IOException {
		return generateWaveFromRaw16bitPcm(getDataFromFile(pcmFile));
	}
	
	public static WaveFile generateWaveFromRaw8bitPcm(String pcmFile) throws IOException {
		return generateWaveFromRaw8bitPcm(getDataFromFile(pcmFile));
	}
	
	public static WaveFile generateWaveFromRawMulaw(byte[] audioData) {		
		WaveFile wave = new WaveFile();	
		wave.setFormatChunk(createMulawFormatChunk());
		wave.setAudioDataChunk(createAudioDataChunkFromBytes(audioData));		
		return wave;
	}
	
	public static WaveFile generateWaveFromRawAlaw(byte[] audioData) {		
		WaveFile wave = new WaveFile();	
		wave.setFormatChunk(createAlawFormatChunk());
		wave.setAudioDataChunk(createAudioDataChunkFromBytes(audioData));		
		return wave;
	}
	
	public static WaveFile generateWaveFromRaw16bitPcm(byte[] audioData) {		
		WaveFile wave = new WaveFile();	
		wave.setFormatChunk(create16bitPcmFormatChunk());
		wave.setAudioDataChunk(createAudioDataChunkFromBytes(audioData));		
		return wave;
	}
	
	public static WaveFile generateWaveFromRaw8bitPcm(byte[] audioData) {		
		WaveFile wave = new WaveFile();	
		wave.setFormatChunk(create8bitPcmFormatChunk());
		wave.setAudioDataChunk(createAudioDataChunkFromBytes(audioData));		
		return wave;
	}
		
	private static byte[] getDataFromFile(String fileName) throws IOException {
		File file = null;
		InputStream is = null;
		byte[] data = null;
		try {
			file = new File(fileName);	
			is = new FileInputStream(file);		
			long fileLength = file.length();
			data = new byte[(int)fileLength];		
			is.read(data);		
		} finally {
			if (is != null) {
				is.close();
			}
		}
		
		return data;
		
	}
	
	private static AudioDataChunk createAudioDataChunkFromBytes(byte[] audioData) {
		AudioDataChunk chunk = new AudioDataChunk();
		chunk.setChunkData(audioData);
		return chunk;
	}
	
	private static FormatChunk createMulawFormatChunk() {
		FormatChunk chunk = new FormatChunk();
		chunk.setAudioFormat(FormatChunk.AUDIO_FORMAT_MU_LAW);
		chunk.setBitsPerSample(FormatChunk.BITS_PER_SAMPLE_8);
		chunk.setNumChannels(FormatChunk.NUM_CHANNELS_1);
		chunk.setSampleRate(FormatChunk.SAMPLE_RATE_8K);
		return chunk;
	}
	
	private static FormatChunk createAlawFormatChunk() {
		FormatChunk chunk = new FormatChunk();
		chunk.setAudioFormat(FormatChunk.AUDIO_FORMAT_A_LAW);
		chunk.setBitsPerSample(FormatChunk.BITS_PER_SAMPLE_8);
		chunk.setNumChannels(FormatChunk.NUM_CHANNELS_1);
		chunk.setSampleRate(FormatChunk.SAMPLE_RATE_8K);
		return chunk;
	}
	
	private static FormatChunk create16bitPcmFormatChunk() {
		FormatChunk chunk = new FormatChunk();
		chunk.setAudioFormat(FormatChunk.AUDIO_FORMAT_PCM);
		chunk.setBitsPerSample(FormatChunk.BITS_PER_SAMPLE_16);
		chunk.setNumChannels(FormatChunk.NUM_CHANNELS_1);
		chunk.setSampleRate(FormatChunk.SAMPLE_RATE_8K);
		return chunk;
	}
	
	private static FormatChunk create8bitPcmFormatChunk() {
		FormatChunk chunk = new FormatChunk();
		chunk.setAudioFormat(FormatChunk.AUDIO_FORMAT_PCM);
		chunk.setBitsPerSample(FormatChunk.BITS_PER_SAMPLE_8);
		chunk.setNumChannels(FormatChunk.NUM_CHANNELS_1);
		chunk.setSampleRate(FormatChunk.SAMPLE_RATE_8K);
		return chunk;
	}				
}
