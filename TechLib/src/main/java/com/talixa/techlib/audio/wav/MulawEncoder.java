package com.talixa.techlib.audio.wav;

/**
 * This code converted to Java from the original C program.
 * The original can be found at:
 * http://web.mit.edu/Source/third/gst-plugins/gst/law/mulaw-conversion.c
 * 
 *@author tcgerlach
 */
public class MulawEncoder {

	private static final boolean ZEROTRAP = true;	/* turn on the trap as per the MIL-STD */
	private static final int BIAS = 0x84;           /* define the add-in bias for 16 bit samples */
	private static final int CLIP = 32635;

	private static final short encodeLookup[] = {
		0, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3,
	    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
	    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
	    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
	    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
	    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
	    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
	    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7
	  };

	/**
	 * This routine converts from linear to ulaw
	 * 29 September 1989
	 *
	 * Craig Reese: IDA/Supercomputing Research Center
	 * Joe Campbell: Department of Defense
	 *
	 * References:
	 * 1) CCITT Recommendation G.711  (very difficult to follow)
	 * 2) "A New Digital Technique for Implementation of Any
	 *     Continuous PCM Companding Law," Villeret, Michel,
	 *     et al. 1973 IEEE Int. Conf. on Communications, Vol 1,
	 *     1973, pg. 11.12-11.17
	 * 3) MIL-STD-188-113,"Interoperability and Performance Standards
	 *     for Analog-to_Digital Conversion Techniques,"
	 *     17 February 1987
	 *
	 * Input: Signed 16 bit linear sample
	 * Output: 8 bit ulaw sample
	 * 
	 * @param pcmBuffer input PCM buffer
	 * @return PCM converted to muLaw
	 */
	public static byte[] convertPcmSampleToMulaw(short[] pcmBuffer) {
		short sign, exponent, mantissa, sample;
		byte ulawbyte;
		byte[] outputBuffer = new byte[pcmBuffer.length];

		for (int i = 0; i < pcmBuffer.length; i++) {
			sample = pcmBuffer[i];
			/** get the sample into sign-magnitude * */
			sign = (short) ((sample >> 8) & 0x80); /* set aside the sign */
			if (sign != 0) {
				sample = (short) -sample; /* get magnitude */
			}
			/*
			 * sample can be zero because we can overflow in the inversion,
			 * checking against the unsigned version solves this
			 */
			if (((short) sample) > CLIP) {
				sample = CLIP; /* clip the magnitude */
			}

			/** convert from 16 bit linear to ulaw * */
			sample = (short) (sample + BIAS);
			exponent = encodeLookup[(sample >> 7) & 0xFF];
			mantissa = (short) ((sample >> (exponent + 3)) & 0x0F);
			ulawbyte = (byte) ~(sign | (exponent << 4) | mantissa);
			if (ZEROTRAP) {
				if (ulawbyte == 0) {
					ulawbyte = 0x02; /* optional CCITT trap */
				}
			}
			outputBuffer[i] = ulawbyte;
		}

		return outputBuffer;
	}

	private static final int[] ULAW_TO_LINEAR_16_BIT = new int[]{
        -32124, -31100, -30076, -29052, -28028, -27004, -25980, -24956, -23932, -22908, -21884, -20860, -19836, -18812,
        -17788, -16764, -15996, -15484, -14972, -14460, -13948, -13436, -12924, -12412, -11900, -11388, -10876, -10364,
        -9852, -9340, -8828, -8316, -7932, -7676, -7420, -7164, -6908, -6652, -6396, -6140, -5884, -5628, -5372, -5116,
        -4860, -4604, -4348, -4092, -3900, -3772, -3644, -3516, -3388, -3260, -3132, -3004, -2876, -2748, -2620, -2492,
        -2364, -2236, -2108, -1980, -1884, -1820, -1756, -1692, -1628, -1564, -1500, -1436, -1372, -1308, -1244, -1180,
        -1116, -1052, -988, -924, -876, -844, -812, -780, -748, -716, -684, -652, -620, -588, -556, -524, -492, -460,
        -428, -396, -372, -356, -340, -324, -308, -292, -276, -260, -244, -228, -212, -196, -180, -164, -148, -132, -120,
        -112, -104, -96, -88, -80, -72, -64, -56, -48, -40, -32, -24, -16, -8, 0, 32124, 31100, 30076, 29052, 28028,
        27004, 25980, 24956, 23932, 22908, 21884, 20860, 19836, 18812, 17788, 16764, 15996, 15484, 14972, 14460, 13948,
        13436, 12924, 12412, 11900, 11388, 10876, 10364, 9852, 9340, 8828, 8316, 7932, 7676, 7420, 7164, 6908, 6652, 6396,
        6140, 5884, 5628, 5372, 5116, 4860, 4604, 4348, 4092, 3900, 3772, 3644, 3516, 3388, 3260, 3132, 3004, 2876, 2748,
        2620, 2492, 2364, 2236, 2108, 1980, 1884, 1820, 1756, 1692, 1628, 1564, 1500, 1436, 1372, 1308, 1244, 1180, 1116,
        1052, 988, 924, 876, 844, 812, 780, 748, 716, 684, 652, 620, 588, 556, 524, 492, 460, 428, 396, 372, 356, 340,
        324, 308, 292, 276, 260, 244, 228, 212, 196, 180, 164, 148, 132, 120, 112, 104, 96, 88, 80, 72, 64, 56, 48, 40,
        32, 24, 16, 8, 0};

	/**
	 * Converts muLaw data back into PCM
	 * @param mulawBuffer input data in mulaw format
	 * @return audio data converted to PCM
	 */
	public static short[] convertMulawSampleToPcm(byte[] mulawBuffer) {
		short[] outputBuffer = new short[mulawBuffer.length];

		for (int i = 0; i < mulawBuffer.length; i++) {
			short value = (short)mulawBuffer[i];
			if (value < 0) {
				value = (short)(value + 256);
			}
			outputBuffer[i] = (short)ULAW_TO_LINEAR_16_BIT[value];
		}

		return outputBuffer;
	}

	/**
	 * Encode a single sample of pcm data to mulaw
	 *
	 * @param pcmSample a single PCM sample
	 * @return the corresponding muLaw sample value
	 */
	public static byte convertPcmSampleToMulaw(short pcmSample) {
		return convertPcmSampleToMulaw(new short[]{pcmSample})[0];
	}

	/**
	 * Decode a single sample of mulaw data to pcm
	 * 
	 * @param mulawSample a single mulaw sample
	 * @return the corresponding PCM sample
	 */
	public static short convertMulawSampleToPcm(byte mulawSample) {
		return convertMulawSampleToPcm(new byte[]{mulawSample})[0];
	}
}