package com.talixa.techlib.audio.wav;

import com.talixa.techlib.arch.EndianConverter;
import com.talixa.techlib.audio.riff.RiffFile;
import com.talixa.techlib.audio.riff.chunk.AudioDataChunk;
import com.talixa.techlib.audio.riff.chunk.FormatChunk;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A standard RIFF wave file
 * 
 * @author tcgerlach
 */
public class WaveFile extends RiffFile {

	@SuppressFBWarnings(value="MS_PKGPROTECT", justification="This should be usable by anything")
	public static final byte[] WAVE_HEADER = {'W', 'A', 'V', 'E'};
	
	protected FormatChunk formatChunk;
	protected AudioDataChunk audioDataChunk;
	
	public byte[] getRiffHeader() {
		byte[] header = new byte[12];
		System.arraycopy(RIFF_HEADER,0,header,0,4);		
		System.arraycopy(EndianConverter.javaIntToBigEndianBytes(getRiffDataSize()), 0, header,4,4);
		System.arraycopy(WAVE_HEADER,0,header,8,4);
		return header;
	}
	
	public int getRiffDataSize() {
		return formatChunk.getByteLength() + audioDataChunk.getByteLength() + 4;  // +4 for 'WAVE';
	}

	@SuppressFBWarnings(value="EI_EXPOSE_REP", justification="Not a serious concern")
	public byte[] getRiffDataType() {
		return WAVE_HEADER;
	}
	
	public FormatChunk getFormatChunk() {
		return formatChunk;
	}
	
	public AudioDataChunk getAudioDataChunk() {
		return audioDataChunk;
	}
	
	public void setFormatChunk(FormatChunk newFormat) {
		this.formatChunk = newFormat;
	}
	
	public void setAudioDataChunk(AudioDataChunk newAudioData){
		this.audioDataChunk = newAudioData;
	}

	public byte[] getBytes() {
		byte[] waveData = new byte[getRiffDataSize() + 8];
				
		int dataStartByte   = 12 + formatChunk.getByteLength(); 
		
		System.arraycopy(getRiffHeader(), 			0, waveData, 0,12);
		System.arraycopy(formatChunk.getBytes(), 	0, waveData,12,formatChunk.getByteLength());
		System.arraycopy(audioDataChunk.getBytes(), 0, waveData,dataStartByte, audioDataChunk.getByteLength());
		return waveData;
	}
	
	public int getNumberOfSamples() {
		return audioDataChunk.getChunkData().length / formatChunk.getNumChannels() / (formatChunk.getBitsPerSample()/8);
	}
	
	// sample will be 8-bits or 16-bits depending on the number of bits per sample
	public int getSampleAt(int position) {
		return audioDataChunk.getAudioSampleAt(formatChunk.getBitsPerSample(), position);
	}
}
