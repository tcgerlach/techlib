package com.talixa.techlib.audio.exceptions;

/**
 * Exception for any error in the formatting of a chunk
 * 
 * @author tcgerlach
 */
@SuppressWarnings("serial")
public class ChunkFormatException extends Exception {

	public ChunkFormatException() {
	}
	
	public ChunkFormatException(String string) {
		super(string);
	}	
}
