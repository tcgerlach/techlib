package com.talixa.techlib.audio.riff.chunk;

import com.talixa.techlib.arch.EndianConverter;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * An audio chunk consists of a header containing 'data' and a 
 * 4 byte little endian data size followed the audio data
 * 
 * @author tcgerlach
 */
@SuppressWarnings("serial")
public class AudioDataChunk extends Chunk {

	@SuppressFBWarnings(value="MS_MUTABLE_ARRAY", justification="This poses little risk")
	public static final byte[] AUDIO_DATA_CHUNK_HEADER = {'d', 'a', 't', 'a'};
	
	private byte[] data;
	
	@SuppressFBWarnings(value="EI_EXPOSE_REP", justification="Not concerned with this")
	public byte[] getChunkHeader() {
		return AUDIO_DATA_CHUNK_HEADER;
	}

	public int getChunkLength() {
		return data.length;
	}

	@SuppressFBWarnings(value="EI_EXPOSE_REP", justification="Intentionaly exposing data")
	public byte[] getChunkData() {
		return data;
	}

	@SuppressFBWarnings(value="EI_EXPOSE_REP2", justification="Intentionally setting as pointer")
	public void setChunkData(byte[] chunkData) {
		data = chunkData;	
	}
	
	public int getAudioSampleAt(int sampleSize, int index) {
		if (sampleSize == 8) {
			return data[index];
		} else {
			return EndianConverter.littleEndianShortToJavaInt(data[index+1],data[index]);
		}
	}
	
	public byte[] getBytes() {	
		byte[] audioData = new byte[getByteLength()];
		System.arraycopy(AUDIO_DATA_CHUNK_HEADER, 0, audioData, 0, 4);
		System.arraycopy(EndianConverter.javaIntToBigEndianBytes(data.length), 0, audioData, 4, 4);
		System.arraycopy(data, 0, audioData, 8, data.length);
		return audioData;
	}

	public int getByteLength() {
		return getChunkLength() + 8; // +4 for header & +4 for data length;
	}
}
