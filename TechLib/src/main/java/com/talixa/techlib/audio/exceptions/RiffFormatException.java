package com.talixa.techlib.audio.exceptions;

/**
 * Error with RIFF format
 * 
 * @author tcgerlach
 */
@SuppressWarnings("serial")
public class RiffFormatException extends Exception {
	public RiffFormatException(final String msg) {
		super(msg);
	}
}
