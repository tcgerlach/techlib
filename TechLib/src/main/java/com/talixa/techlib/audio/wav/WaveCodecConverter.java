package com.talixa.techlib.audio.wav;

import java.io.File;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

/**
 * Command line application to do wave file conversion
 * 
 * @author tcgerlach
 */
public class WaveCodecConverter {
	
	public static final AudioFormat MULAW_8K 	= new AudioFormat(new AudioFormat.Encoding("ULAW"), 		8000, 8, 1, 1, 8000, true);
	public static final AudioFormat PCM_16BIT   = new AudioFormat(new AudioFormat.Encoding("PCM_SIGNED"), 	8000, 16, 1, 2, 8000, false);
	public static final AudioFormat PCM_8BIT   	= new AudioFormat(new AudioFormat.Encoding("PCM_UNSIGNED"), 8000, 8, 1, 1, 8000, false);
	public static final AudioFormat ALAW_8K  	= new AudioFormat(new AudioFormat.Encoding("ALAW"), 		8000, 8, 1, 1, 8000, false);
	
	/**
	 * Convert input file to 8K mulaw
	 * 
	 * @param infile input file name
	 * @param outfile output file name
	 */
	public static void convertToMulaw(String infile, String outfile) {		
		convert(infile,outfile,MULAW_8K);		
	}
	
	/**
	 * Convert input file to 16 bit PCM
	 * 
	 * @param infile input file name
	 * @param outfile output file name
	 */
	public static void convertToPcm(String infile, String outfile) {	
		convert(infile,outfile,PCM_16BIT);	
	}
	
	/**
	 * Convert input file to 8K alaw
	 * 
	 * @param infile input file name
	 * @param outfile output file name
	 */
	public static void convertToAlaw(String infile, String outfile) {	
		convert(infile,outfile,ALAW_8K);			
	}
	
	private static void convert(String infile, String outfile, AudioFormat targetFormat) {
		try {			
			// Create files
			File sourceFile = new File(infile);		 
			File targetFile = new File(outfile);	
			
			// Create streams
			AudioInputStream sourceAudioInputStream = AudioSystem.getAudioInputStream(sourceFile);		 		
			AudioInputStream targetAudioInputStream = AudioSystem.getAudioInputStream(targetFormat, sourceAudioInputStream);			
			
			// Write output
			AudioSystem.write(targetAudioInputStream, AudioFileFormat.Type.WAVE, targetFile);
			
			// Close streams
			sourceAudioInputStream.close();
			targetAudioInputStream.close();						
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
