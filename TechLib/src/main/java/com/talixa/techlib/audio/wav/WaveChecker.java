package com.talixa.techlib.audio.wav;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Checks a file to see if it has a RIFF or WAVE header
 * 
 * @author tcgerlach
 */
public class WaveChecker {

	public static boolean isRiffFile(File inputFile) throws IOException {
		boolean isRiff = false;

    	FileInputStream fis = new FileInputStream(inputFile);
        try {
        	byte[] riff = new byte[4];        	

        	if (fis.available() >= 4) {
        		fis.read(riff,0,4);
        		//see if a riff header already exists
        		if(riff[0]=='R'&&riff[1]=='I'&&riff[2]=='F'&&riff[3]=='F') {
        			isRiff = true;
        		} else {
        			isRiff = false;
        		}
        	} else {
        		isRiff = false;
        	}
        } finally {
        	fis.close();	
        }
        return isRiff;
	}

	public static boolean isRiffFile(String inputFile) throws IOException {
		return isRiffFile(new File(inputFile));
	}

	public static boolean isWaveFile(String inputFile) throws IOException {
		return isWaveFile(new File(inputFile));
	}
	
	public static boolean isWaveFile(File inputFile) throws IOException {
		boolean isWave = false;

    	FileInputStream fis = new FileInputStream(inputFile);
        try {
        	byte[] header = new byte[12]; 

        	if (fis.available() >= 12) {
        		fis.read(header,0,12);
        		//see if a riff header already exists
        		if(
        				header[0]=='R' && header[1]=='I'&&header[2] =='F'&&header[3] =='F'&&
        				header[8]=='W' && header[9]=='A'&&header[10]=='V'&&header[11]=='E'
        			) 
        		{
        			isWave = true;
        		} else {
        			isWave = false;
        		}
        	} else {
        		isWave = false;
        	}
        } finally {
        	fis.close();
        } 
        return isWave;
	}

}
