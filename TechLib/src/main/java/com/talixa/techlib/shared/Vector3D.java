package com.talixa.techlib.shared;

public class Vector3D {

	private float x;
	private float y;
	private float z;
	
	public Vector3D() {
		// DO NOTHING
	}
	
	public Vector3D(float x, float y, float z) {	
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}
	
	public float getMagnitude() {
		return (float)Math.sqrt(x*x+y*y+z*z);
	}
	
	public void addVector(Vector3D other) {
		this.x += other.x;
		this.y += other.y;
		this.z += other.z;
	}
	
	public void subtractVector(Vector3D other) {
		this.x -= other.x;
		this.y -= other.y;
		this.z -= other.z;
	}
	
	public static Vector3D addVectors(Vector3D lhs, Vector3D rhs) {
		return new Vector3D (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
	}
	
	public static Vector3D subtractVectors(Vector3D lhs, Vector3D rhs) {
		return new Vector3D(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
	}
	
	public static Vector3D scaleVector(Vector3D lhs, float rhs) {
		return new Vector3D (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
	}
	
	public void scale(float scaler) {
		x *= scaler;
		y *= scaler;
		z *= scaler;
	}
	
	public void normalize() {				
		float mag = getMagnitude();
		
		if (mag == 0) {
			return;
		}
		
		this.x /= mag;
		this.y /= mag;
		this.z /= mag;
	}
	
	public float dotProduct(Vector3D vector) {
		return (this.x * vector.x) + (this.y * vector.y) + (this.z * vector.z);
	}
	
	public float getDistance(Vector3D other) {
		float xdif = other.x - this.x;
		float ydif = other.y - this.y;
		float zdif = other.z - this.z;
		return (float)Math.sqrt(xdif * xdif + ydif * ydif + zdif * zdif);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		result = prime * result + Float.floatToIntBits(z);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector3D other = (Vector3D) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vector3D [x=" + x + ", y=" + y + ", z=" + z + "]";
	}
}
