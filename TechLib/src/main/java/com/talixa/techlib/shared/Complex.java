package com.talixa.techlib.shared;

/**
 * Implementation of a complex number.
 * All complex numbers are immutable.
 * 
 * @author tcgerlach
 */
public class Complex {
	
    private final double real;   		// the real part
    private final double imaginary;   	// the imaginary part

    /**
     * Create a new complex object.
     * 
     * @param r real component of complex
     * @param i imaginary component of complex
     */
    public Complex(final double r, final double i) {
        this.real = r;
        this.imaginary = i;
    }
    
    /**
     * Return the real part of this complex number.
     * 
     * @return real value
     */
    public double getReal() { 
    	return real; 
    }
    
    /**
     * Return the imaginary part of this complex number.
     * 
     * @return imaginary value
     */
    public double getImaginary() { 
    	return imaginary; 
    }
    
    /**
     * Add a complex to this value
     * 
     * @param b right hand operand of addition
     * @return new complex with the result
     */
    public Complex add(final Complex b) {
        return add(this, b);
    }
    
    /**
     * Add two complex numbers and return the result.
     * 
     * @param a left hand operand of addition
     * @param b right hand operand of addition
     * @return new complex with the result
     */
    public static Complex add(final Complex a, final Complex b) {
        return new Complex(a.real + b.real, a.imaginary + b.imaginary);
    }
    
    /**
     * Subtract a complex from this value
     * 
     * @param b right hand operand of subtraction
     * @return new complex with the result
     */
    public Complex subtract(final Complex b) {
    	return subtract(this, b);
    }
    
    /**
     * Subtract two complex numbers and return the result.
     * 
     * @param a left hand operand of subtraction
     * @param b right hand operand of subtraction
     * @return new complex with the result
     */
    public static Complex subtract(final Complex a, final Complex b) {
        return new Complex(a.real - b.real, a.imaginary - b.imaginary);
    }
    
    /**
     * Multiply this complex with another complex
     * 
     * @param b right hand operand of multiplication
     * @return new complex with the result
     */
    public Complex multiply(final Complex b) {
       return multiply(this, b);
    }
    
    /**
     * Multiply two complex numbers
     * 
     * @param a left hand operand of multiplication
     * @param b right hand operand of multiplication
     * @return new complex with the result
     */
    public Complex multiply(final Complex a, final Complex b) {
        double real = a.real * b.real - a.imaginary * b.imaginary;
        double imag = a.real * b.imaginary + a.imaginary * b.real;
        return new Complex(real, imag);
    }
    
    /**
     * Scale this complex
     * 
     * @param alpha scaler value
     * @return new complex with the result
     */
    public Complex multiply(final double alpha) {
        return multiply(this, alpha);
    }
    
    /**
     * Scale a complex
     * 
     * @param a complex value to scale
     * @param alpha scaler value
     * @return new complex with the result
     */
    public static Complex multiply(Complex a, final double alpha) {
        return new Complex(alpha * a.real, alpha * a.imaginary);
    }
    
    /**
     * Divide this complex by complex b
     * 
     * @param b denominator of operation
     * @return this / b
     */
    public Complex divideBy(final Complex b) {
    	return divide(this, b);
    }
    
    /**
     * Divide two complex numbers
     * 
     * @param a numerator of operation
     * @param b denominator of operation
     * @return a / b
     */
    public static Complex divide(final Complex a, final Complex b) {
    	return a.multiply(b.reciprocal());
    }
    
    /**
     * Calculate the conjugate.
     * 
     * @return a new Complex object whose value is the conjugate of this
     */
    public Complex conjugate() {  
    	return new Complex(real, -imaginary); 
    }

    /**
     * Calculate the reciprocal.
     * 
     * @return a new Complex object whose value is the reciprocal of this value
     */
    public Complex reciprocal() {
        double scale = real * real + imaginary * imaginary;
        return new Complex(real / scale, -imaginary / scale);
    }
    
    /**
     * Calculate phase.
     * 
     * @return phase value of complex
     */
    public double phase() { 
    	return Math.atan2(imaginary, real); 
    }
    
    /**
     * Calculate absolute value.
     * 
     * @return absolute value of complex
     */
    public double abs()   { 
    	return Math.hypot(real, imaginary); 
    }
    
    /**
     * Calculate complex exponential.
     * 
     * @return a new Complex object whose value is the complex exponential of this
     */
    public Complex exp() {
        return new Complex(Math.exp(real) * Math.cos(imaginary), Math.exp(real) * Math.sin(imaginary));
    }

    /**
     * Calculate complex sine.
     * 
     * @return a new Complex object whose value is the complex sine of this
     */
    public Complex sin() {
        return new Complex(Math.sin(real) * Math.cosh(imaginary), Math.cos(real) * Math.sinh(imaginary));
    }

    /**
     * Calculate complex cosine.
     * 
     * @return a new Complex object whose value is the complex cosine of this
     */
    public Complex cos() {
        return new Complex(Math.cos(real) * Math.cosh(imaginary), -Math.sin(real) * Math.sinh(imaginary));
    }
 
    /**
     * Calculate complex tangent.
     * 
     * @return a new Complex object whose value is the complex tangent of this complex
     */
    public Complex tan() {
        return sin().divideBy(cos());
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(imaginary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(real);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Complex other = (Complex) obj;
		if (Double.doubleToLongBits(imaginary) != Double.doubleToLongBits(other.imaginary))
			return false;
		if (Double.doubleToLongBits(real) != Double.doubleToLongBits(other.real))
			return false;
		return true;
	}
	
	@Override
    public String toString() {
    	if (imaginary == 0) {
        	return real + "";
        } else if (real == 0) {
        	return imaginary + "i";
        } else if (imaginary <  0) {
        	return real + " - " + (-imaginary) + "i";
        } else {
        	return real + " + " + imaginary + "i";
        }
    }
}
