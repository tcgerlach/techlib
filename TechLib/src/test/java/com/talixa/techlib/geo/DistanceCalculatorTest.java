package com.talixa.techlib.geo;

import org.junit.Test;

import com.talixa.techlib.geo.data.Cities;

import junit.framework.Assert;

public class DistanceCalculatorTest {

	@Test
	public void testDistance() {
		double distance = DistanceCalculator.distance(Cities.AltoonaPA,  Cities.NewYorkCity);
		
		// Altoona to NYC is 371 KM
		Assert.assertEquals(371, Math.floor(distance / 1000), 1);
	}
}
