package com.talixa.techlib.conversion;

import org.junit.Test;

import junit.framework.Assert;

public class ConvertTest {

	@Test 
	public void testFeetToMeters() {
		Assert.assertEquals(.3048, Convert.feetToMeters(1), .01);
	}
	
	@Test 
	public void testMetersToFeet() {
		Assert.assertEquals(1, Convert.metersToFeet(.3048), .01);
	}
}
