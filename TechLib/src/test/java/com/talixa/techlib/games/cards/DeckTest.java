package com.talixa.techlib.games.cards;

import org.junit.Test;

import junit.framework.Assert;

public class DeckTest {

	@Test
	public void testDeck() {
		Deck d = new Deck();
		
		// verify start number of cards
		Assert.assertEquals(52, d.cardsLeft());
		
		// remove card and verify number cards left
		Card c = d.getTopCard();
		Assert.assertEquals(51, d.cardsLeft());
		
		// check that each card is in range
		while (d.cardsLeft() > 0) {
			c = d.getTopCard();
			Assert.assertTrue(c.getIntValue() > 0 && c.getIntValue() <= 13);
		}  
	}
}
