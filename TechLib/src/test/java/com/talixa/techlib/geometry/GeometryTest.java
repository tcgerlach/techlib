package com.talixa.techlib.geometry;

import org.junit.Test;

import com.talixa.techlib.shared.Vector2D;

import junit.framework.Assert;

public class GeometryTest {

	private static final double TOLERANCE = .001;
	private static final float DEG_0 = 0;
	private static final float DEG_90 = (float)(Math.PI/2);
	private static final float DEG_180 = (float)Math.PI;
	private static final float DEG_270 = DEG_90+DEG_180;
	
	@Test
	public void testGetPointX() {
		Assert.assertEquals(1, Geometry.getXPoint(DEG_0, 1, 0), TOLERANCE);
		Assert.assertEquals(0, Geometry.getXPoint(DEG_90, 1, 0), TOLERANCE);
		Assert.assertEquals(-1, Geometry.getXPoint(DEG_180, 1, 0), TOLERANCE);
		Assert.assertEquals(0, Geometry.getXPoint(DEG_270, 1, 0), TOLERANCE);
	}
	
	@Test
	public void testGetPointY() {
		Assert.assertEquals(0, Geometry.getYPoint(DEG_0, 1, 0), TOLERANCE);
		Assert.assertEquals(1, Geometry.getYPoint(DEG_90, 1, 0), TOLERANCE);
		Assert.assertEquals(0, Geometry.getYPoint(DEG_180, 1, 0), TOLERANCE);
		Assert.assertEquals(-1, Geometry.getYPoint(DEG_270, 1, 0), TOLERANCE);
	}
	
	@Test 
	public void testMidPoint() {
		Assert.assertEquals(2, Geometry.findMidPoint(1, 3), TOLERANCE);
		Assert.assertEquals(-2, Geometry.findMidPoint(-1, -3), TOLERANCE);
	}
	
	@Test
	public void testPolygonAngle() {
		// test angles to each point of square - starts at 0 degrees and goes counterclockwise
		Assert.assertEquals(DEG_0, Geometry.getAngleOfPointInPolygon(4, 0), TOLERANCE);
		Assert.assertEquals(DEG_90, Geometry.getAngleOfPointInPolygon(4, 1), TOLERANCE);
		Assert.assertEquals(DEG_180, Geometry.getAngleOfPointInPolygon(4, 2), TOLERANCE);
		Assert.assertEquals(DEG_270, Geometry.getAngleOfPointInPolygon(4, 3), TOLERANCE);
	}
	
	@Test
	public void testPolygonPoint() {
		// square, first point
		Vector2D a = Geometry.getPolygonPoint(4, 0, 1, new Vector2D(0,0));
		Assert.assertEquals(1, a.getX(), TOLERANCE); 
		Assert.assertEquals(0, a.getY(), TOLERANCE); 
		
		// second point
		Vector2D b = Geometry.getPolygonPoint(4, 1, 1, new Vector2D(0,0));
		Assert.assertEquals(0, b.getX(), TOLERANCE); 
		Assert.assertEquals(1, b.getY(), TOLERANCE); 
		
		// third point
		Vector2D c = Geometry.getPolygonPoint(4, 2, 1, new Vector2D(0,0));
		Assert.assertEquals(-1, c.getX(), TOLERANCE); 
		Assert.assertEquals(0, c.getY(), TOLERANCE); 
		
		// fourth point
		Vector2D d = Geometry.getPolygonPoint(4, 3, 1, new Vector2D(0,0));
		Assert.assertEquals(0, d.getX(), TOLERANCE); 
		Assert.assertEquals(-1, d.getY(), TOLERANCE); 
	}
}
