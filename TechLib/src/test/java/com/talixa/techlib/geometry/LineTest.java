package com.talixa.techlib.geometry;

import org.junit.Test;

import com.talixa.techlib.shared.Vector2D;

import junit.framework.Assert;

public class LineTest {
	private static final double TOLERANCE = .001;
	private static final Vector2D V1 = new Vector2D(0,0);
	private static final Vector2D V2 = new Vector2D(2,2);
	private static final Vector2D V3 = new Vector2D(3,4);
	private static final Vector2D V4 = new Vector2D(0,2);
	private static final Vector2D V5 = new Vector2D(2,0);	
	
	@Test 
	public void testMidPoint() {
		Vector2D mid = Line.findMidPoint(V1, V2);
		Assert.assertEquals(1, mid.getX(), TOLERANCE);
		Assert.assertEquals(1, mid.getY(), TOLERANCE);
	}
	
	@Test
	public void testDistance() {
		double distance = Line.getDistance(V1,V3);
		Assert.assertEquals(5, distance, TOLERANCE);
	}
	
	@Test
	public void testIntersection() {
		Vector2D intersection = Line.intersection(V1, V2, V4, V5);
		Assert.assertEquals(1, intersection.getX(), TOLERANCE);
		Assert.assertEquals(1, intersection.getY(), TOLERANCE);
	}
}
