package com.talixa.techlib.encoding;

import java.nio.charset.Charset;

import org.junit.Test;

import junit.framework.Assert;

public class Base64EncoderTest {

	private static final String RAW = "THE QUICK BROWN FOX";
	private static final String ENCODED = "VEhFIFFVSUNLIEJST1dOIEZPWA==";
	
	@Test
	public void testEncode() {
		String encoded = Base64Encoder.encodeBase64(RAW.getBytes());
		Assert.assertEquals(ENCODED, encoded);
	}
	
	@Test
	public void testDecode() {
		byte[] decoded = Base64Encoder.decodeBase64(ENCODED);
		Assert.assertEquals(RAW, new String(decoded, Charset.defaultCharset()));
	}
}
