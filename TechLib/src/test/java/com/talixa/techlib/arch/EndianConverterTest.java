package com.talixa.techlib.arch;

import org.junit.Test;

import junit.framework.Assert;

public class EndianConverterTest {
	
	private static final byte[] TEST_SHORT_BYTES = new byte[] {(byte)0xff, (byte)0x00};
	private static final int TEST_SHORT = 0x00ff;
	private static final byte[] TEST_INT_BYTES = new byte[] {(byte)0xff, (byte)0xee, (byte)0xcc, (byte)0x00};
	private static final int TEST_INT = 0x00cceeff;

	@Test
	public void testLittleEndianShortToJavaInt() {
		int value = EndianConverter.littleEndianShortToJavaInt(TEST_SHORT_BYTES[1], TEST_SHORT_BYTES[0]);
		Assert.assertEquals(TEST_SHORT, value);
		
		value = EndianConverter.littleEndianShortToJavaInt(TEST_SHORT_BYTES);
		Assert.assertEquals(TEST_SHORT, value);
	}
	
	@Test
	public void testLittleEndianIntToJavaLong() {
		long value = EndianConverter.littleEndianIntToJavaLong(TEST_INT_BYTES[3], TEST_INT_BYTES[2], TEST_INT_BYTES[1], TEST_INT_BYTES[0]);
		Assert.assertEquals(TEST_INT, value);
		
		value = EndianConverter.littleEndianIntToJavaLong(TEST_INT_BYTES);
		Assert.assertEquals(TEST_INT, value);
	}
	
	@Test(expected=RuntimeException.class) 
	public void testWrongShortSize() {
		// sending 4 bytes instead of 2 - should throw exception
		EndianConverter.littleEndianShortToJavaInt(TEST_INT_BYTES);
	}
	
	@Test(expected=RuntimeException.class) 
	public void testWrongIntSize() {
		// sending 2 bytes instead of 4 - should throw exception
		EndianConverter.littleEndianIntToJavaLong(TEST_SHORT_BYTES);
	}
	
	@Test
	public void testJavaIntToBigEndianBytes() {
		byte[] bytes = EndianConverter.javaIntToBigEndianBytes(TEST_INT);
		Assert.assertEquals(4, bytes.length);
		for(int i = 0; i < 4; ++i) {
			Assert.assertEquals(TEST_INT_BYTES[i], bytes[i]);
		}
	}
	
	@Test
	public void testJavaShortToBigEndianBytes() {
		byte[] bytes = EndianConverter.javaShortToBigEndianBytes((short)TEST_SHORT);
		Assert.assertEquals(2, bytes.length);
		for(int i = 0; i < 2; ++i) {
			Assert.assertEquals(TEST_SHORT_BYTES[i], bytes[i]);
		}
	}
}
