package com.talixa.techlib.math.functions;

import com.talixa.techlib.math.interfaces.Function;

public class SquareRoot implements Function {

	@Override
	public double execute(final double[] input) {
		return Math.sqrt(input[0]);
	}
}
