package com.talixa.techlib.math;

import org.junit.Test;

import junit.framework.Assert;

public class MathematicsTest {

	@Test
	public void testFactorCount() {
		Assert.assertEquals(1, Mathematics.factorCount(1));
		Assert.assertEquals(3, Mathematics.factorCount(4));
		Assert.assertEquals(4, Mathematics.factorCount(10));
		Assert.assertEquals(12, Mathematics.factorCount(200));
	}
	
	@Test
	public void testFactors() {
		int[] factors = Mathematics.getFactors(200);
		Assert.assertEquals(12, factors.length);
		Assert.assertEquals(1, factors[0]);
		Assert.assertEquals(2, factors[1]);
		Assert.assertEquals(4, factors[2]);
		Assert.assertEquals(5, factors[3]);
		Assert.assertEquals(8, factors[4]);
		Assert.assertEquals(10, factors[5]);
		Assert.assertEquals(20, factors[6]);
		Assert.assertEquals(25, factors[7]);
		Assert.assertEquals(40, factors[8]);
		Assert.assertEquals(50, factors[9]);
		Assert.assertEquals(100, factors[10]);
		Assert.assertEquals(200, factors[11]);
	}
}
