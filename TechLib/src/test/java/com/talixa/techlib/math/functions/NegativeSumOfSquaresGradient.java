package com.talixa.techlib.math.functions;

import com.talixa.techlib.math.interfaces.Function;

public class NegativeSumOfSquaresGradient implements Function {
	@Override
	public double execute(final double[] input) {
		float sosg = 0;
		for (int i = 0; i < input.length; ++i) {
			sosg += input[i] * 2;
		}
		return sosg * -1;
	}
}
