package com.talixa.techlib.math;

import junit.framework.Assert;
import org.junit.Test;

public class VectorTest {

	private static final double[] DATA = new double[] {1,2,3,4,5};
	private static final double TOLERANCE = .01;

	@Test
	public void testAddVectors() {
		double[] result = Vector.addVectors(DATA,DATA);
		Assert.assertEquals(2,result[0],TOLERANCE);
		Assert.assertEquals(10,result[4],TOLERANCE);
	}

	@Test
	public void testSubtractVector() {
		double[] result = Vector.subtractVectors(DATA,DATA);
		Assert.assertEquals(0,result[0],TOLERANCE);
		Assert.assertEquals(0,result[4],TOLERANCE);
	}

	@Test
	public void testScaleVector() {
		double[] result = Vector.scaleVector(DATA,5);
		Assert.assertEquals(5,result[0],TOLERANCE);
		Assert.assertEquals(25,result[4],TOLERANCE);
	}

	@Test
	public void testSumOfSquares() {
		double soq = Vector.sumOfSquares(DATA);
		Assert.assertEquals(55,soq,TOLERANCE);
	}

	@Test
	public void testDotProduct() {
		double p = Vector.dotProduct(DATA,DATA);
		Assert.assertEquals(55,p,TOLERANCE);
	}
	
	@Test 
	public void testSumVector() {
		double s = Vector.sumVector(DATA);
		Assert.assertEquals(15, s, TOLERANCE);
	}
}

