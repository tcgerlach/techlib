package com.talixa.techlib.math.functions;

import com.talixa.techlib.math.interfaces.Function;

public class SumOfSquares implements Function {

	@Override
	public double execute(final double[] input) {
		float sos = 0;
		for (int i = 0; i < input.length; ++i) {
			sos += input[i] * input[i];
		}
		return sos;
	}
}
