package com.talixa.techlib.math;

import org.junit.Test;

import com.talixa.techlib.math.functions.Cube;
import com.talixa.techlib.math.functions.Square;
import com.talixa.techlib.math.functions.SquareRoot;
import com.talixa.techlib.math.interfaces.Function;

import junit.framework.Assert;

public class FunctionCheckerTest {
	
	private static Function square = new Square();
	private static Function cube = new Cube();
	private static Function root = new SquareRoot();

	@Test
	public void testEven() {
		Assert.assertTrue(FunctionChecker.isEven(square));
		Assert.assertFalse(FunctionChecker.isOdd(square));
	}
	
	@Test
	public void testOdd() {
		Assert.assertTrue(FunctionChecker.isOdd(cube));
		Assert.assertFalse(FunctionChecker.isEven(cube));
	}
	
	@Test
	public void testNeitherOddNorEven() {
		Assert.assertFalse(FunctionChecker.isOdd(root));
		Assert.assertFalse(FunctionChecker.isEven(root));
	}
	
	@Test
	public void testSymmetryOnX() {
		Assert.assertTrue(FunctionChecker.hasSymmetryOnX(square));
		Assert.assertFalse(FunctionChecker.hasSymmetryOnX(cube));
	}
	
	@Test
	public void testSymmetryOnOrigin() {
		Assert.assertTrue(FunctionChecker.hasSymmetryOnOrigin(cube));
		Assert.assertFalse(FunctionChecker.hasSymmetryOnOrigin(square));
	}
}
