package com.talixa.techlib.math.functions;

import com.talixa.techlib.math.interfaces.Function;

public class Square implements Function {

	@Override
	public double execute(final double[] input) {
		return input[0] * input[0];
	}
}
