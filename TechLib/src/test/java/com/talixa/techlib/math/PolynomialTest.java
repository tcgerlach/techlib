package com.talixa.techlib.math;

import org.junit.Test;

import junit.framework.Assert;

public class PolynomialTest {

	private static final float TOLERANCE = .1f;
	
	@Test
	public void testPolynomialCalc() {
		Assert.assertEquals(0, Polynomial.calculate(1, new float[] {0,0,0}), TOLERANCE);
		Assert.assertEquals(4, Polynomial.calculate(2, new float[] {2,0}), TOLERANCE);
		Assert.assertEquals(9, Polynomial.calculate(2, new float[] {2,5}), TOLERANCE);
		Assert.assertEquals(45, Polynomial.calculate(4, new float[] {2,3,1}), TOLERANCE);
	}
}
