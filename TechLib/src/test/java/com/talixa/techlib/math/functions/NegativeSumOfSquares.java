package com.talixa.techlib.math.functions;

import com.talixa.techlib.math.interfaces.Function;

public class NegativeSumOfSquares implements Function {

	@Override
	public double execute(final double[] input) {
		float sos = 0;
		for (int i = 0; i < input.length; ++i) {
			sos += input[i] * input[i];
		}
		return sos * -1;
	}
}
