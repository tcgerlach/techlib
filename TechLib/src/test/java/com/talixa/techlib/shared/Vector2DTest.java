package com.talixa.techlib.shared;

import org.junit.Test;

import junit.framework.Assert;

public class Vector2DTest {
	
	private static final float TOLERANCE = .0001f;
	private static final Vector2D POINT_11 = new Vector2D(1,1);
	private static final Vector2D ORIGIN = new Vector2D(0,0); 
	
	private static final float FROM_00_TO_11 = 1.4142135f;
	private static final float NORMALIZED_11 = .707106769f;
	
	@Test 
	public void testGetAngle() {
		Assert.assertEquals(Math.PI/4, POINT_11.getAngle(), TOLERANCE);
	}
	
	@Test 
	public void testMagnitude() {
		Assert.assertEquals(FROM_00_TO_11, POINT_11.getMagnitude(), TOLERANCE);
	}
	
	@Test
	public void testDistance() {
		Assert.assertEquals(FROM_00_TO_11,  ORIGIN.getDistance(POINT_11));
	}
	
	@Test
	public void testScale() {
		Vector2D test = new Vector2D(1,1);
		test.scale(5);
		Assert.assertEquals(5, test.getX(), TOLERANCE);
		Assert.assertEquals(5, test.getY(), TOLERANCE);
	}
	
	@Test 
	public void testNormalize() {
		Vector2D test = new Vector2D(1,1);
		test.normalize();
		Assert.assertEquals(NORMALIZED_11, test.getX(), TOLERANCE);
		Assert.assertEquals(NORMALIZED_11, test.getY(), TOLERANCE);
	}
	
	@Test
	public void testDotProduct() {
		Vector2D v1 = new Vector2D(5,4);
		Vector2D v2 = new Vector2D(3,9);
		Assert.assertEquals(51, v1.dotProduct(v2), TOLERANCE);
	}	
}
