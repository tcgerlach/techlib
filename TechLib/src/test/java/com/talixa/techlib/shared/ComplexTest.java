package com.talixa.techlib.shared;

import org.junit.Test;

import junit.framework.Assert;

public class ComplexTest {
	
	// TODO finish complex tests
	
	private static final float TOLERANCE = .1f;

	@Test
	public void testAddition() {
		// test addition - static version
		Complex a = new Complex(1,2);
		Complex b = new Complex(4,8);
		Complex c = Complex.add(a, b);
		Assert.assertEquals(5, c.getReal(), TOLERANCE);
		Assert.assertEquals(10, c.getImaginary(), TOLERANCE);
		
		// test addition to existing complex
		Complex d = a.add(b);
		Assert.assertEquals(5, d.getReal(), TOLERANCE);
		Assert.assertEquals(10, d.getImaginary(), TOLERANCE);
	}
	
	@Test
	public void testSubtraction() {
		// test addition
		Complex a = new Complex(1,2);
		Complex b = new Complex(4,8);
		Complex c = Complex.subtract(a, b);
		Assert.assertEquals(-3, c.getReal(), TOLERANCE);
		Assert.assertEquals(-6, c.getImaginary(), TOLERANCE);
		
		// test addition to existing complex
		Complex d = a.subtract(b);
		Assert.assertEquals(-3, d.getReal(), TOLERANCE);
		Assert.assertEquals(-6, d.getImaginary(), TOLERANCE);
	}
	
	@Test
	public void testScalarMultiply() {
		Complex a = new Complex(5,10);
		Complex b = a.multiply(5);
		Assert.assertEquals(25, b.getReal(), TOLERANCE);
		Assert.assertEquals(50, b.getImaginary(), TOLERANCE);
	}
}
