package com.talixa.techlib.shared;

import org.junit.Test;

import junit.framework.Assert;

public class Vector3DTest {

	private static final float TOLERANCE = .0001f;
	
	private static final Vector3D POINT_100 = new Vector3D(1,0,0);
	private static final Vector3D POINT_010 = new Vector3D(0,1,0);
	private static final Vector3D POINT_001 = new Vector3D(0,0,1);
	private static final Vector3D ORIGIN = new Vector3D(0,0,0); 
	
	@Test 
	public void testMagnitude() {
		Assert.assertEquals(1, POINT_100.getMagnitude(), TOLERANCE);
		Assert.assertEquals(1, POINT_010.getMagnitude(), TOLERANCE);
		Assert.assertEquals(1, POINT_001.getMagnitude(), TOLERANCE);
	}
	
	@Test
	public void testScale() {
		Vector3D v = new Vector3D(3,2,1);
		v.scale(5);
		
		Assert.assertEquals(15, v.getX(), TOLERANCE);
		Assert.assertEquals(10, v.getY(), TOLERANCE);
		Assert.assertEquals(5, v.getZ(), TOLERANCE);
	}
	
	@Test
	public void testNormalize() {
		Vector3D v = new Vector3D(5,3,1);
		v.normalize();
		
		Assert.assertEquals(1, v.getMagnitude(), TOLERANCE);
	}
	
	@Test
	public void testDistance() {
		Assert.assertEquals(1, POINT_001.getDistance(ORIGIN), TOLERANCE);
		Assert.assertEquals(1, POINT_010.getDistance(ORIGIN), TOLERANCE);
		Assert.assertEquals(1, POINT_100.getDistance(ORIGIN), TOLERANCE);
	}
	
	@Test
	public void testAdd() {
		Vector3D p011 = Vector3D.addVectors(POINT_001, POINT_010);
		Assert.assertEquals(0, p011.getX(), TOLERANCE);
		Assert.assertEquals(1, p011.getY(), TOLERANCE);
		Assert.assertEquals(1, p011.getZ(), TOLERANCE);
	}
	
	@Test
	public void testSubtract() {
		Vector3D p = Vector3D.subtractVectors(POINT_001, POINT_010);
		Assert.assertEquals(0, p.getX(), TOLERANCE);
		Assert.assertEquals(-1, p.getY(), TOLERANCE);
		Assert.assertEquals(1, p.getZ(), TOLERANCE);
	}
}
