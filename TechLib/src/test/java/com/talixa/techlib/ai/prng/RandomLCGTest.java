package com.talixa.techlib.ai.prng;

import org.junit.Assert;
import org.junit.Test;

public class RandomLCGTest {
	
	// 50 million iterations is good
	private static final int MAX_ITERATIONS = 50000000;

	@Test 
	public void testNextInt() {	
		long  m = RandomLCG.getMaxValue();
		
		int first = RandomLCG.getNextInt();
		
		for(int i = 0; i < MAX_ITERATIONS; ++i) {
			int rand = RandomLCG.getNextInt();
			
			// should be less than max number & non-negative
			Assert.assertTrue(rand < m);
			Assert.assertTrue(rand >= 0);
			
			// make sure last/next has not happened in the past
			if (rand == first) {
				Assert.fail("Repeating after " + i);
			}
		}
	}
	
	@Test
	public void testNextIntMax() {
		for(int i = 1; i < MAX_ITERATIONS; ++i) {
			int rand = RandomLCG.getNextInt(i);
			Assert.assertTrue(rand <= i);
		}
	}
	
	@Test
	public void testFloat() {
		for(int i = 0; i < MAX_ITERATIONS; ++i) {
			float f = RandomLCG.getNextFloat();
			Assert.assertTrue(f <= 1);
			Assert.assertTrue(f >= 0);
		}
	}
	
	@Test
	public void testFloatMax() {
		boolean foundAbove49 = false;
		for(int i = 0; i < MAX_ITERATIONS; ++i) {
			float f = RandomLCG.getNextFloat(50);
			Assert.assertTrue(f <= 50);
			Assert.assertTrue(f >= 0);
			if (f > 49) {
				foundAbove49 = true;
			}
		}
		
		// make sure we found one near the max
		Assert.assertTrue(foundAbove49);
	}
}
