package com.talixa.techlib.ai.genetic.programming;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.talixa.techlib.ai.prng.RandomLCG;

public class TreeBuilderTest {

	private static final int TREE_DEPTH = 3;
	private static final int MEMORY_SIZE = 5;
	private static final int RUN_COUNT = 1000;
	
	// As the tree depth grows, so does the chance of failure
	private static final float MAX_FULL_FAIL_PERCENT = .25f;	// full has a high likelihood of bad functions
	private static final float MAX_GROW_FAIL_PERCENT = .10f;	// grow is far less likely to produce bad functions
	
	private static float[] memory;
	
	@BeforeClass
	public static void setup() {
		// seed LCG
		RandomLCG.seed(System.currentTimeMillis());
		
		// generate random memory
		memory = new float[MEMORY_SIZE];
		for(int i = 0; i < MEMORY_SIZE; ++i) {
			memory[i] = RandomLCG.getNextFloat();
		}
	}
	
	@Test
	public void testFullInit() {
		System.out.println("Testing Full Tree Initialization");
		runTest(true, MAX_FULL_FAIL_PERCENT);
	}
	
	@Test 
	public void testGrowInit() {
		System.out.println("Testing Grow Tree Initialization");
		runTest(false, MAX_GROW_FAIL_PERCENT);
	}
	
	public void runTest(boolean fullTree, float maxFailPercent) {
		int failCount = 0;
		
		// run numerous times
		for(int i = 0; i < RUN_COUNT; ++i) {
			// init tree
			TreeNode root ;
			if (fullTree) {
				root = TreeBuilder.fullTreeInitalization(TREE_DEPTH, MEMORY_SIZE);
			} else {
				root = TreeBuilder.growTreeInitialization(TREE_DEPTH, MEMORY_SIZE);
			}
			
			// evaluate
			float value = Tree.evaluate(root, memory);
			
			// invalid values will happen from divide by zero, sqrt of negative, etc
			if (Float.isInfinite(value) || Float.isNaN(value)) {
				++failCount;
			}
		}
		
		// check failure count
		float failPercent = (float)(failCount)/(float)(RUN_COUNT);
		if (failPercent > maxFailPercent) {
			Assert.fail("Unacceptably high failure count for tree evaluation: " + (failPercent*100) + "%");
		} else {
			System.out.println("Failure rate: " + (failPercent*100) + "%");
		}
	}
}
