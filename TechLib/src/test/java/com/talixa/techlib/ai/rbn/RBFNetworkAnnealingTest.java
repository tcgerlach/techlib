package com.talixa.techlib.ai.rbn;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

import com.talixa.techlib.ai.general.Errors;

import junit.framework.Assert;

public class RBFNetworkAnnealingTest {
	
	private static Random r = new Random();
	
	// network inputs/outputs
	private static final float[][] INPUTS = new float[][] {{0,0}, {1,0}, {0,1}, {1,1}};
	private static final float[] XOR_OUT = new float[] {0,1,1,0};
	private static final float[] AND_OUT = new float[] {0,0,0,1};
	private static final float[] OR_OUT  = new float[] {0,1,1,1};
	
	private static final float TOLERANCE = .1f;
	
	// annealing data
	private int k = 0;
	private int kMax = 512;
	private float startingTemperature = 400;
	private float endingTemperature = 0.0001f;
	private float lastChance = 0;
	
	// network
	private RBFNetwork n;
	private float[] bestMemory;
	
	// best score achieved by algorithm - this iteration and ever
	private float iterationBestScore;
	private float globalBestScore;
	
	public void resetState() {
		k = 0; 
		iterationBestScore = Float.MAX_VALUE;
		globalBestScore = Float.MAX_VALUE;
		lastChance = 0;
		n = new RBFNetwork(2, 5, 1);
	}
	
	@Test
	public void testCalculateXorNetwork() {
		testCalculateNetwork("SA XOR", INPUTS, XOR_OUT);
	}
	
	@Test
	public void testCalculateAndNetwork() {
		testCalculateNetwork("SA AND", INPUTS, AND_OUT);
	}
	
	@Test
	public void testCalculateOrNetwork() {
		testCalculateNetwork("SA OR", INPUTS, OR_OUT);
	}
	
	private static final int MAX_ATTEMPTS = 20;
	private static final float MIN_ACCEPTABLE = .01f;
	
	private void testCalculateNetwork(String name, float[][] input, float[] expectedOutput) {	

		// simulated annealing is fast, but sometimes has a bad start.
		// lets run a max of i times or until error is at an acceptable level
		boolean done = false;
		for (int i = 0; i < MAX_ATTEMPTS && !done; ++i) {
			// reset all variables
			resetState();
			
			// having a zero in the starting memory is bad - start with something random
			n.randomizeMemory();
			
			while (k < kMax) {
				iteration(n, 100, input, expectedOutput);
			}
			
			// if the score is good enough, exit
			if (globalBestScore < MIN_ACCEPTABLE) {
				done = true;
			}
		}
		
		n.setMemory(bestMemory);
		// apply best values to data and verify success
		System.out.println(name + " Network");
		System.out.println("Score: " + globalBestScore);
		for(int i = 0; i < 4; ++i) {
			float computed = n.computeNetwork(input[i])[0];
			System.out.println("[" + input[i][0] + ", " + input[i][1] + "] -> " + computed + ", Ideal: " + expectedOutput[i]);
			Assert.assertEquals(expectedOutput[i], Math.round(computed), TOLERANCE);
		}	
	}
	
	private void iteration(RBFNetwork n, int cycles, float[][] input, float[] expectedOutput) {
		int length = n.getMemory().length;
		++k;
		float currentTemperature = coolingSchedule();
		
		float[] backup;
		
		for(int i = 0; i < cycles; ++i) {
			backup = Arrays.copyOf(n.getMemory(), length);
			
			// randomize memory
            performRandomize(n.getMemory());

            // did we improve it?
            float trialScore = calculateScore(n, input, expectedOutput);
           
            // was this iteration an improvement?  If so, always keep.
            boolean keep = false;

            if (trialScore < iterationBestScore) {
                keep = true;
            } else {
            	// random chance to keep anyway
                this.lastChance = calcProbability(iterationBestScore, trialScore, currentTemperature);
                if (this.lastChance > r.nextDouble()) {
                    keep = true;
                }
            }

            if (keep) {
            	iterationBestScore = trialScore;
                // better than global error
                if (trialScore < globalBestScore) {
                    globalBestScore = trialScore;
                    bestMemory = Arrays.copyOf(n.getMemory(), length);
                }
            } else {
            	// backout changes
            	n.setMemory(backup);
            }
		}
	}
	
	public float calcProbability(float ecurrent, float enew, float t) {
		return (float)Math.exp(-(Math.abs(enew - ecurrent) / t));
	}
	 
	public void performRandomize(final float[] memory) {
		for (int i = 0; i < memory.length; i++) {
			memory[i] += (r.nextGaussian() / 10);
		}
	}
	
	public float coolingSchedule() {
		float ex = (float)k / (float)kMax;
		return startingTemperature * (float)Math.pow(endingTemperature / startingTemperature, ex);
    }
	
	// vector to hold temp output values
	float[] outputVector = new float[4];
	
	private float calculateScore(RBFNetwork n, float[][] input, float[] expectedOutput) {
		for(int i = 0; i < outputVector.length; ++i) {
			outputVector[i] = n.computeNetwork(input[i])[0];
		}
		
		return Errors.sumOfSquares(expectedOutput, outputVector);
	}
}
