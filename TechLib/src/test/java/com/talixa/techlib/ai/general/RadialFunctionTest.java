package com.talixa.techlib.ai.general;

import org.junit.Test;

import junit.framework.Assert;

public class RadialFunctionTest {
	
	private static final double TOLERANCE = .0001;
	
	@Test
	public void testGaussian() {
		float[] expected = new float[] {.0183156f, .367879f, 1f, .367879f, .0183156f};
		float[] input = new float[] {-2,-1,0,1,2};
		for(int i = 0; i < input.length; ++i) {
			float rbf = RadialFunctions.gaussian(input[i]);
			Assert.assertEquals(expected[i], rbf, TOLERANCE);
		}
	}
	
	@Test
	public void testRickerWavelet() {
		float[] expected = new float[] {-.406005f, 0, 1, 0, -.406005f};
		float[] input = new float[] {-2,-1,0,1,2};
		for(int i = 0; i < input.length; ++i) {
			float rbf = RadialFunctions.rickerWavelet(input[i]);
			Assert.assertEquals(expected[i], rbf, TOLERANCE);
		}
	}
}
