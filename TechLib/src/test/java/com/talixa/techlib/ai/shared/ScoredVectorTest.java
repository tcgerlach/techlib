package com.talixa.techlib.ai.shared;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class ScoredVectorTest {
	
	public static final float TOLERANCE = .01f;
	
	private ScoredFloatVector v1;
	private ScoredFloatVector v2;
	private List<ScoredVector> pop;
	
	@Before
	public void setup() {
		v1 = new ScoredFloatVector(new float[]{.1f,.2f,.3f}, .2f);
		v2 = new ScoredFloatVector(new float[]{1f,2f,3f}, 2);
		
		pop = new ArrayList<ScoredVector>();
		pop.add(v1);
		pop.add(v2);
	}

	@Test
	public void testScoredVector() {
		Assert.assertEquals(.2f, v1.getScore(), TOLERANCE);
		Assert.assertEquals(3, v1.getData().length);
		Assert.assertEquals(2f, v2.getScore(), TOLERANCE);
		Assert.assertEquals(3, v2.getData().length);
	}
	
	@Test
	public void testPopulationScore() {
		float score = ScoredVector.getPopulationScore(pop);
		Assert.assertEquals(2.2f, score, TOLERANCE);
	}
	
	@Test
	public void testReverseScores() {
		Assert.assertEquals(.2f, pop.get(0).getScore(), TOLERANCE);
		Assert.assertEquals(2f, pop.get(1).getScore(), TOLERANCE);
		ScoredVector.reverseScores(pop);
		Assert.assertEquals(2, pop.get(0).getScore(), TOLERANCE);
		Assert.assertEquals(.2f, pop.get(1).getScore(), TOLERANCE);
	}
	
	
}
