package com.talixa.techlib.ai.rbn;

import java.util.Arrays;

import org.junit.Test;

import com.talixa.techlib.ai.general.Errors;

import junit.framework.Assert;

public class RBFNetworkRandomGreedyTest {
	
	// Known good estimates
	private static final float[] MEMORY = {-5.472872014891603f, -2.992056142335599f, 7.408783247770394f, -4.324322467205843f,
			-3.5281504258274605f, 3.248253072428305f, -6.3467019652535095f, -4.638839020089602f, 0.0759532408586967f,
			3.802072387607719f, 8.547660960208908f, -8.536558472119275f, 8.298858709032068f, 2.625364687329256f,
			2.1965613544294698f, 4.196841452521031f, 5.6795094259252075f, 1.435501807962778f, 2.4720949032524295f,
			9.027514935388812f, -3.273827938095617f, 6.130682001317297f, -8.426589960142799f, 5.507114634709138f,
			-4.109847573047263f, 7.621673664780978f, -9.906143093650002f, 7.97706512453496f, 4.451696151195748f,
			-5.413644562404924f, -7.630330196988124f};
	
	private static final float[][] INPUTS = new float[][] {{0,0}, {1,0}, {0,1}, {1,1}};
	
	private static final float[] XOR_OUT = new float[] {0,1,1,0};
	private static final float[] AND_OUT = new float[] {0,0,0,1};
	private static final float[] OR_OUT  = new float[] {0,1,1,1};
	
	private static final int ITERATIONS = 500000;
	private static final float TOLERANCE = .1f;
	
	@Test
	public void testGoodXorMemory() {
		RBFNetwork n = new RBFNetwork(2, 5, 1);
		
		// known good values
		n.setMemory(MEMORY);
		
		for(int i = 0; i < 4; ++i) {
			float computed = n.computeNetwork(INPUTS[i])[0];
			Assert.assertEquals(XOR_OUT[i], Math.round(computed), TOLERANCE);
		}	
	}
	
	@Test
	public void testCalculateXorNetwork() {
		testCalculateNetwork("GRT XOR", INPUTS, XOR_OUT);
	}
	
	@Test
	public void testCalculateAndNetwork() {
		testCalculateNetwork("GRT AND", INPUTS, AND_OUT);
	}
	
	@Test
	public void testCalculateOrNetwork() {
		testCalculateNetwork("GRT OR", INPUTS, OR_OUT);
	}
	
	private void testCalculateNetwork(String name, float[][] input, float[] expectedOutput) {
		float bestScore = Float.MAX_VALUE;
		float[] bestMemory = null;
		
		RBFNetwork n = new RBFNetwork(2, 5, 1);
		
		// use random greedy training to find best values
		float[] outputVector = new float[expectedOutput.length];
		for(int x = 0; x < ITERATIONS; ++x) {
			n.randomizeMemory();
			for(int i = 0; i < expectedOutput.length; ++i) {
				outputVector[i] = n.computeNetwork(input[i])[0];
			}
			
			float score = Errors.sumOfSquares(expectedOutput, outputVector);
			if (score < bestScore) {
				bestScore = score;
				bestMemory = Arrays.copyOf(n.getMemory(), n.getMemory().length);
			}
		}
		
		// apply best values to data and verify success
		System.out.println(name + " Network");
		System.out.println("Score: " + bestScore);
		n.setMemory(bestMemory);
		for(int i = 0; i < 4; ++i) {
			float computed = n.computeNetwork(input[i])[0];
			System.out.println("[" + input[i][0] + ", " + input[i][1] + "] -> " + computed + ", Ideal: " + expectedOutput[i]);
			// this is random training, round to nearest whole number as results will be crude
			Assert.assertEquals(expectedOutput[i], Math.round(computed), TOLERANCE);
		}	
	}
}
