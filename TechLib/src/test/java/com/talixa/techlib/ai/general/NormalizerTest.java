package com.talixa.techlib.ai.general;

import org.junit.Test;

import junit.framework.Assert;

public class NormalizerTest {
	
	private static final double TOLERANCE = .0001;
	
	private static final int NUM_POSITIONS = 13;
	private static final int[] DENORMALIZED_ORDINAL = {0,3,6,9,12};
	private static final float[] NORMALIZED_ORDINAL = {-1f,-.5f,0f,.5f,1f};
	
	private static final float[] DENORMALIZED_RECIPROCAL = new float[] {1,2,3,4};
	private static final float[] NORMALIZED_RECIPROCAL = new float[] {1, .5f, 1f/3f, .25f};
	
	private static final int MAX_DENORMALIZED = 9;
	private static final int MIN_DENORMALIZED = 1;
	private static final float[] DENORMALIZED = new float[] {1,3,5,7,9};
	private static final float[] NORMALIZED = new float[] {-1,-.5f,0,.5f,1};

	@Test 
	public void testNormalizeOrdinalVector() {
		float[] output = Normalizer.normalizeOrdinalVector(DENORMALIZED_ORDINAL, NUM_POSITIONS);
		for(int i = 0; i < output.length; ++i) {
			Assert.assertEquals(NORMALIZED_ORDINAL[i], output[i], TOLERANCE);
		}
	}
	
	@Test 
	public void testDenormalizeOrdinalVector() {
		int[] output = Normalizer.denormalizeOrdinalVector(NORMALIZED_ORDINAL, NUM_POSITIONS);
		for(int i = 0; i < output.length; ++i) {
			Assert.assertEquals(DENORMALIZED_ORDINAL[i], output[i], TOLERANCE);
		}
	}
	
	@Test
	public void testReciprocalNormalization() {
		float[] normalized = Normalizer.reciprocalNormalization(DENORMALIZED_RECIPROCAL);
		for(int i = 0; i < normalized.length; ++i) {
			Assert.assertEquals(NORMALIZED_RECIPROCAL[i], normalized[i], TOLERANCE);
		}
	}
	
	@Test
	public void testReciprocalDenormalization() {
		float[] denormalized = Normalizer.reciprocalNormalization(NORMALIZED_RECIPROCAL);
		for(int i = 0; i < denormalized.length; ++i) {
			Assert.assertEquals(DENORMALIZED_RECIPROCAL[i], denormalized[i], TOLERANCE);
		}
	}
	
	@Test 
	public void testNormalizeData() {
		float[] normalized = Normalizer.normalizeData(DENORMALIZED, MIN_DENORMALIZED, MAX_DENORMALIZED);
		for(int i = 0; i < normalized.length; ++i) {
			Assert.assertEquals(NORMALIZED[i], normalized[i], TOLERANCE);
		}
	}
	
	@Test 
	public void testDenormalizeData() {
		float[] denormalized = Normalizer.denormalizeData(NORMALIZED, MIN_DENORMALIZED, MAX_DENORMALIZED);
		for(int i = 0; i < denormalized.length; ++i) {
			Assert.assertEquals(DENORMALIZED[i], denormalized[i], TOLERANCE);
		}
	}
}
