package com.talixa.techlib.ai.cluster;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.talixa.techlib.ai.prng.RandomLCG;
import com.talixa.techlib.geo.DistanceCalculator;
import com.talixa.techlib.geo.data.Cities;
import com.talixa.techlib.geo.data.City;

public class KMeansTest {
	
	@Test 
	public void testKMeans() {	
		float[][] data = new float[][] {{1,1,1,1}, {2,2,2,2}, {3,3,3,3}, {40,40,40,40}, {50,50,50,50}, {60,60,60,60}};
		KMeans x = new KMeans(data, 2);
		List<Cluster> clusters = x.cluster();
		
		// have 2 clusters
		Assert.assertTrue(clusters.size() == 2);
		
		// each cluster has 3 items
		Assert.assertTrue(clusters.get(0).getObservations().size() == 3);
		Assert.assertTrue(clusters.get(1).getObservations().size() == 3);
		
		// test that one has a centroid {2,2,2,2} and the other has {50,50,50,50}
		boolean found2 = false;
		boolean found50 = false;
		for (Cluster c : clusters) {
			if (c.getCentroid().getDataPoints()[0] == 2) {
				found2 = true;
			} else if (c.getCentroid().getDataPoints()[0] == 50) {
				found50 = true;
			} 
		}
		Assert.assertTrue(found2);
		Assert.assertTrue(found50);
	}
	
	// More realistic sample of clustering North American cities
	@Test
	public void testCityCluster() {
		RandomLCG.seed(System.currentTimeMillis());
		
		City[] cityList = Cities.NorthAmerica;
		float[][] data = new float[cityList.length][];
		for(int i = 0; i < cityList.length; ++i) {
			data[i] = new float[2];
			data[i][0] = (float)cityList[i].getLatitude();
			data[i][1] = (float)cityList[i].getLongitude();
		}
		
		int numClusters = 4;
		KMeans x = new KMeans(data, numClusters);
		List<Cluster> clusters = x.cluster();
		for(int i = 0; i < numClusters; ++i) {
			// make sure we have observations
			if (clusters.get(i).getObservations().size() == 0) {
				continue;
			}
			
			Observation centroid = clusters.get(i).getCentroid();
			City center = DistanceCalculator.getNearestCity(centroid.getDataPoints()[0], centroid.getDataPoints()[1], cityList);
			System.out.println("Center: " + center.getName());
			System.out.print("Cities: ");
			for(int j = 0; j < clusters.get(i).getObservations().size() ; ++j) {
				Observation observation = clusters.get(i).getObservations().get(j);
				City observedCity = DistanceCalculator.getNearestCity(observation.getDataPoints()[0], observation.getDataPoints()[1], cityList);
				if (j > 0) {
					System.out.print(" - ");
				}
				System.out.print(observedCity.getName());
			}
			System.out.println();
		}
	}
}
