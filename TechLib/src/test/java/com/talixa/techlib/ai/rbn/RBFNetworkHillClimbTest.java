package com.talixa.techlib.ai.rbn;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.talixa.techlib.ai.general.Errors;

import junit.framework.Assert;

public class RBFNetworkHillClimbTest {
	
	// network inputs/outputs
	private static final float[][] INPUTS = new float[][] {{0,0}, {1,0}, {0,1}, {1,1}};
	private static final float[] XOR_OUT = new float[] {0,1,1,0};
	private static final float[] AND_OUT = new float[] {0,0,0,1};
	private static final float[] OR_OUT  = new float[] {0,1,1,1};
	
	private static final float TOLERANCE = .1f;
	
	// step climber params
	private static final int INITIAL_VELOCITY = 1;
	private static final int ACCELERATION = 1;

	// step climber data
	private float[] candidate = new float[5];
	private float[] stepSize;
	private float bestScore = Float.MAX_VALUE;
	
	// vector to hold temp output values
	private float[] outputVector = new float[4];
	
	// number of attempts to climb
	private int ITERATIONS = 512;
	
	// network
	private RBFNetwork n;
	
	@Before
	public void resetState() {
		n = new RBFNetwork(2, 5, 1);
		
		candidate = new float[5];
		bestScore = Float.MAX_VALUE;
		
		stepSize = new float[n.getMemory().length];
		for(int i = 0; i < stepSize.length; ++i) {
			stepSize[i] = INITIAL_VELOCITY;
		}
		
		candidate[0] = -ACCELERATION;
		candidate[1] = -1/ACCELERATION;
		candidate[2] = 0;
		candidate[3] = 1/ACCELERATION;
		candidate[4] = ACCELERATION;
	}
	
	@Test
	public void testCalculateXorNetwork() {
		testCalculateNetwork("HC XOR", INPUTS, XOR_OUT);
	}
	
	@Test
	public void testCalculateAndNetwork() {
		testCalculateNetwork("HC AND", INPUTS, AND_OUT);
	}
	
	@Test
	public void testCalculateOrNetwork() {
		testCalculateNetwork("HC OR", INPUTS, OR_OUT);
	}
	
	private void testCalculateNetwork(String name, float[][] input, float[] expectedOutput) {		
		// data for best found network
		float bestNetworkScore = Float.MAX_VALUE;
		float[] bestMemory = null;
		
		// for best results, lets start with a random state numerous times, climb, repeat
		// this functions as a hybrid of both random greedy and hill climbing
		for(int i = 0; i < ITERATIONS; ++i) {
			// reset RBF state
			resetState();
			
			// having a zero in the starting memory is bad - start with something random
			n.randomizeMemory();
			
			boolean done = false;
			while(!done) {
				done = climb(n, input, expectedOutput);
			}
			
			float myScore = calculateScore(n, input, expectedOutput);
			if (myScore < bestNetworkScore) {
				bestNetworkScore = myScore;
				bestMemory = Arrays.copyOf(n.getMemory(), n.getMemory().length);
			}
		}
		
		// apply best memory
		n.setMemory(bestMemory);
		
		// apply best values to data and verify success
		System.out.println(name + " Network");
		System.out.println("Score: " + bestNetworkScore);
		for(int i = 0; i < 4; ++i) {
			float computed = n.computeNetwork(input[i])[0];
			System.out.println("[" + input[i][0] + ", " + input[i][1] + "] -> " + computed + ", Ideal: " + expectedOutput[i]);
			Assert.assertEquals(expectedOutput[i], computed, TOLERANCE);
		}	
	}
	
	private boolean climb(RBFNetwork n, float[][] input, float[] output) {
		int length = n.getMemory().length;
		boolean moveMade = false;
		
		// for each coefficient
		for(int i = 0; i < length; ++i) {
			int best = -1;
			
			
			// try every possible step
			for(int j = 0; j < candidate.length; ++j) {
				n.getMemory()[i] += (stepSize[i] * candidate[j]);
				float score = calculateScore(n, input, output);
				n.getMemory()[i] -= (stepSize[i] * candidate[j]);
				
				// did it improve? keep track of improvement
				if (score < bestScore) {
					bestScore = score;
					best = j;
				}
			}
			
			if (best != -1) {
				n.getMemory()[i] += (stepSize[i] * candidate[best]);
				stepSize[i] *= candidate[best];
				moveMade = true;
			}
		}
		
		return !moveMade;
	}
	
	private float calculateScore(RBFNetwork n, float[][] input, float[] expectedOutput) {
		for(int i = 0; i < outputVector.length; ++i) {
			outputVector[i] = n.computeNetwork(input[i])[0];
		}
		
		return Errors.sumOfSquares(expectedOutput, outputVector);
	}
}
