package com.talixa.techlib.ai.training;

import org.junit.Test;

import com.talixa.techlib.math.Polynomial;

import junit.framework.Assert;

public class PolyTest {

	private static final float TOLERANCE = .01f;
	
	private static final float[] INPUTS =  new float[] {-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10};
	private static final float a = 2;
	private static final float b = 4;
	private static final float c = 6;
	private static final float d = 3;
	private static final float e = -4;
	private static final float f = 15;
	private static final float g = 14;
	
	// maximum number of iterations for search
	private static final int ITERATIONS = 100000;
	
	// min/max values for random training
	private static final int MINX = 0;
	private static final int MAXX = 10;
	
	@Test
	public void testFirstDegreePolyGRT() {
		// calculate real values
		float[] outputs = new float[INPUTS.length];
		for(int i = 0; i < INPUTS.length; ++i) {
			outputs[i] = Polynomial.calculate(INPUTS[i], new float[] {a,b});
		}
		
		// run poly finder
		PolyFinder poly = new PolyGreedy(INPUTS, outputs, 1, MINX, MAXX);
		float[] v = poly.getCoefficients(ITERATIONS);
		
		// verify coefficients
		Assert.assertEquals(a, v[0], TOLERANCE);
		Assert.assertEquals(b, v[1], TOLERANCE);
	}
	
	@Test
	public void testSecondDegreePolyGRT() {
		// calculate real values
		float[] outputs = new float[INPUTS.length];
		for(int i = 0; i < INPUTS.length; ++i) {
			outputs[i] = Polynomial.calculate(INPUTS[i], new float[] {a,b,c});
		}
		
		// run poly finder
		PolyGreedy poly = new PolyGreedy(INPUTS, outputs, 2, MINX, MAXX);
		float[] v = poly.getCoefficients(ITERATIONS);
		
		// verify coefficients
		Assert.assertEquals(a, v[0], TOLERANCE);
		Assert.assertEquals(b, v[1], TOLERANCE);
		Assert.assertEquals(c, v[2], TOLERANCE);
	}
	
	@Test
	public void testThirdDegreePolyHill() {
		// calculate real values
		float[] outputs = new float[INPUTS.length];
		for(int i = 0; i < INPUTS.length; ++i) {
			outputs[i] = Polynomial.calculate(INPUTS[i], new float[] {a,b,c,d});
		}
		
		// run poly finder
		PolyFinder poly = new PolyHill(INPUTS, outputs, 3);
		float[] v = poly.getCoefficients(ITERATIONS);
		
		// verify coefficients
		Assert.assertEquals(a, v[0], TOLERANCE);
		Assert.assertEquals(b, v[1], TOLERANCE);
		Assert.assertEquals(c, v[2], TOLERANCE);
		Assert.assertEquals(d, v[3], TOLERANCE);
	}
	
	@Test
	public void testFifthDegreePolyHill() {
		// calculate real values
		float[] outputs = new float[INPUTS.length];
		for(int i = 0; i < INPUTS.length; ++i) {
			outputs[i] = Polynomial.calculate(INPUTS[i], new float[] {a,b,c,d,e,f});
		}
		
		// run poly finder
		PolyFinder poly = new PolyHill(INPUTS, outputs, 5);
		float[] v = poly.getCoefficients(ITERATIONS);
		
		// verify coefficients
		Assert.assertEquals(a, v[0], TOLERANCE);
		Assert.assertEquals(b, v[1], TOLERANCE);
		Assert.assertEquals(c, v[2], TOLERANCE);
		Assert.assertEquals(d, v[3], TOLERANCE);
		Assert.assertEquals(e, v[4], TOLERANCE);
		Assert.assertEquals(f, v[5], TOLERANCE);
	}
	
	@Test
	public void testSixthDegreePolyHill() {
		// calculate real values
		float[] outputs = new float[INPUTS.length];
		for(int i = 0; i < INPUTS.length; ++i) {
			outputs[i] = Polynomial.calculate(INPUTS[i], new float[] {a,b,c,d,e,f,g});
		}
		
		// run poly finder
		PolyFinder poly = new PolyHill(INPUTS, outputs, 6);
		float[] v = poly.getCoefficients(ITERATIONS);
		
		// verify coefficients
		Assert.assertEquals(a, v[0], TOLERANCE);
		Assert.assertEquals(b, v[1], TOLERANCE);
		Assert.assertEquals(c, v[2], TOLERANCE);
		Assert.assertEquals(d, v[3], TOLERANCE);
		Assert.assertEquals(e, v[4], TOLERANCE);
		Assert.assertEquals(f, v[5], TOLERANCE);
		Assert.assertEquals(g, v[6], TOLERANCE);
	}
}
