package com.talixa.techlib.ai.general;

import org.junit.Test;

import junit.framework.Assert;

public class DistanceTest {
	
	private static final double TOLERANCE = .0001;
		
	private static final float[] POINT_A = new float[] {0,0};
	private static final float[] POINT_B = new float[] {1,1};
	
	@Test
	public void testEuclideanDistance() {
		float distance = Distance.euclidean(POINT_A, POINT_B);
		Assert.assertEquals(1.4142f, distance, TOLERANCE);
	}
	
	@Test
	public void testManhattanDistance() {
		float distance = Distance.manhattan(POINT_A, POINT_B);
		Assert.assertEquals(2f, distance, TOLERANCE);
	}
	
	@Test
	public void testChebyshevDistance() {
		float distance = Distance.chebyshev(POINT_A, POINT_B);
		Assert.assertEquals(1f, distance, TOLERANCE);
	}
}
