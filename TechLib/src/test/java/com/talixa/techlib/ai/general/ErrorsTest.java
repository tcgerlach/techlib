package com.talixa.techlib.ai.general;

import org.junit.Test;

import junit.framework.Assert;

public class ErrorsTest {
	
	private static final double TOLERANCE = .0001;

	private static float[] Y_HAT = new float[]{10,10,10};
	private static float[] Y_BAR = new float[]{0,0,0};
	
	@Test
	public void testSumOfSquaresError() {
		Assert.assertEquals(300, Errors.sumOfSquares(Y_HAT, Y_BAR), TOLERANCE);
	}
	
	@Test
	public void testMeanSquareError() {
		Assert.assertEquals(100, Errors.meanSquared(Y_HAT, Y_BAR), TOLERANCE);
	}
	
	@Test
	public void testRootMeanSquareError() {
		Assert.assertEquals(10, Errors.rootMeanSquared(Y_HAT, Y_BAR), TOLERANCE);
	}
}
