package com.talixa.techlib.ai.genetic.programming;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import junit.framework.Assert;

public class TreeTest {
	
	private static TreeNode root;
	private static float[] memory;
	
	@BeforeClass
	public static void setup() {
		// set memory - three constants and one variable
		float x = 5;
		memory = new float[] {.75f, 2, 1, x};
		
		// setup tree
		TreeNode c2 = new TreeNode(null,null,Tree.OPCODE_MEMORY+1);
		TreeNode vX = new TreeNode(null,null,Tree.OPCODE_MEMORY+3);
		TreeNode fPow = new TreeNode(vX,c2,Tree.OPCODE_POW);
		TreeNode cD75 = new TreeNode(null,null,Tree.OPCODE_MEMORY+0);
		TreeNode fMul = new TreeNode(cD75,fPow,Tree.OPCODE_MULTIPLY);
		TreeNode fSqrt = new TreeNode(fMul,null,Tree.OPCODE_SQRT);
		TreeNode c1 = new TreeNode(null,null,Tree.OPCODE_MEMORY+2);
		TreeNode fSub = new TreeNode(fSqrt,c1,Tree.OPCODE_SUBTRACT);
		
		root = fSub;
	}

	@Test
	public void testTreeEvaluation() {
		// evaluate value
		float value = Tree.evaluate(root, memory);
		Assert.assertEquals(3.3301f, value, .01f);
	}
	
	@Test
	public void testTreeSize() {
		// calculate size
		int size = Tree.countNodes(root);
		Assert.assertEquals(8, size);
	}
	
	@Test
	public void testRandomSelection() {
		// test random selection of node
		
		// keep track of selections
		Map<TreeNode,Integer> countByHash = new HashMap<TreeNode,Integer>();
		
		// run 100 selections and count results
		for(int i = 0; i < 100; ++i) {
			TreeNode selected = Tree.getRandomNode(root);
			if (countByHash.containsKey(selected)) {
				countByHash.put(selected, countByHash.get(selected) + 1);
			} else {
				countByHash.put(selected, 1);
			}
		}
		
		// each node should have been selected at least once
		Assert.assertEquals(Tree.countNodes(root), countByHash.size());
	}
	
	@Test
	public void testClone() {
		TreeNode copy = Tree.clone(root);
		
		// test a few pieces to make sure they are the same values
		Assert.assertEquals(root.getOpcode(), copy.getOpcode());
		Assert.assertEquals(root.getLeft().getOpcode(), copy.getLeft().getOpcode());
		Assert.assertEquals(root.getRight().getOpcode(), copy.getRight().getOpcode());
		
		// test evaluation equality
		Assert.assertEquals(Tree.evaluate(root, memory), Tree.evaluate(root, memory), .01f);

		// test that nodes are not equal
		Assert.assertNotSame(root, copy);
		Assert.assertNotSame(root.getRight(), copy.getRight());
		Assert.assertNotSame(root.getLeft(), copy.getLeft());

	}
}
