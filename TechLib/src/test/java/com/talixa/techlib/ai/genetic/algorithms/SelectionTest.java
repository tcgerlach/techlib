package com.talixa.techlib.ai.genetic.algorithms;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.talixa.techlib.ai.genetic.algorithms.Selection;
import com.talixa.techlib.ai.shared.ScoredFloatVector;
import com.talixa.techlib.ai.shared.ScoredVector;

public class SelectionTest {
	
	private List<ScoredVector> population;
	private static final int ITERATIONS = 10000;

	@Before 
	public void setup() {
		population = new ArrayList<ScoredVector>();
		for(int i = 0; i < 100; ++i) {
			// don't need any data, just the score to test
			population.add(new ScoredFloatVector(null,i));
		}
	}
	
	@Test
	public void testTruncation() {
		for(int i = 0; i < ITERATIONS; ++i) {
			ScoredVector x = Selection.truncation(population, .1f);
			// should always get samples in first 10% of population
			Assert.assertTrue(x.getScore() < 10);
		}
	}
	
	@Test
	public void testTournament() {
		float totalScore = 0;
		for(int i = 0; i < ITERATIONS; ++i) {
			totalScore += Selection.tournament(population, 10).getScore();
		}
		
		// reasonable to assume that if we run the algorithm a number of times,
		// the average score should be a low score.
		if (totalScore/ITERATIONS > 10) {
			Assert.fail("Expected average < 10, actual average was " + totalScore/ITERATIONS);
		}
	}
	
	@Test
	public void testFitnessProportionate() {
		ScoredVector.reverseScores(population);		// selection algorithm says better scores are larger numbers
		
		float totalScore = 0;
		for(int i = 0; i < ITERATIONS; ++i) {
			totalScore += Selection.fitnessProportionate(population).getScore();
		}
		
		// reasonable to assume that if we run the algorithm a number of times,
		// the average score should be a high score.
		if (totalScore/ITERATIONS < 65) {
			Assert.fail("Expected average > 65, actual average was " + totalScore/ITERATIONS);
		}
	}
	
	@Test
	public void testStochasticUniversalSampling() {
		ScoredVector.reverseScores(population);		// selection algorithm says better scores are larger numbers
		List<ScoredVector> x = Selection.stochasticUniversalSampling(population, 10);
		Assert.assertEquals(10, x.size());
	}
}
