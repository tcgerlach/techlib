package com.talixa.techlib.ai.tsp;

import org.junit.Test;

import com.talixa.techlib.geo.DistanceCalculator;
import com.talixa.techlib.geo.data.Cities;
import com.talixa.techlib.geo.data.City;

public class TravelingSalesmanProblemTest {

	@Test
	public void testSolve() {
		City[] order = TravelingSalesmanProblem.solve(Cities.AllCities);
		System.out.println("Order to Visit Cities: ");
		for(int i = 0; i < order.length; ++i) {
			// some formatting to make it look nice
			if (i > 0) {
				if (i % 5 == 0) {
					System.out.println();
				} else {
					System.out.print(" - ");
				}
			}
			System.out.print(order[i].getName());
		}
		System.out.println();
		long distanceTraveled = Math.round(DistanceCalculator.routeDistance(order)/1000);
		System.out.println("Distance Traveled: " + distanceTraveled + " KM");
	}
}
