package com.talixa.techlib.ai.genetic.algorithms;

import org.junit.Assert;
import org.junit.Test;

import com.talixa.techlib.ai.shared.OffspringPair;
import com.talixa.techlib.ai.genetic.algorithms.Mutation;
import com.talixa.techlib.ai.prng.RandomLCG;
import com.talixa.techlib.geo.data.Cities;
import com.talixa.techlib.geo.data.City;
import com.talixa.techlib.math.Vector;

public class MutationTest {

	private static final int ITERATIONS = 10000;
	private static final float PRECISION = .01f;
	
	@Test
	public void testPerturb() {
		int noChangeCount = 0;
		int totalCount = 0;
		for(int n = 0; n < ITERATIONS; ++n) {
			float[] start = new float[] {1,2,3,4,5,6,7,8,9,10};
			float[] end = Mutation.perturb(start, 10);
			for(int i = 0; i <= 9; ++i) {
				if (end[i] == (i+1)) {
					noChangeCount++;
				}
				
				++totalCount;
			}
		}
		
		// around 5 percent of the time, the value will be unchanged
		// verify that we are no more than 6%
		float percentUnchanged = (float)noChangeCount/(float)totalCount;
		Assert.assertTrue(percentUnchanged < .06f);
	}
	
	@Test
	public void testShuffle() {
		for(int n = 0; n < ITERATIONS; ++n) {
			float[] start = new float[] {1,2,3,4,5,6,7,8,9,10};
			float[] end = Mutation.shuffle(start,1);
			
			boolean mutated = false;
			for(int i = 0; i < 10; ++i) {
				if (i+1 != (int)end[i]) {
					mutated = true;
					break;
				}
			}
			
			if (!mutated) {
				Assert.fail("Input vector did not change");
			}
			
			Assert.assertEquals(55, Vector.sumVector(end), .01);
		}
	}
	
	@Test
	public void testSplice() {
		basicSpliceTest(true);
	}
	
	@Test
	public void testSpliceNoRepeat() {
		basicSpliceTest(false);
	}
	
	private void basicSpliceTest(boolean allowRepeat) {
		for(int n = 0; n < ITERATIONS; ++n) {
			int cutSize = RandomLCG.getNextInt(3) + 1;
			
			float[] p1 = new float[] {1,2,3,4,5,6,7,8,9,10};
			float[] p2 = new float[] {10,9,8,7,6,5,4,3,2,1}; 
			
			float[][] children;
			if (allowRepeat) {
				children = Mutation.splice(p1, p2, cutSize);
			} else {
				children = Mutation.spliceNoRepeat(p1, p2, cutSize);
			}
			
			// check number of mutations in child 0
			int mutationCount1 = 0;
			for(int i = 0; i < 10; ++i) {
				if (Math.abs(p1[i] - children[0][i]) > 0.01) {
					mutationCount1++;
				}
			}
			
			// check mutation count in child 1
			int mutationCount2 = 10; // opposite order count down
			for(int i = 0; i < 10; ++i) {
				if (Math.abs(p1[i] - children[1][i]) > 0.01) {
					mutationCount2--;
				}
			}
			
			if (!allowRepeat) {
				// if no repeats, should always sum to 55
				float sum1 = Vector.sumVector(children[0]);
				float sum2 = Vector.sumVector(children[1]);
				Assert.assertEquals(55, sum1, PRECISION);
				Assert.assertEquals(55, sum2, PRECISION);
			} else {
				// mutation count should match cut size for both children if repeats allowed
				Assert.assertEquals(cutSize, mutationCount1);
				Assert.assertEquals(cutSize, mutationCount2);
			}
		}
	}
	
	@Test
	public void testObjectSplice() {
		City[] list1 = new City[] {Cities.AltoonaPA, Cities.Atlanta, Cities.Boston, Cities.LosAngeles};
		City[] list2 = new City[] {Cities.Savannah, Cities.Pittsburgh, Cities.Orlando, Cities.NewYorkCity};
		
		OffspringPair<City> o = Mutation.splice(list1, list2, 2);
		
		// each offspring should have 2 mutations
		int mutationCount1 = 0;
		int mutationCount2 = 0;
		for(int i = 0; i < list1.length; ++i) {
			if (o.getChild1()[i] != list1[i]) {
				++mutationCount1;
			}
			if (o.getChild2()[i] != list2[i]) {
				++mutationCount2;
			}
		}
		
		Assert.assertEquals(2, mutationCount1);
		Assert.assertEquals(2, mutationCount2);
		
	}
}
