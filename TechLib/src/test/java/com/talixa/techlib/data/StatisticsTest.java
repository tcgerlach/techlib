package com.talixa.techlib.data;

import junit.framework.Assert;
import org.junit.Test;

public class StatisticsTest {

	private static final double[] DATA = new double[] {1,2,3,4,5};

	@Test
	public void testSum() {
		double sum = Statistics.sum(DATA);
		Assert.assertEquals(15,sum,.01);
	}

	@Test
	public void testMean() {
		double mean = Statistics.mean(DATA);
		Assert.assertEquals(3,mean,.01);				
	}

	@Test
	public void testMedian() {
		double median = Statistics.median(DATA);
		Assert.assertEquals(3,median,.01);
	}

	@Test
	public void testRange() {
		double range = Statistics.range(DATA);
		Assert.assertEquals(4,range,.01);
	}
}

