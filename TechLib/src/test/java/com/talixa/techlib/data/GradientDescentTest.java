package com.talixa.techlib.data;

import org.junit.Test;

import com.talixa.techlib.math.functions.NegativeSumOfSquares;
import com.talixa.techlib.math.functions.NegativeSumOfSquaresGradient;
import com.talixa.techlib.math.functions.SumOfSquares;
import com.talixa.techlib.math.functions.SumOfSquaresGradient;
import com.talixa.techlib.math.interfaces.Function;

import junit.framework.Assert;

public class GradientDescentTest {

	private static double[] inputData1 = new double[] {10,10,10};
	private static double[] inputData2 = new double[] {8,14,72};
	private static Function func = new SumOfSquares();
	private static Function grad = new SumOfSquaresGradient();
	private static Function nfunc = new NegativeSumOfSquares();
	private static Function ngrad = new NegativeSumOfSquaresGradient();
	private static final double TOLERANCE = .001;
	
	@Test
	public void testFunctions() {
		// verify soq works		
		Assert.assertEquals(300, func.execute(inputData1), TOLERANCE);

		// verify gradient works
		Assert.assertEquals(60, grad.execute(inputData1), TOLERANCE);

		// verify soq works
		Assert.assertEquals(-300, nfunc.execute(inputData1), TOLERANCE);

		// verify gradient works
		Assert.assertEquals(-60, ngrad.execute(inputData1), TOLERANCE);
	}

	@Test
	public void testDescent() {
		// verify descent
		double[] theta = GradientDescent.minimize(func, grad, inputData1, GradientDescent.MAX_PRECISION);
		Assert.assertEquals(0, func.execute(theta), TOLERANCE);
		Assert.assertEquals(0, theta[0], TOLERANCE);
		Assert.assertEquals(0, theta[1], TOLERANCE);
		Assert.assertEquals(0, theta[2], TOLERANCE);
		
		// test again with bad starting point
		double[] theta2 = GradientDescent.minimize(func, grad, inputData2, GradientDescent.MAX_PRECISION);
		Assert.assertEquals(0, func.execute(theta2), TOLERANCE);
		Assert.assertEquals(0, theta2[0], TOLERANCE);
		Assert.assertEquals(0, theta2[1], TOLERANCE);
		Assert.assertEquals(0, theta2[2], TOLERANCE);
	}
	
	@Test
	public void testAscent() {		
		// verify descent
		double[] theta = GradientDescent.maximize(nfunc, ngrad, inputData1, GradientDescent.MAX_PRECISION);
		Assert.assertEquals(0, nfunc.execute(theta), TOLERANCE);
		Assert.assertEquals(0, theta[0], TOLERANCE);
		Assert.assertEquals(0, theta[1], TOLERANCE);
		Assert.assertEquals(0, theta[2], TOLERANCE);
		
		// test again with bad starting point
		double[] theta2 = GradientDescent.maximize(nfunc, ngrad, inputData2, GradientDescent.MAX_PRECISION);
		Assert.assertEquals(0, nfunc.execute(theta2), TOLERANCE);
		Assert.assertEquals(0, theta2[0], TOLERANCE);
		Assert.assertEquals(0, theta2[1], TOLERANCE);
		Assert.assertEquals(0, theta2[2], TOLERANCE);
	}

	@Test
	public void infiniteAscent() {
		double[] theta = GradientDescent.maximize(func, grad, inputData1);
		Assert.assertNull(theta);
	}

	@Test
	public void infiniteDescent() {
		double[] theta = GradientDescent.minimize(nfunc, ngrad, inputData1);
		Assert.assertNull(theta);
	}
}
