package com.talixa.techlib.data;

import junit.framework.Assert;
import org.junit.Test;

public class ProbabilityTest {
	
	private static final float TOLERANCE = .01f;

	@Test
	public void testUniformPdf() {
		Assert.assertEquals(1, Probability.uniformPdf(.500), TOLERANCE);
		Assert.assertEquals(1, Probability.uniformPdf(.999), TOLERANCE);
		Assert.assertEquals(0, Probability.uniformPdf(1.00), TOLERANCE);
	}

	@Test
	public void testUniformCdf() {
		Assert.assertEquals(0, Probability.uniformCdf(-5), TOLERANCE);
		Assert.assertEquals(.9, Probability.uniformCdf(.9), TOLERANCE);
		Assert.assertEquals(1, Probability.uniformCdf(1), TOLERANCE);
	}
	
	@Test
	public void testEvenEventCount() {
		// How many options available for a 4-bit number?
		Assert.assertEquals(16, Probability.getEventCount(4, 2));
		
		// How many combinations of numbers with 4-digits?
		Assert.assertEquals(10000, Probability.getEventCount(4, 10));
		
		// How many 5-letter passwords?
		Assert.assertEquals(11881376, Probability.getEventCount(5, 26));
	}
	
	@Test
	public void testUnevenEventCount() {
		// How many number combinations where first number is not 0, 
		// second is any number, and third number is odd?
		Assert.assertEquals(450, Probability.getEventCount(new int[] {9,10,5}));
	}
	
	@Test
	public void testPermCount() {
		// with 4 teams, how many permutations are there for who places
		// in each place from 1st place to 5th place?
		Assert.assertEquals(120, Probability.getPermCount(5));
		
		// In the above question, what if I only care about teams in the top 3?
		Assert.assertEquals(60,  Probability.getPermCount(5, 3));
		
		// a seven member club is electing president, vp, and secretary
		// how many permutations of the board exist?
		Assert.assertEquals(210, Probability.getPermCount(7,3));
	}
	
	@Test 
	public void testBinomialDistributionTheoremProbability() {
		// what is the probability of obtaining exactly four heads in 6 coin tosses?
		double prob = Probability.binomialDistributionTheoremProbability(6, .5f, 4);
		Assert.assertEquals(15f/64f, prob, TOLERANCE);
		
		// probability of 3 sixes in five throws of a die
		prob = Probability.binomialDistributionTheoremProbability(5, 1f/6f, 3);
		Assert.assertEquals(125f/3882f, prob, TOLERANCE);
	}
}