package com.talixa.techlib.physics;

import org.junit.Test;

import junit.framework.Assert;

public class TemperatureTest {
	
	private static final double TOLERANCE = .0001;

	@Test
	public void convertToCelcius() {
		Assert.assertEquals(0, Temperature.farenheitToCelcius(32), TOLERANCE);
		Assert.assertEquals(100, Temperature.farenheitToCelcius(212), TOLERANCE);
	}
	
	@Test
	public void convertToFarenheit() {
		Assert.assertEquals(32, Temperature.celsiusToFarenheit(0), TOLERANCE);
		Assert.assertEquals(212, Temperature.celsiusToFarenheit(100), TOLERANCE);
	}
}
