package com.talixa.techlib.physics;

import org.junit.Test;

import com.talixa.techlib.shared.Vector2D;

import junit.framework.Assert;

public class PhysicsTest {
	
	private static final double TOLERANCE = .1;

	@Test
	public void testMassToWeight() {
		double weight = Physics.calculateWeightInPounds(45.3592, Physics.GRAVITY_ON_EARTH);
		Assert.assertEquals(100, weight, TOLERANCE);
	}
	
	@Test 
	public void testGetVelocity() {
		Vector2D velocity = Physics.getVelocity(1.41f,45f);
		Assert.assertEquals(1, velocity.getX(), TOLERANCE);
		Assert.assertEquals(1, velocity.getY(), TOLERANCE);
	}
}
