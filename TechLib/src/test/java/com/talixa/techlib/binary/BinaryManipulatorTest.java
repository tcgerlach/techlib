package com.talixa.techlib.binary;

import org.junit.Test;

import junit.framework.Assert;

public class BinaryManipulatorTest {

	@Test
	public void testReverse() {
		Assert.assertEquals(0xF0, BinaryManipulator.reverseBits(0x0F));
		Assert.assertEquals(0x01, BinaryManipulator.reverseBits(0x80));
		Assert.assertEquals(0x03, BinaryManipulator.reverseBits(0xC0));
	}
}
